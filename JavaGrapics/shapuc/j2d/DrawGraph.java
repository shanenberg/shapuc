package shapuc.j2d;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;

import javax.swing.JFrame;

import shapuc.graphs.Edge;
import shapuc.graphs.Graph;
import shapuc.graphs.factories.GraphFactory;
import shapuc.graphs.factories.GraphFactoryGraphical;
import shapuc.graphs.factories.GraphFactoryGraphical2D;
import shapuc.graphs.graphical.Edge2DMultiPoints;
import shapuc.graphs.graphical.Graphical2DNode;
import shapuc.graphs.graphical.GraphicalNode;
import shapuc.util.datastructures.PointDouble2D;

public class DrawGraph extends JFrame {

	Graph<Graphical2DNode, Edge2DMultiPoints> graph;
	 
	PointDouble2D center;
	 
	Graphical2DNode movingNode = null;
 	 
	int nodeSize = 0;
	PointDouble2D moveStartPosition=null;
	PointDouble2D movedElementStartPosition=null;
	
	MouseListener mouseClickListener = new MouseListener() {
		
		public void mouseReleased(MouseEvent e) {
			movingNode = null;
			moveStartPosition = null;
		}
		
		public void mousePressed(MouseEvent event) {
			for (Graphical2DNode node : graph.getNodes()) {
				Ellipse2D e = new Ellipse2D.Double();

				e.setFrame(
						(int) (node.getX() - 0.5*nodeSize),
						(int) (node.getY() - 0.5*nodeSize),
						nodeSize, nodeSize);
				
				// I clicked on a node
				if (e.contains(event.getPoint())) {
					movingNode = node;
					moveStartPosition = new PointDouble2D(event.getX(), event.getY());
					movedElementStartPosition = node.getPosition().clone();
					repaint();
					return;
				}
			}		

			for (Edge2DMultiPoints edge : graph.getEdges()) {
				PointDouble2D labelPos = edge.getLabelPosition();
				int width = DrawGraph.this.getGraphics().getFontMetrics().stringWidth(edge.getName());
				int height = DrawGraph.this.getGraphics().getFontMetrics().getHeight();
				
				Rectangle r = 
						new Rectangle(
								(int) (edge.getLabelPosition().x-(0.5*width)), 
								(int) (edge.getLabelPosition().y-(height)), 
								width, height);
				
				if (r.contains(event.getPoint())) {
					movingNode = null;
					
					// if even number of nodes, add new node
					if ((edge.getNumberOfNodes() & 1) == 0) {
						movingNode = edge.insertMiddlePoint();
						movedElementStartPosition = movingNode.getPosition().clone();
						moveStartPosition = new PointDouble2D(event.getX(), event.getY());
						repaint();
					} else {
						movingNode = edge.getMiddlePoint();
						movedElementStartPosition = movingNode.getPosition().clone();
						moveStartPosition = new PointDouble2D(event.getX(), event.getY());
						repaint();
					}
					
				}
			}
		
		}
		
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	};
	
	
	MouseMotionListener listener = new MouseMotionListener() {
		public void mouseDragged(MouseEvent event) {
			if (movingNode!=null) {
				PointDouble2D distanceToStart = moveStartPosition.minusCopy(new PointDouble2D(event.getX(), event.getY()));
				movingNode.setX(movedElementStartPosition.x - distanceToStart.x);
				movingNode.setY(movedElementStartPosition.y - distanceToStart.y);
				repaint();
			}
		}

		public void mouseMoved(MouseEvent event) {

		}
	};

	public DrawGraph( Graph<Graphical2DNode, Edge2DMultiPoints> g, int nodeSize) throws HeadlessException {
		super();
		this.graph = g;
		this.nodeSize = nodeSize;

		int i = 0;
		
		this.getContentPane().addMouseListener(mouseClickListener);
		this.getContentPane().addMouseMotionListener(listener);
	}
	
	
	public void automPositionNodes() {
		int numNodes = graph.getNodes().size();
		
		double part = 2 * Math.PI / numNodes;
		
		int i = 0;
		for (Graphical2DNode n : graph.getNodes()) {
			n.setX(center.x + Math.sin(i*part)*nodeSize);
			n.setY(center.y + Math.cos(i*part)*nodeSize);
			i++;
		}

		
	}

	@Override
	public void paint(Graphics graphics) {
		super.paint(graphics);
		
		double xOffset = (0.5*nodeSize);
		double yOffset = (0.5*nodeSize);
		
		for (Graphical2DNode n : graph.getNodes()) {
			graphics.drawOval((int) (n.getX() -  xOffset), (int) (n.getY() - yOffset), nodeSize, nodeSize);

			int width = graphics.getFontMetrics().stringWidth(n.getIdentifier());
			graphics.drawString(n.getIdentifier(), (int) (n.getX() - (0.5*width)), (int) n.getY());
		}

		for (Edge2DMultiPoints edge : graph.getEdges()) {
			PointDouble2D[] path = edge.getPath();
			for (int i = 0; i < path.length-1; i++) {

				PointDouble2D[] startEnd = path[i].connectionStartEnd(path[i + 1], nodeSize / 2);

				if (i == 0) {
					if (path.length==2) {
						graphics.drawLine((int) startEnd[0].x, (int) startEnd[0].y, (int) startEnd[1].x,
								(int) startEnd[1].y);
					} else {
						graphics.drawLine((int) startEnd[0].x, (int) startEnd[0].y, 
								(int) path[i+1].x, (int) path[i+1].y);
					}
				} else if (i == (path.length-2)) {
					graphics.drawLine((int) path[i].x, (int) path[i].y, (int) startEnd[1].x,
							(int) startEnd[1].y);
				} else {
					graphics.drawLine((int) path[i].x, (int) path[i].y, (int) path[i+1].x,
							(int) path[i+1].y);
				}

				if (i==(path.length-2)) {
					graphics.setColor(Color.red);
					graphics.drawOval((int) startEnd[1].x-2, (int) startEnd[1].y-2, 4, 4);
					graphics.setColor(Color.black);
				}
			}
			
		}
		
		for (Edge2DMultiPoints edge : graph.getEdges()) {
			PointDouble2D labelPos = edge.getLabelPosition();
			int width = graphics.getFontMetrics().stringWidth(edge.getName());
			graphics.drawString(edge.getName(), (int) labelPos.x - (width/2), (int) (labelPos.y));
		}
		
		

	}

	public void setSize(int x, int y) {
		super.setSize(x, y);
		this.center = new PointDouble2D(0.5*x, 0.5*y);
	}
	
	public void showIt() {
		this.setSize(500, 500);
		this.setVisible(true);
		
	}
	
}
