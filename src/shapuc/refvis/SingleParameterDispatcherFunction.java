/*******************************************************************************
 * /**
 * * The MIT License (MIT)
 * *
 * * Copyright (c) 2015, 2016 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * * 
 * * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * * software and associated documentation files (the "Software"), to deal in the Software 
 * * without restriction, including without limitation the rights to use, copy, modify, 
 * * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * * The above copyright notice and this permission notice shall be included in all copies or 
 * * substantial portions of the Software.
 *
 * * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * **/
package shapuc.refvis;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import shapuc.refvis.exceptions.VisitorInvokationException;
import shapuc.refvis.reflectiveVisitor.impl.SingleParameterMethodFinder;

public class SingleParameterDispatcherFunction<ReturnType, TreeNodeType> {

	final Object targetObject;
	final String visitMethodName; 
	
	public SingleParameterDispatcherFunction(Object targetObject, String visitMethodName) {
		super();
		this.targetObject = targetObject;
		this.visitMethodName = visitMethodName;
	}

	/**
	 * Dispatches the method call to the most appropriate method using a SingleParameterMethodFiner. 
	 * 
	 * @param treeNode
	 * @return
	 */
	public ReturnType invokeVisit(TreeNodeType treeNode) {
		Method m = new SingleParameterMethodFinder(visitMethodName, targetObject.getClass(), treeNode.getClass()).findMethod();
		try {
			return (ReturnType) m.invoke(targetObject, treeNode);
		} catch (InvocationTargetException ex) {
			if (ex.getTargetException() instanceof RuntimeException)
				throw (RuntimeException) ex.getTargetException();
			throw new RuntimeException(ex.getTargetException());
		} catch (IllegalAccessException ex1) {
			throw new VisitorInvokationException("Cannot invoke method " + m.toString() + " on " + this.getClass() + " with parameter type " + treeNode.getClass().getName());
		} catch (IllegalArgumentException ex1) {
			throw new VisitorInvokationException("Cannot invoke method " + m.toString() + " on " + this.getClass() + " with parameter type " + treeNode.getClass().getName());
		}
	}
}
