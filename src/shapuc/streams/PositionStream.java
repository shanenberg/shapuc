/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package shapuc.streams;

import java.util.List;


public abstract class PositionStream<StreamElement> {
	public int currentPosition = 0;

	public abstract int length();
	
	/**
	 * Reads the current element and increments currentPosition.
	 * */
	public StreamElement next() {
		StreamElement ret = this.current();
		currentPosition++;
		return ret;
	}

	/* Reads the element at the current position */
	public abstract StreamElement current();
	
	public abstract List<StreamElement> elements(int from, int to);
	public abstract String elementsToString(int from, int to);

	public boolean hasRestString() {
		return currentPosition < length();
	}

	public boolean isEOF() {
		return !hasRestString();
	}
	
	public String stuckExceptionString() {
		return "I am stuck";
	}
	
	public String stuckExceptionStringUpTo(int position) {
		return stuckExceptionString();
	}
}
