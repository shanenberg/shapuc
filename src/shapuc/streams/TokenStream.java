/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package shapuc.streams;

import java.util.List;
import java.util.Vector;

import shapuc.scanning.Token;


public class TokenStream<POSITIONSTREAM_TYPE> extends PositionStream<Token<?>> {

	final Vector<Token<?>> tokens = new Vector<Token<?>>();
	public PositionStream<POSITIONSTREAM_TYPE> sourceStream;
	
	public TokenStream() {
		super();
	}	
	
	public TokenStream(PositionStream<POSITIONSTREAM_TYPE> sourceStream) {
		super();
		this.sourceStream = sourceStream;
	}

	public void add(Token<?> t) {
		tokens.add(t);
	}

	@Override
	public int length() {
		return tokens.size();
	}

	@Override
	public Token<?> current() {
		return tokens.get(super.currentPosition);
	}

	@Override
	public List<Token<?>> elements(int from, int to) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Token<?> get(int i) {
		return tokens.get(i);
	}

	public String stuckExceptionString() {
		return stuckExceptionStringUpTo(currentPosition - 1);
	}
	
	public String stuckExceptionStringUpTo(int position) {
		return sourceStream.stuckExceptionStringUpTo(get(position).startPositionInStream);
	}	
	
	public String toString() {
		StringBuffer ret = new StringBuffer();
		for (Token<?> token : tokens) {
			ret.append(token.toString() + " --- ");
		}
		return ret.toString();
	}

	@Override
	public String elementsToString(int from, int to) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void clean() {
		tokens.removeAllElements();
		this.currentPosition=0;
	}
	
	public void printOutStream() {
		for (Token<?> token : tokens) {
			System.out.println(token);
		}
	}

	public void print() {
		for (Token<?> token : tokens) {
			System.out.print(token);
		}
	}
	
	public void printToCurrent() {
		int pos = 0;
		while (pos<=currentPosition && pos < this.length()) {
			System.out.print(tokens.elementAt(pos));
			pos++;
		}
	}	

}
