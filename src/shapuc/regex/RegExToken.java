package shapuc.regex;

import shapuc.operatorTree.OPLeaf;
import shapuc.regex.operations.RegExVisitor;
import shapuc.regex.operations.RegExVisitorVoid;

public class RegExToken implements RegEx {

	Character value;

	public Character getValue() {
		return value;
	}

	public void setValue(Character value) {
		this.value = value;
	}
	
	public RegExToken(Character value) {
		this.value = value;
	}

	public <PARAMETERYPE> void accept(RegExVisitorVoid<PARAMETERYPE> visitor, PARAMETERYPE p) {
		visitor.visit(this, p);
	}

	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(RegExVisitor<RETURNTYPE, PARAMETERYPE> visitor,
			PARAMETERYPE p) {
		return 	visitor.visit(this, p);
	}

	public String regExString() {
		return value.toString();
	}

}
