package shapuc.regex;

import java.util.ArrayList;

import shapuc.operatorTree.OPLeaf;
import shapuc.regex.operations.RegExVisitor;
import shapuc.regex.operations.RegExVisitorVoid;
/**
 * StringStuff: "(!"|\")*"
 *             => Starting with ", followed by everything except " or \ followed by ", and finally a "
 *             => AND(TOKEN("), KLEENE(OR(ANYACCEPT("), AND(TOKEN(\), TOKEN(")))), TOKEN("))
 * @author stefan
 *
 */
public class RegExAnyTokenWithout implements RegEx {

	public ArrayList<Character> getNonAcceptedChars() {
		return nonAcceptedChars;
	}

	public RegExAnyTokenWithout(ArrayList<Character> nonAcceptedChars) {
		super();
		this.nonAcceptedChars = nonAcceptedChars;
	}

	ArrayList<Character> nonAcceptedChars = new ArrayList<Character>();
	
	public RegExAnyTokenWithout(Character...invalidChar) {
		super();
		for (int i = 0; i < invalidChar.length; i++) {
			nonAcceptedChars.add(invalidChar[i]);
		}
	}

	public <PARAMETERYPE> void accept(RegExVisitorVoid<PARAMETERYPE> visitor, PARAMETERYPE p) {
		visitor.visit(this, p);
	}

	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(RegExVisitor<RETURNTYPE, PARAMETERYPE> visitor,
			PARAMETERYPE p) {
		return visitor.visit(this, p);
	}

	public String regExString() {
		StringBuilder ret = new StringBuilder();
		ret.append("![");
		for (Character character : nonAcceptedChars) {
			ret.append(character);
		}
		ret.append("]");
		return ret.toString();
	}

}
