package shapuc.regex;

public abstract class RegExNAry {
	
	public final RegEx[] children;
	
	public RegEx[] getChildren() {
		return children;
	}
	
	public RegExNAry(RegEx... list) {
		children = list;
	}

	public String regExString() {
		StringBuffer ret = new StringBuffer();

		if (children.length == 1)
			return "";

		ret.append(children[0].regExString());

		for (int i = 1; i < children.length; i++) {
			ret.append(separator());
			ret.append(children[i].regExString());
		}
		return ret.toString();
	}

	abstract protected String separator();


}
