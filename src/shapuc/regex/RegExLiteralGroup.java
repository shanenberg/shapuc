package shapuc.regex;

import shapuc.regex.operations.RegExVisitor;
import shapuc.regex.operations.RegExVisitorVoid;

public class RegExLiteralGroup implements RegEx {

	public RegExToken start;
	public RegExToken end;
	
	public RegExLiteralGroup(RegExToken start, RegExToken end) {
		super();
		this.start = start;
		this.end = end;
	}
	
	public RegExLiteralGroup(Character start, Character end) {
		super();
		this.start = new RegExToken(start);
		this.end =  new RegExToken(end);
	}
	

	public String regExString() {
		return "{" + "}";
	}

	public <PARAMETERYPE> void accept(RegExVisitorVoid<PARAMETERYPE> visitor, PARAMETERYPE p) {
		visitor.visit(this, p);
	}

	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(RegExVisitor<RETURNTYPE, PARAMETERYPE> visitor,
			PARAMETERYPE p) {
		return visitor.visit(this, p);
	}

}
