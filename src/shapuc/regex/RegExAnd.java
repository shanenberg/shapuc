package shapuc.regex;

import shapuc.operatorTree.OPNAry;
import shapuc.regex.operations.RegExVisitor;
import shapuc.regex.operations.RegExVisitorVoid;

public class RegExAnd extends RegExNAry implements RegEx {

	public RegExAnd(RegEx... list) {
		super(list);
	}

	public <PARAMETERYPE> void accept(RegExVisitorVoid<PARAMETERYPE> visitor, PARAMETERYPE p) {
		visitor.visit(this, p);
	}
 
	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(RegExVisitor<RETURNTYPE, PARAMETERYPE> visitor,
			PARAMETERYPE p) {
		return 	visitor.visit(this, p);
	}

	@Override
	protected String separator() {
		return "";
	}


}
