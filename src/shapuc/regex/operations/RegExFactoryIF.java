package shapuc.regex.operations;

import shapuc.automata.NEA;
import shapuc.automata.NEAFactory;

public interface RegExFactoryIF<ELEMENT_TYPE> {
	
	public ELEMENT_TYPE KLEENE(ELEMENT_TYPE regEx);
	public ELEMENT_TYPE OR(ELEMENT_TYPE...regEx);
	public ELEMENT_TYPE AND(ELEMENT_TYPE...regEx);
	public ELEMENT_TYPE OPT(ELEMENT_TYPE regEx);
	public ELEMENT_TYPE BRACKET(ELEMENT_TYPE regEx);

}
