package shapuc.regex.operations;

import shapuc.operatorTree.OPAnd;
import shapuc.operatorTree.OPBracket;
import shapuc.operatorTree.OPN0Fold;
import shapuc.operatorTree.OPOpt;
import shapuc.operatorTree.OPOr;
import shapuc.operatorTree.OPN1Fold;
import shapuc.operatorTree.OPLeaf;
import shapuc.regex.RegEx;
import shapuc.regex.RegExAnd;
import shapuc.regex.RegExAnyTokenWithout;
import shapuc.regex.RegExBracket;
import shapuc.regex.RegExKleene;
import shapuc.regex.RegExLiteralGroup;
import shapuc.regex.RegExOptional;
import shapuc.regex.RegExOr;
import shapuc.regex.RegExPlus;
import shapuc.regex.RegExToken;
import shapuc.util.cmd.Cmd;
import shapuc.util.cmd.CmdP1;

public interface RegExVisitorVoid<PARAMETERYPE> {
	
	public void visit(RegExAnd node, PARAMETERYPE p);
	public void visit(RegExBracket node, PARAMETERYPE p);
	public void visit(RegExKleene node, PARAMETERYPE p);
	public void visit(RegExLiteralGroup node, PARAMETERYPE p);
	public void visit(RegExOptional node, PARAMETERYPE p);
	public void visit(RegExOr node, PARAMETERYPE p);
	public void visit(RegExPlus node, PARAMETERYPE p);
	public void visit(RegExToken node, PARAMETERYPE p);
	public void visit(RegExAnyTokenWithout node, PARAMETERYPE p);
	
}
