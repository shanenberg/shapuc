package shapuc.regex.operations;

import shapuc.automata.NEA;
import shapuc.automata.NEAFactory;
import shapuc.operatorTree.OPAnd;
import shapuc.operatorTree.OPBracket;
import shapuc.operatorTree.OPN0Fold;
import shapuc.operatorTree.OPOpt;
import shapuc.operatorTree.OPOr;
import shapuc.operatorTree.OPLeaf;
import shapuc.regex.RegEx;
import shapuc.regex.RegExAnd;
import shapuc.regex.RegExAnyTokenWithout;
import shapuc.regex.RegExBracket;
import shapuc.regex.RegExKleene;
import shapuc.regex.RegExLiteralGroup;
import shapuc.regex.RegExOptional;
import shapuc.regex.RegExOr;
import shapuc.regex.RegExToken;

public abstract class RegExFactory 
	/*implements RegExFactoryIF<RegEx<Token<TOKENVALUE_TYPE>>>*/ {
	
	public RegExKleene KLEENE(RegEx regEx) {
		return new RegExKleene(regEx);
	}
	
	public RegExOr OR(RegEx...regEx) {
		return new RegExOr(regEx);
	}

	public RegExAnd AND(RegEx...regEx) { 
		return new RegExAnd(regEx);
	}

	public RegExOptional OPT(RegEx regEx) {
		return new RegExOptional(regEx);
	}	
	
	public RegExBracket BRACKET(RegEx regEx) {
		return new RegExBracket(regEx); 
	}	
	
	public RegExToken TOKEN(Character value) {
		return new RegExToken(value); 
	}		
	
	public RegExAnyTokenWithout ANYTOKEN_WITHOUT(Character value) {
		return new RegExAnyTokenWithout(value); 
	}		
	
	public RegExLiteralGroup RANGE(Character start, Character end) {
		return new RegExLiteralGroup(TOKEN(start), TOKEN(end)); 
	}		
	
	public abstract RegEx regEx();
	
}
