package shapuc.regex.operations;

import shapuc.regex.*;
import shapuc.util.cmd.Cmd;
import shapuc.util.cmd.CmdP1;

public interface RegExVisitor<RETURNTYPE, PARAMETERYPE> {
	
	public RETURNTYPE visit(RegExAnd node, PARAMETERYPE p);
	public RETURNTYPE visit(RegExBracket node, PARAMETERYPE p);
	public RETURNTYPE visit(RegExKleene node, PARAMETERYPE p);
	public RETURNTYPE visit(RegExLiteralGroup node, PARAMETERYPE p);
	public RETURNTYPE visit(RegExOptional node, PARAMETERYPE p);
	public RETURNTYPE visit(RegExOr node, PARAMETERYPE p);
	public RETURNTYPE visit(RegExPlus node, PARAMETERYPE p);
	public RETURNTYPE visit(RegExToken node, PARAMETERYPE p);
	public RETURNTYPE visit(RegExAnyTokenWithout node, PARAMETERYPE p);

}
