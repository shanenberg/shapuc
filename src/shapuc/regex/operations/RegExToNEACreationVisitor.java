package shapuc.regex.operations;

import java.util.Collection;
import java.util.List;

import shapuc.automata.NEA;
import shapuc.automata.NEAFactory;
import shapuc.automata.custom.CharTransition;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.automata.custom.WildcardStringTransition;
import shapuc.operatorTree.treeoperations.OPVisitorVoid;
import shapuc.regex.*;
import shapuc.regex.operations.RegExToNEACreationVisitor.Params;
import shapuc.scanning.tokenizer.TokenizerNEA;
import shapuc.scanning.tokenizers.TokenizerState;

/**
 * Visitor creates a NEA for a given RegularExpression. This visitor is abstract and should be used 
 * by concrete visitors such as the one implemented in NEAFromKeyword
 */
public abstract class RegExToNEACreationVisitor 
	implements RegExVisitorVoid<RegExToNEACreationVisitor.Params<TokenizerState>> {
 
	protected abstract CustomTransitionHandler<Character> createSpontaneousTransition();
	protected abstract TokenizerState createStartState();
	protected abstract TokenizerState createEndState();
	protected abstract CustomTransitionHandler<Character> createTransition(Character val);
	protected abstract CustomTransitionHandler<Character> createLiteralGroupTransition(Character start, Character end);
	protected abstract CustomTransitionHandler<Character> createAnyWithoutTransition(List<Character> val);
	
	public TokenizerNEA nea;

	public boolean hasActiveStates = false;
	
	public static class Params<STATE_TYPE> {
		final STATE_TYPE start, end;
		public Params(STATE_TYPE start, STATE_TYPE end) {super();this.start = start;this.end = end;}
	}	
	
	public Params<TokenizerState> params(TokenizerState start, TokenizerState end) {
		return new Params<TokenizerState>(start, end);
	}
	
	
	public RegExToNEACreationVisitor() {
	}

	public RegExToNEACreationVisitor(TokenizerState startState, TokenizerState endState) {
		this();
		nea.setStartState(startState);
	}	


	protected TokenizerState createInnerState() {
		return createStartState();
	}	

	/**
	 * For AND N1, N2, N3
	 *   Start  END ---> START->N1Start N1End->N2Start N2End->N3Start N3End->End
	 */
	public void visit(RegExAnd node, Params<TokenizerState> p) {
		TokenizerState currentStart = p.start;
		
		for (RegEx element : node.children) {
			TokenizerState myStart = createInnerState();
			TokenizerState myEnd = createInnerState();
			nea.addSpontaneousTransition(currentStart, myStart);
			
			element.accept(this, params(myStart, myEnd));
			currentStart=myEnd;
		}
		
		nea.addSpontaneousTransition(currentStart, p.end);
		
	}

	public void visit(RegExBracket node, Params<TokenizerState> p) {
		node.accept(this, p);
	} 

	public void visit(RegExKleene node, Params<TokenizerState> p) {
		TokenizerState myStart = createInnerState();
		TokenizerState myEnd = createInnerState();
		nea.addSpontaneousTransition(p.start, myStart);
		nea.addSpontaneousTransition(myEnd, p.end);
		nea.addSpontaneousTransition(myStart, myEnd); // From start to end -> 0 times
		
		// Create inner nodes for 0 times
		TokenizerState myInnerStart = createInnerState();
		TokenizerState myInnerEnd = createInnerState();
		nea.addSpontaneousTransition(myStart, myInnerStart);
		nea.addSpontaneousTransition(myInnerEnd, myEnd);
		nea.addSpontaneousTransition(myInnerEnd, myInnerStart); // From inner end to inner start -> n times
		
		node.child.accept(this, params(myInnerStart, myInnerEnd));
	}
	
	public void visit(RegExLiteralGroup node, Params<TokenizerState> p) {
		CustomTransitionHandler<Character> transition = 
				this.createLiteralGroupTransition((Character) node.start.getValue(), (Character) node.end.getValue());
		nea.addTransition(p.start, transition, p.end);
	}	

	public void visit(RegExOptional node, Params<TokenizerState> p) {
		TokenizerState myStart = createStartState();
		TokenizerState myEnd = createStartState();
		nea.addSpontaneousTransition(p.start, myStart);
		nea.addSpontaneousTransition(myEnd, p.end);
		nea.addSpontaneousTransition(myStart, myEnd); // From start to end -> 0 times
		node.child.accept(this, params(myStart, myEnd));
	}

	public void visit(RegExOr node, Params<TokenizerState> p) {
		for (RegEx element : node.children) {
			TokenizerState myStart = createInnerState();
			TokenizerState myEnd = createInnerState();
			nea.addSpontaneousTransition(p.start, myStart);
			nea.addSpontaneousTransition(myEnd, p.end);
			
			element.accept(this, params(myStart, myEnd));
		}	
	}

	public void visit(RegExPlus node, Params<TokenizerState> p) {
		
		RegExAnd c = 
				new RegExAnd(
					node.child,
					new RegExKleene(node.child)
				);
		
		c.accept(this, p);
	}

	public void visit(RegExToken node, Params<TokenizerState> p) {
		CustomTransitionHandler<Character> transition = this.createTransition((Character) node.getValue());
		nea.addTransition(p.start, transition, p.end);
	}
	
	public void visit(RegExAnyTokenWithout node, Params<TokenizerState> p) {
		CustomTransitionHandler<Character> transition = this.createAnyWithoutTransition((List<Character>) node.getNonAcceptedChars());
		nea.addTransition(p.start, transition, p.end);
	}
	
	public void initializeNEA(TokenizerNEA nea) {
		this.nea = nea;
		this.nea.setStartState(createStartState());
		this.nea.addEndState(createEndState());
		this.nea.setSpontaneousTransition(createSpontaneousTransition());		
	}

}
