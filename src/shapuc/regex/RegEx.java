package shapuc.regex;

import shapuc.regex.operations.RegExVisitor;
import shapuc.regex.operations.RegExVisitorVoid;

public interface RegEx {
	
	public String regExString();
	
	public <PARAMETERYPE> void accept(RegExVisitorVoid<PARAMETERYPE> visitor, PARAMETERYPE p);
	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(RegExVisitor<RETURNTYPE, PARAMETERYPE> visitor, PARAMETERYPE p);

	
}