package shapuc.regex;

public class RegExLib {

	public static RegEx createRegExForKeyword(String keyword) {
		RegExToken[] tokens = new RegExToken[keyword.length()];
		
		for (int i = 0; i < keyword.length(); i++) {
			tokens[i] = new RegExToken(keyword.charAt(i));
		}
		
		return new RegExAnd(tokens);
	}
}
