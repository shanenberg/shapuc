package shapuc.regex;

import shapuc.operatorTree.OPUnary;
import shapuc.regex.operations.RegExVisitor;
import shapuc.regex.operations.RegExVisitorVoid;

public class RegExPlus implements RegEx {

	public RegEx child;
	
	public RegEx getChild() {
		return child;
	}

	public void setChild(RegEx p) {
		child = p;
	}	
	
	public RegExPlus(RegEx regex) {
		child = regex;
	}	
	
	public String regExString() {
		return child.regExString() + "+";
	}

	public <PARAMETERYPE> void accept(RegExVisitorVoid<PARAMETERYPE> visitor, PARAMETERYPE p) {
		visitor.visit(this, p);
	}

	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(RegExVisitor<RETURNTYPE, PARAMETERYPE> visitor,
			PARAMETERYPE p) {
		return 	visitor.visit(this, p);
	}

}
