package shapuc.regex;

import shapuc.regex.operations.RegExVisitor;
import shapuc.regex.operations.RegExVisitorVoid;

public class RegExOr extends RegExNAry implements RegEx {

	public RegExOr(RegEx... list) {
		super(list);
	}

	public <PARAMETERYPE> void accept(RegExVisitorVoid<PARAMETERYPE> visitor, PARAMETERYPE p) {
		visitor.visit(this, p);
	}

	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(RegExVisitor<RETURNTYPE, PARAMETERYPE> visitor,
			PARAMETERYPE p) {
		return 	visitor.visit(this, p);
	}

	@Override
	protected String separator() {
		return "|";
	}

}
