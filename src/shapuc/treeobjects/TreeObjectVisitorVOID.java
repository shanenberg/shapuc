package shapuc.treeobjects;

public interface TreeObjectVisitorVOID {
	public void visit(TreeObject to);
	public void visit(TreeObject1 to);
	public void visit(TreeObjectN to);
}
