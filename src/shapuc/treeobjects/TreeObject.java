package shapuc.treeobjects;

import java.util.List;
import java.util.Vector;

public class TreeObject<TYPE, VALUETYPE> {
	public TYPE type;
	public VALUETYPE value;
	
	public TreeObject(TYPE type, VALUETYPE value) {
		super();
		this.type = type;
		this.value = value;
	}

	public TreeObject() {
		super();
	}
	
	public void accept(TreeObjectVisitorVOID visitor) {
		visitor.visit(this);
	}

	public List getChildren() {
		return new Vector();
	}
	
	public <RETTYPE extends TreeObject<? extends TreeObject, ?>> RETTYPE at(Integer...elements) {
		RETTYPE ret = (RETTYPE) this;
		for (int i = 0; i < elements.length; i++) {
			ret = (RETTYPE) (ret.getChildren().get(elements[i]));
		}
		return ret;
	}
	
}
