package shapuc.treeobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class TreeObject1<TYPE, VALUETYPE, CHILDTYPE> extends TreeObject<TYPE, VALUETYPE>{
	public CHILDTYPE child;
	
	public void accept(TreeObjectVisitorVOID visitor) {
		visitor.visit(this);
	}

	public List<CHILDTYPE> getChildren() {
		Vector r = new Vector();
		r.addElement(child);
		return r;
	}	
	
	public ArrayList<CHILDTYPE> treeObjects = new ArrayList<CHILDTYPE>();

	public TreeObject1(TYPE type, VALUETYPE value, CHILDTYPE treeObject) {
		super(type, value);
		this.child = treeObject;
	}	
}
