package shapuc.treeobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class TreeObjectN<TYPE, VALUETYPE, CHILDTYPE> extends TreeObject<TYPE, VALUETYPE>{

	public ArrayList<CHILDTYPE> treeObjects = new ArrayList<CHILDTYPE>();

	public TreeObjectN(TYPE type, VALUETYPE value, List<CHILDTYPE> treeObjects) {
		super(type, value);
		this.treeObjects.addAll(treeObjects);
	}

	public TreeObjectN() {
		super();
	}
	
	public void accept(TreeObjectVisitorVOID visitor) {
		visitor.visit(this);
	}

	public List<CHILDTYPE> getChildren() {
		return treeObjects;
	}	
	
}
