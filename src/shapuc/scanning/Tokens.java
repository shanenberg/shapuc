package shapuc.scanning;

import shapuc.regex.RegEx;

public class Tokens  {

	public static final TokenTypeKeyword KEYWORD() {
		return TokenTypeKeyword.keyword;
	}
	
	public static final TokenTypeRegEx REGEX(String name, RegEx regex) {
		return new TokenTypeRegEx(name, regex);
	}
	
}	
