package shapuc.scanning;

import java.util.Set;

import shapuc.automata.AutomataFactoryParameters;
import shapuc.automata.DEA;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.util.IdentitySet;

public class DEAWithStateSet extends DEA<Set<TokenizerState>, CustomTransitionHandler<Character>, Character> {

	public DEAWithStateSet() {
		super(AutomataFactoryParameters.CUSTOM_CHECK, AutomataFactoryParameters.INACTIVE_STATE);
	}

	public boolean containsState(Set<TokenizerState> aState) {
		return this.getAllStates().contains(aState);
	}
	
	public Set<TokenizerState> getMatchingState(Set<TokenizerState> aState) {
		// System.out.println("getMatchingState");
		for (Set<TokenizerState> aSet : this.getAllStates()) {
			// System.out.println("+");
			if (aSet.equals(aState)) 
				return aSet;

		}
		throw new RuntimeException("Set not contained");
	}

}