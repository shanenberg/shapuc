package shapuc.scanning.tokenizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import shapuc.automata.AutomataFactoryParameters;
import shapuc.automata.DEA;
import shapuc.automata.DEAFactory;
import shapuc.automata.NEA;
import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.automata.custom.AnyWithoutTransition;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CharSetTransition;
import shapuc.automata.custom.CharTransition;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.automata.custom.WildcardStringTransition;
import shapuc.regex.RegEx;
import shapuc.regex.operations.RegExToNEACreationVisitor;
import shapuc.scanning.DEAWithStateSet;
import shapuc.scanning.DisjointInputsResults;
import shapuc.scanning.KeywordOrRegExKeyword;
import shapuc.scanning.KeywordOrRegExRegEx;
import shapuc.scanning.Separator;
import shapuc.scanning.TokenType;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.Tokens;
import shapuc.scanning.tokenizers.NEAForKeywordCreationVisitor;
import shapuc.scanning.tokenizers.NEAForRegExCreationVisitor;
import shapuc.scanning.tokenizers.NEAForSeparatorCreationVisitor;
import shapuc.scanning.tokenizers.NEAForTokenCreationVisitor;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.scanning.tokenizers.TokenizerStateAcceptor;
import shapuc.scanning.tokenizers.TokenizerStateFinalizer;
import shapuc.scanning.tokenizers.TokenizerStateInactive;
import shapuc.scanning.tokenizers.TokenizingException;
import shapuc.streams.StringBufferStream;
import shapuc.streams.TokenStream;
import shapuc.util.IdentitySet;
import shapuc.util.Pair;
import shapuc.util.Triple;


public class TokenizerFactory {

	Tokenizer tokenizer = new Tokenizer();

	public Tokenizer createTokenizerFromKeywordsOrRegExs(List<Object> stringOrRegExList, String input) {
		StringBufferStream charInputStream = new StringBufferStream(input);
		TokenStream tokenOutputStream = new TokenStream(charInputStream);
		Tokenizer tokenizer = createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		tokenizer.charStream = charInputStream;
		tokenizer.resultTokenStream = tokenOutputStream;		
		return tokenizer;
	}	
	
	/**
	 * The main method to be used. Pass here the objects are are interested in .
	 * 
	 * 
	 * @param stringOrRegExList
	 * @return
	 */
	

	
	public Tokenizer createTokenizerFromKeywordsOrRegExs(List<Object> stringOrRegExList) {

		Vector<TokenizerNEA> neas = 
				new NEAsCreation().createNEAsFromKeywordsOrRegExs(tokenizer, stringOrRegExList);

		TokenizerNEA resultNEA = 
				new NEACombinator().combineNEAs(tokenizer, neas);

		TokenizerDEA dea
		 	= new DEAFromNEAConstructor().constructDEAFromNEA(tokenizer, resultNEA);
		
		tokenizer.automata = dea;
		return tokenizer;
	}

}


class StateSet {
	public Set<IdentitySet<TokenizerState>> set = new HashSet<IdentitySet<TokenizerState>>();

	public IdentitySet<TokenizerState> addAll(Set<TokenizerState> currentStates) {
		IdentitySet<TokenizerState> newSet = new IdentitySet<TokenizerState>();
		newSet.addAll(currentStates);
		set.add(newSet);
		return newSet;
	}

	public boolean containsStates(Set<TokenizerState> aSet) {
		for (IdentitySet<TokenizerState> stateSet : set) {
			if (stateSet.containsAll(aSet) && (stateSet.size() == set.size())) {
				return true;
			}
		}
		return false;
	}
	


	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		s.append("{");
		for (IdentitySet<TokenizerState> identitySet : set) {
			s.append(" " + identitySet.toString() + " ");
		}
		s.append("}");
		return s.toString();
	}

}
