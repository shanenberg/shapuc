package shapuc.scanning.tokenizer;

import java.util.List;
import java.util.Vector;

import shapuc.automata.NEA;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.regex.RegEx;
import shapuc.scanning.KeywordOrRegExKeyword;
import shapuc.scanning.KeywordOrRegExRegEx;
import shapuc.scanning.Separator;
import shapuc.scanning.TokenType;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.Tokens;
import shapuc.scanning.tokenizers.NEAForKeywordCreationVisitor;
import shapuc.scanning.tokenizers.NEAForRegExCreationVisitor;
import shapuc.scanning.tokenizers.NEAForSeparatorCreationVisitor;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.scanning.tokenizers.TokenizingException;
import shapuc.util.Pair;

public class NEAsCreation {

	public Vector<TokenizerNEA> 
		createNEAsFromKeywordsOrRegExs(Tokenizer tokenizer, List<Object> stringOrRegExList) {

		Vector<TokenizerNEA> neas = 
				new Vector<TokenizerNEA>();		
		
		for (int i = 0; i < stringOrRegExList.size(); i++) {
			Object object = stringOrRegExList.get(i);
			
			if (object instanceof KeywordOrRegExKeyword) {
				KeywordOrRegExKeyword keyword = (KeywordOrRegExKeyword) object;
				neas.add(createNEAforKeyword(tokenizer, keyword.keyword, i + 1));

			// Pair means it is a regex
			} else if (object instanceof String) {
				neas.add(createNEAforKeyword(tokenizer, (String) object, i + 1));
				
			} else if (object instanceof Pair<?, ?>) {
				Pair<String, RegEx> regexPair = (Pair<String, RegEx>) object;
				neas.add(createNEAforRegEx(
						Tokens.REGEX(regexPair.getLeft(), regexPair.getRight()),
						tokenizer, regexPair.getRight(), i+1));
				
			} else if (object instanceof KeywordOrRegExRegEx) {
				KeywordOrRegExRegEx regEx = (KeywordOrRegExRegEx) object;
				neas.add(createNEAforRegEx(
						Tokens.REGEX(regEx.name, regEx.regex),
						tokenizer, regEx.regex, i+1));
				
			} else if (object instanceof Separator){
				neas.add(createNEAforSeparator(
						Tokens.REGEX("SEPARATOR", ((Separator) object).regularExpression),
						tokenizer, ((Separator) object).regularExpression, i+1));
				
			} else {
				throw new TokenizingException("createTokenizerFromKeywordsOrRegExs requires RegExs or Strings");
			}
		}
		
		return neas;
	}
	
	
	TokenizerNEA createNEAforKeyword(Tokenizer tokenizer,
			String keyword, int priority) {
		return new NEAForKeywordCreationVisitor(priority, tokenizer, keyword).createNEA();
	}
	
	TokenizerNEA createNEAforRegEx(
			TokenType tokenType, Tokenizer tokenizer, RegEx regex, int priority) {
		return new NEAForRegExCreationVisitor(tokenType, priority, tokenizer, regex).createNEA();
	}	
	
	TokenizerNEA createNEAforSeparator(
			TokenType tokenType, Tokenizer tokenizer, RegEx regex, int priority) {
		return new NEAForSeparatorCreationVisitor(tokenType, priority, tokenizer, regex).createNEA();
	}		
	
	
}
