package shapuc.scanning.tokenizer;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Stack;
import java.util.TreeSet;
import java.util.Vector;

import shapuc.automata.custom.AnyWithoutTransition;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CharTransition;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.util.KeySetContainer;
import shapuc.util.Pair;
import shapuc.util.datastructures.Interval.ContainmentType;
import shapuc.util.datastructures.IntervalChar;

public class DEAFollowUpTransitionStateCollector {
	
	public KeySetContainer<CharRangeTransition, TokenizerState> rangeTransitions = 
			new KeySetContainer<CharRangeTransition, TokenizerState>();
	
	public KeySetContainer<AnyWithoutTransition, TokenizerState> anyWithoutTransitions = 
			new KeySetContainer<AnyWithoutTransition, TokenizerState>();
	

	public void addRangeCharTransition(CharRangeTransition transition, TokenizerState followUpState) {
		rangeTransitions.add(transition, followUpState);
	}	
	
	public void addAnyWithoutTransition(AnyWithoutTransition transition, TokenizerState followUpState) {
		anyWithoutTransitions.add(transition, followUpState);
	}

	/**
	 * Die singleChar transitions sind bereits überschneidungsfrei.
	 * 
	 * 1. singleChars aus Ranges entfernen.
	 * 2. Ranges aus sich selbst entfernen.
	 * @param newStateSets 
	 * @param stateSets 
	 * @param resultDEA 
	 * @param tokenizerNEA 
	 * @return 
	 */
	public Vector<Pair<IntervalChar,HashSet<TokenizerState>>> prepareForDEA(
			TokenizerNEA tokenizerNEA, 
			TokenizerDEA resultDEA, 
			HashMap<TokenizerState,HashSet<TokenizerState>> stateSets, 
			HashMap<TokenizerState, HashSet<TokenizerState>> newStateSets) {
		
		// Sortieren der Characters!!!!!!!
		// rangedCharacter: Character, left - end (HashSet(HashSet())) - right - start (HashSet(HashSet()))
		Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>> rangedCharacters = 
				new Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>>();
		
		// Bereits die Intervalle vor, d.h. erzeuge die Liste "a"...."z"....usw
		prepareIntervals(rangedCharacters);

		Vector<Pair<IntervalChar, HashSet<TokenizerState>>> resultIntervalStates = 
				new Vector<Pair<IntervalChar, HashSet<TokenizerState>>>();	
		
		// Erzeuge die Intervalle (und Folgezustände in resultIntervalStates
		createIntervals(rangedCharacters, resultIntervalStates);
		
		// Folgezustände enthalten noch nicht mögliche Spontanübergänge, d.h. nun müssen 
		// für alle Eingaben die Folgezustände inkl. spontanübergänge beschrieben werden.
		
		for (Pair<IntervalChar, HashSet<TokenizerState>> pair : resultIntervalStates) {
			tokenizerNEA.setCurrentStatesAndExploreSponaneousTransitions(pair.getRight());
			pair.getRight().addAll(tokenizerNEA.getCurrentStates());
		}
		
		return resultIntervalStates;
	}

	public void createIntervals(
			Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>> rangedCharacters,
			Vector<Pair<IntervalChar, HashSet<TokenizerState>>> resultIntervalStates) {

		// Alle Intervalgrenzen liegen nun fest und start/ende-states sind enthalten.

		TreeSet<Character> ts = new TreeSet<Character>();
		ts.addAll(rangedCharacters.keySet());
		
		HashSet<HashSet<TokenizerState>> currentStates = 
				new HashSet<HashSet<TokenizerState>>(); 
		
		Character last = null;
		
		for (Character aChar : ts) {
			if (last==null) {

				// Anfang und Ende sind gleich, d.h. es handelt sich um 
				// einen einzelnen Buchstaben, für den die Start-Zustandsmenge gilt.
				if (!rangedCharacters.get(aChar).getLeft().isEmpty() &&
						!rangedCharacters.get(aChar).getRight().isEmpty()) {
					currentStates.addAll(rangedCharacters.get(aChar).getRight());
					resultIntervalStates.add(intervallNStates(asSingleHashSet(currentStates), aChar, aChar ));
					last = (char) (aChar + 1);
					currentStates.removeAll(rangedCharacters.get(aChar).getLeft());
				} else {
					currentStates.addAll(rangedCharacters.get(aChar).getRight());
					last = aChar;
				}
				
			} else {
				// wenn nur kein Ende eines Intervals (also keine neue Startzustände), 
				// dann muss kein zusatzIntervall erzeugt werden
				if (rangedCharacters.get(aChar).getLeft().isEmpty()) {
					resultIntervalStates.add(intervallNStates(asSingleHashSet(currentStates), last, aChar -1 ));

					currentStates.addAll(rangedCharacters.get(aChar).getRight());
					last = aChar;
				// wenn sowohl Anfang als auch Ende gesetzt wird, dann muss aktueller Charakter einzeln
				// behandelt werden, das sowohl Zustandsmenge vorher und nachher enthält
				} else if (
					!rangedCharacters.get(aChar).getLeft().isEmpty() &&
					!rangedCharacters.get(aChar).getRight().isEmpty()){
					
					resultIntervalStates.add(intervallNStates(asSingleHashSet(currentStates), last, aChar -1 ));
					
					currentStates.addAll(rangedCharacters.get(aChar).getRight());

					resultIntervalStates.add(intervallNStates(asSingleHashSet(currentStates), aChar, aChar));

					
					currentStates.removeAll(rangedCharacters.get(aChar).getLeft());
					
					last = (char) (aChar + 1);
					
				// wenn nur Ende gesetzt wird, dann  
				} else {
					resultIntervalStates.add(intervallNStates(asSingleHashSet(currentStates), last, aChar));
					
					currentStates.removeAll(rangedCharacters.get(aChar).getLeft());
					last = (char) (aChar + 1);
				}
					
			}
		}
		
		
	}

	private Pair<IntervalChar, HashSet<TokenizerState>> intervallNStates(
			HashSet<TokenizerState> states, int beginInterval, int endInterval) {
		return new Pair<IntervalChar, HashSet<TokenizerState>>(
			new IntervalChar((char) beginInterval, (char) endInterval),states);
	}	

	/**
	 * Erzeugt eine Hashtable von allen Intervallen, die beschrrieben werden:
	 *   - end (menge, die entfernt wird), start(Menge, die hinzugefügt wird), 
	 *     links = ende, rechts = start
	 * 
	 * @param rangedCharacters
	 */
	public void prepareIntervals(
			Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>> rangedCharacters) {
		// Beschreibe alle Intervallgrenzen .....
		for (Pair<CharRangeTransition, HashSet<TokenizerState>> aTransition : rangeTransitions.elements) {
			if (!rangedCharacters.containsKey(aTransition.getLeft().interval.start)) {
				rangedCharacters.put(aTransition.getLeft().interval.start, startEndPair());
			}
			rangedCharacters.get(aTransition.getLeft().interval.start).getRight().add(aTransition.getRight());

			if (!rangedCharacters.containsKey(aTransition.getLeft().interval.end)) {
				rangedCharacters.put(aTransition.getLeft().interval.end, startEndPair());
			}
			rangedCharacters.get(aTransition.getLeft().interval.end).getLeft().add(aTransition.getRight());
		}
	}

	private HashSet<TokenizerState> asSingleHashSet(HashSet<HashSet<TokenizerState>> currentStates) {
		HashSet<TokenizerState> ret = new HashSet<TokenizerState>();
		for (HashSet<TokenizerState> states : currentStates) {
			ret.addAll(states);
		}
		return ret;
	}

	private Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>> startEndPair() {
		return new Pair(new HashSet(), new HashSet());
	}

	private Pair<CharRangeTransition, HashSet<TokenizerState>> newTransition(IntervalChar interval, HashSet<TokenizerState> states) {
		return new Pair<CharRangeTransition, HashSet<TokenizerState>>(
				new CharRangeTransition(interval),
				states);
	}
	
//	
//	private void removeCharFromRanges(Pair<CharTransition, HashSet<TokenizerState>> aCharTransition) {
//		char removedChar = aCharTransition.getLeft().getAcceptanceChar();
//		
//		for (int i = 0; i < rangeTransitions.elements.size(); i++) {
//			Pair<CharRangeTransition, HashSet<TokenizerState>> aRange = rangeTransitions.elements.get(i);
//
//			if (aRange==null) continue;
//			
//			if (aRange.getLeft().handles(removedChar)) {
//				
//				// add all followUpStates to matching single char transition
//				aCharTransition.getRight().addAll(aRange.getRight());
//				
//				HashSet<CharRangeTransition> newRanges = aRange.getLeft().rangesWithRemovedChar(removedChar);
//				
//				for (CharRangeTransition charRangeTransition : newRanges) {
//					rangeTransitions.addAll(charRangeTransition, aRange.getRight());
//				}
//				rangeTransitions.elements.set(i, null);
//			}
//		}
//	}

	
}
