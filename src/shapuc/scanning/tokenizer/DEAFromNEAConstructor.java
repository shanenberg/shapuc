package shapuc.scanning.tokenizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import shapuc.automata.AutomataFactoryParameters;
import shapuc.automata.DEA;
import shapuc.automata.NEA;
import shapuc.automata.custom.AnyWithoutTransition;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CharTransition;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.DEAWithStateSet;
import shapuc.scanning.DisjointInputsResults;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.scanning.tokenizers.TokenizerStateFinalizer;
import shapuc.util.Pair;
import shapuc.util.Triple;

public class DEAFromNEAConstructor {

	public TokenizerDEA 
		constructDEAFromNEA(Tokenizer tokenizer, TokenizerNEA nea) {
		
		AutomataFactoryParameters p = new AutomataFactoryParameters();
		DEAWithStateSet tmpDea = new DEAWithStateSet();
		
		nea.stateReachedCmd = AutomataFactoryParameters.INACTIVE_STATE;

		nea.start();

		// I need the currentNEA states to set the DEA finally to it.
		HashSet<TokenizerState> currentNEAStates = (HashSet) nea.getCurrentStates().clone();
		HashSet<Set<TokenizerState>> newStateSets = new HashSet<Set<TokenizerState>>();
		
		newStateSets.add(currentNEAStates); // Start exploration with current states
		
		while (!newStateSets.isEmpty()) {
			// get all nodes in new States
			HashSet<Set<TokenizerState>> loopNewStates = 
					(HashSet<Set<TokenizerState>>) newStateSets.clone();
			
			newStateSets = new HashSet<Set<TokenizerState>>();
			
			for (Set<TokenizerState> stateSet :  loopNewStates) {
				tmpDea.addState(stateSet);
				nea.setCurrentStates(stateSet);
				HashSet<TokenizerState> neaStates = nea.getCurrentStates();
			
				List<Pair<CustomTransitionHandler<Character>, TokenizerState>> nonSpontaneousTransitions = 
						nea.allNonSpontaneousTransitionsFromStateSet(neaStates);

				DisjointInputsResults realTransitions = 
						this.getDisjointInputsAndTransitions(nonSpontaneousTransitions);

				handleAcceptedInputs(nea, tmpDea, newStateSets, realTransitions);
				handleDisjointAnyInput(nea, tmpDea, newStateSets, realTransitions);				
				handleAny(nea, tmpDea, newStateSets, realTransitions);
			}
		}
		tmpDea.setStartState(tmpDea.getMatchingState(currentNEAStates));
		
/* Hier sind NICHT-Kanten schon enthalten.
 * System.out.println(tmpDea.toString());		
 */
		
//		TokenizingWithAny.drawGraph(tmpDea);	

		
		TokenizerDEA dea = 
				translateDEAtoRegularDEA(tokenizer, tmpDea);
		
		return dea;
	}
	
	private void handleDisjointAnyInput(
			TokenizerNEA nea,
			DEAWithStateSet tmpDea, HashSet<Set<TokenizerState>> newStateSets,
			DisjointInputsResults realTransitions) {
		for (Character inputChar : realTransitions.exceptionDisjoints) {
//System.out.println("input char" + inputChar);			
			Set<TokenizerState> neaStatesBeforeRunningTransition = 
					(HashSet<TokenizerState>) nea.getCurrentStates().clone();
			nea.gotoNext(inputChar);
			Set<TokenizerState> neaStatesAfterRunningTransition = (HashSet<TokenizerState>) nea.getCurrentStates().clone();
			nea.setCurrentStates(neaStatesBeforeRunningTransition); // NEA zurücksetzen
//System.out.println("handle disjoint" + inputChar);					

			if (tmpDea.addState(neaStatesAfterRunningTransition)) {
				// DEA did not knew states....hence they need to be explored soon...
				newStateSets.add(tmpDea.getMatchingState(neaStatesAfterRunningTransition));
				if (tmpDea.getMatchingState(neaStatesAfterRunningTransition).isEmpty())
					throw new RuntimeException("do not use empty states");				
					
			}				
			// THIS IS CRITICAL!!! THE TRANSITION IS NOW IN!!!!
			tmpDea.addTransition(
				tmpDea.getMatchingState(neaStatesBeforeRunningTransition), 
				new CharTransition(inputChar),
				tmpDea.getMatchingState(neaStatesAfterRunningTransition));					
		}
	}

	private void handleAcceptedInputs(NEA<TokenizerState, CustomTransitionHandler<Character>, Character> nea,
			DEAWithStateSet tmpDea, HashSet<Set<TokenizerState>> newStateSets,
			DisjointInputsResults realTransitions) {
		
		for (Character inputChar : realTransitions.singleAcceptedCharsForTransitions) {
//System.out.println("handle accepted" + inputChar);					
			Set<TokenizerState> neaStatesBeforeRunningTransition = 
					(HashSet<TokenizerState>) nea.getCurrentStates().clone();
			nea.gotoNext(inputChar);
			Set<TokenizerState> neaStatesAfterRunningTransition = (HashSet<TokenizerState>) nea.getCurrentStates().clone();
			nea.setCurrentStates(neaStatesBeforeRunningTransition); // NEA zurücksetzen
			
			if (tmpDea.addState(neaStatesAfterRunningTransition)) {
				// DEA did not knew states....hence they need to be explored soon...
				newStateSets.add(tmpDea.getMatchingState(neaStatesAfterRunningTransition));
				if (tmpDea.getMatchingState(neaStatesAfterRunningTransition).isEmpty())
					throw new RuntimeException("do not use empty states");				
					
			}				
			// THIS IS CRITICAL!!! THE TRANSITION IS NOW IN!!!!
			tmpDea.addTransition(tmpDea.getMatchingState(neaStatesBeforeRunningTransition), new CharTransition(inputChar),
				tmpDea.getMatchingState(neaStatesAfterRunningTransition));					
		}
	}
	
	private void handleAny(NEA<TokenizerState, CustomTransitionHandler<Character>, Character> nea,
			DEAWithStateSet tmpDea, HashSet<Set<TokenizerState>> newStateSets,
			DisjointInputsResults realTransitions) {
		

		if (!realTransitions.hasAllQuantifiedTransition) return;
		
//		realTransitions.exceptions.addAll(realTransitions.singleAcceptedCharsForTransitions);
		realTransitions.exceptions.addAll(realTransitions.exceptionDisjoints);

//		System.out.println("handle any");			
		
		/** Now go to the any transitions */
		if (!realTransitions.exceptions.isEmpty()) {
			Set<TokenizerState> neaStatesBeforeRunningTransition = (HashSet<TokenizerState>) nea
					.getCurrentStates().clone();

			nea.gotoNextWithExceptionSet(realTransitions.exceptions);
			
			Set<TokenizerState> neaStatesAfterRunningTransition = (HashSet<TokenizerState>) nea
					.getCurrentStates().clone();

			if (!neaStatesAfterRunningTransition.isEmpty() && tmpDea.addState(neaStatesAfterRunningTransition)) {
				// DEA did not knew states....hence they need to be
				// explored soon...
				newStateSets.add(tmpDea.getMatchingState(neaStatesAfterRunningTransition));
			}
			// THIS IS CRITICAL!!! THE TRANSITION IS NOW IN!!!!
			if (!neaStatesAfterRunningTransition.isEmpty()) {
			tmpDea.addTransition(tmpDea.getMatchingState(neaStatesBeforeRunningTransition),
					new AnyWithoutTransition(realTransitions.exceptions),
					tmpDea.getMatchingState(neaStatesAfterRunningTransition));
			
			}
		}
	}

	/* TODO: Instead of going through inputs, ....
	 * Put all 
	
	*/
	public DisjointInputsResults getDisjointInputsAndTransitions(
			List<Pair<CustomTransitionHandler<Character>, TokenizerState>> _transitions) {
		
		DisjointInputsResults ret = new DisjointInputsResults();
		
		Pair<CustomTransitionHandler<Character>, TokenizerState>[] transitions =  
				new Pair[_transitions.size()];
		
		transitions = _transitions.toArray(transitions);

		HashSet<Character> singleAcceptedCharsForTransitions = new HashSet<Character>();
		HashSet<Character> exceptions = new HashSet<Character>(); 
		HashSet<Character> exceptionDisjoints = new HashSet<Character>(); 
		ArrayList<AnyWithoutTransition<Character>> anyTransitions =
				new ArrayList<AnyWithoutTransition<Character>>();
		
		/* Collect all single chars and anyTransitions*/
		for (int i = 0; i < transitions.length; i++) {
			if (transitions[i].getLeft() instanceof CharTransition) {
				singleAcceptedCharsForTransitions.add(((CharTransition) transitions[i].getLeft()).getAcceptanceChar());
			} else if (transitions[i].getLeft() instanceof AnyWithoutTransition) {
				anyTransitions.add((AnyWithoutTransition) transitions[i].getLeft());
			} else if (transitions[i].getLeft() instanceof CharRangeTransition) {
				throw new RuntimeException("do be implemented");
			}
			
		}
		
		if (anyTransitions.size()==0) {
			ret.hasAllQuantifiedTransition = false;
		} else {
			ret.hasAllQuantifiedTransition = true;
		}
		
		/* Now, all single chars are stored and the anyTransitions are stored */
		for (int i = 0; i < anyTransitions.size(); i++) {
			boolean requiresOwnTransition = false;
			
			for (Character withoutChar : anyTransitions.get(i).without) {
				for (int j = 0; j < anyTransitions.size(); j++) {
					if (anyTransitions.get(j).accepts(withoutChar)) {
						requiresOwnTransition = true;
						exceptionDisjoints.add(withoutChar);
					}
				}
				if (requiresOwnTransition==false) {
					exceptions.add(withoutChar);
				}
				requiresOwnTransition = false;
			}
		}

		ret.singleAcceptedCharsForTransitions = singleAcceptedCharsForTransitions;
		ret.exceptionDisjoints = exceptionDisjoints;
		ret.exceptions = exceptions;
		
//		System.out.println(" has exception " + ret.hasAllQuantifiedTransition);
		return ret;
	}	

	/**
	 * Translates the given DEA with StateSets into a DEA without stateSets. The states with the 
	 * highest priority are used in the resulting DEA.
	 */
	private TokenizerDEA 
		translateDEAtoRegularDEA(Tokenizer tokenizer, DEAWithStateSet tmpDea) {

//TokenizingWithAny.drawGraph(tmpDea);	

		HashMap<Set<TokenizerState>, TokenizerState> stateTranslator = new HashMap<Set<TokenizerState>, TokenizerState>();
		
		 TokenizerDEA result = 
				 new  TokenizerDEA(
						 AutomataFactoryParameters.CUSTOM_CHECK,
						 AutomataFactoryParameters.ACTIVE_STATE);
		 
		 HashSet<Pair<Set<TokenizerState>, CustomTransitionHandler<Character>>> rejectionTransitions = 
				 new HashSet<Pair<Set<TokenizerState>, CustomTransitionHandler<Character>>>();
		 
		 
		 TokenizerStateFinalizer finalizer = new TokenizerStateFinalizer(tokenizer);
		 result.addState(finalizer);
		 result.addEndState(finalizer);
		 
		 
		 
		 for(Set<TokenizerState> aState: tmpDea.getAllStates()) {
			 // The missing clone() took me some days.....shit....
			 TokenizerState highestPriorityState = getStateWithHighestPriority(aState).clone();
			 stateTranslator.put(aState, highestPriorityState);
			 
			 ArrayList<CustomTransitionHandler<Character>> handlers =
					 new ArrayList<CustomTransitionHandler<Character>>();
			 
			 for (Pair<CustomTransitionHandler<Character>, Set<TokenizerState>> aTransition : tmpDea.allTransitionsFrom(aState)) {
				handlers.add(aTransition.getLeft());
			 }
			 
			 // Create transitions for invalid words
			 for (CustomTransitionHandler<Character> trans: createRejectOthersTransitionFromHandlers(handlers)) {
				 rejectionTransitions.add(
						 new Pair<Set<TokenizerState>, CustomTransitionHandler<Character>>(aState, trans));
			 }

		 }
		 
		 for(Triple<Set<TokenizerState>, CustomTransitionHandler<Character>, Set<TokenizerState>> aTransition : tmpDea.allTransitions()) {
			 result.addTransition(
					 stateTranslator.get(aTransition.getLeft()), 
					 aTransition.getMiddle(), 
					 stateTranslator.get(aTransition.getRight()));
		 }
		 
		 for( Pair<Set<TokenizerState>, CustomTransitionHandler<Character>> rejectionTransition: rejectionTransitions) {
		 
			 result.addTransition(
					 stateTranslator.get(rejectionTransition.getLeft()), 
					 rejectionTransition.getRight(), 
					 finalizer);
		 }
		 
		 
		 
		 result.setStartState(stateTranslator.get(tmpDea.getStartState()));

//			TokenizingWithAny.drawGraph2(result);	

			return result;
	}	
	
	private TokenizerState getStateWithHighestPriority(Set<TokenizerState> aState) {
		TokenizerState ret = (TokenizerState) aState.toArray()[0];
		for (TokenizerState tokenizerState : aState) {
			if (tokenizerState.isAcceptor()) {
				if (!ret.isAcceptor()) {
					ret = tokenizerState;
				} else {
					if (tokenizerState.getPriority() < ret.getPriority()) {
						ret = tokenizerState;
					}
				}
			}
		}
		return ret;
	}	
	
	private List<CustomTransitionHandler<Character>> createRejectOthersTransitionFromHandlers(
			ArrayList<CustomTransitionHandler<Character>> handlers) {
		
		ArrayList<CustomTransitionHandler<Character>> ret = 
				new ArrayList<CustomTransitionHandler<Character>>();
		
		boolean hasAllQuantifier = false;
		Set<Character> acceptedChars = new HashSet<Character>();
		Set<Character> excludedChars = new HashSet<Character>();
		
		for (CustomTransitionHandler<Character> customTransitionHandler : handlers) {
			if (customTransitionHandler instanceof CharTransition) {
				acceptedChars.add(((CharTransition) customTransitionHandler).getAcceptanceChar());
			} else if (customTransitionHandler instanceof AnyWithoutTransition) {
				hasAllQuantifier = true;
				excludedChars.addAll(((AnyWithoutTransition) customTransitionHandler).without);
			}
		} 
		
		if (hasAllQuantifier) {
			HashSet<Character> diffSet = new HashSet<Character>();
			diffSet.addAll(excludedChars);
			diffSet.removeAll(acceptedChars);
			for (Character character : diffSet) {
				ret.add(new CharTransition(character));
			}
			return ret;
		} else {
			ret.add(new AnyWithoutTransition<Character>(acceptedChars));
			return ret;
		}
	}
	
}
