package shapuc.scanning.tokenizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import static shapuc.automata.AutomataFactoryParameters.*;
import shapuc.automata.DEA;
import shapuc.automata.NEA;
import shapuc.automata.custom.AnyWithoutTransition;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CharTransition;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.DEAWithStateSet;
import shapuc.scanning.DisjointInputsResults;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.scanning.tokenizers.TokenizerStateFinalizer;
import shapuc.util.Pair;
import shapuc.util.Triple;

public class DEAFromNEAConstructorOneStep {

	/**
	 * Erzeugt zu NEA einen passenden DEA. 
	 * Erzeuge hier erst einmal nur einen leeren DEA und alle anderen Objekte, 
	 * die noch benötigt werden.
	 */
	public TokenizerDEA constructDEAFromNEA(Tokenizer tokenizer, TokenizerNEA nea) {
		
		nea.stateReachedCmd = INACTIVE_STATE;
		TokenizerDEA resultDEA = new  TokenizerDEA(CUSTOM_CHECK, ACTIVE_STATE);
		
		HashMap<TokenizerState, HashSet<TokenizerState>> stateSets = 
				new HashMap<TokenizerState, HashSet<TokenizerState>>();

		nea.start();
		
		HashSet<TokenizerState> startStates = 
				(HashSet<TokenizerState>) nea.getCurrentStates().clone();
		
		TokenizerState startStateForDEA = 
				nea.getStateWithHighestPriority(startStates).clone();
		
		stateSets.put( 
				startStateForDEA,
				startStates);
		
		resultDEA.addState(startStateForDEA);
		resultDEA.setStartState(startStateForDEA);
		
		
		HashMap<TokenizerState, HashSet<TokenizerState>> nonexploredStates 
			= new HashMap<TokenizerState, HashSet<TokenizerState>>();
		
		nonexploredStates.put(startStateForDEA, startStates);
		/* 
		 * NEA und DEA haben Startzustand, jetzt müssen Folgezustände exploriert werden...
		 * ....also NEA traversieren.
		 */
		traverseAutomata(nea, resultDEA, nonexploredStates, stateSets);
		
		return resultDEA;
	}
	
	/**
	 * Traverses the delivered nea and stores the result into the result dea.
	 * 
	 * The HashMap stateSets contains the sets of states reached by a transition. The 
	 * key is the state in the set with the highest priority.
	 * 
	 * Traversing starts with the current states (which are already contains in the stateSets).
	 * 
	 * The local variable newStateSets contains the set of states that still need to be traversed. 
	 * @param nonexploredStates 
	 * 
	 */
	private void traverseAutomata(TokenizerNEA nea,
			TokenizerDEA resultDEA,
			HashMap<TokenizerState,HashSet<TokenizerState>> nonexploredStates, 
			HashMap<TokenizerState,HashSet<TokenizerState>> stateSets) {
		
		while (!nonexploredStates.isEmpty()) {
			
			HashMap<TokenizerState,HashSet<TokenizerState>> loopNewStates = 
					(HashMap<TokenizerState,HashSet<TokenizerState>>) nonexploredStates.clone();	
			
			nonexploredStates = new HashMap<TokenizerState,HashSet<TokenizerState>>();
			
			for (TokenizerState nonexploredState :  loopNewStates.keySet()) {

				/* Setzte NEA auf neu zu explorierenden Zustand
				 * -> die Spontanübergänge sind bereits enthalten!
				 */
				nea.setCurrentStates(stateSets.get(nonexploredState));
				
				/**
				 * Gehe nun von aktueller Zustandsmenge alle Transitionen ab.
				 *   - ermittle Folgezustände (inkl. Spontanübergänge) 
				 *     - wenn Folgezustände noch nicht in stateSet enthalten sind, füge sie in 
				 *       newStateSets ein (werden im nächsten Durchgang in stateSet gesetzt)
				 */
				nea.traverseCurrentNonSpontaneousTransitionsAndAddToDEA(
						nonexploredState, nea.getCurrentStates(), resultDEA, stateSets, nonexploredStates);
				
			}
			
		}
		
	}


	/**
	 * Gehe nun von aktuellem Zustand alle Transitionen ab.
	 *   - ermittle Folgezustände (inkl. Spontanübergänge) 
	 *     - wenn Folgezustände noch nicht in stateSet enthalten sind, füge sie in 
	 *       newStateSets ein (werden im nächsten Durchgang in stateSet gesetzt)
	 */
	private void traverseFollowingNonSpontaneousTransitions(
			TokenizerNEA nea,
			TokenizerDEA resultDEA,
			HashMap<TokenizerState, HashSet<TokenizerState>> stateSets, 
			HashSet<HashSet<TokenizerState>> newStateSets) {
		
		Vector<Pair<CustomTransitionHandler<Character>, TokenizerState>> transitions = 
				nea.allNonSpontaneousTransitionsFromCurrent();

		nea.traverseCurrentNonSpontaneousTransitionsAndAddToDEA(resultDEA, stateSets, newStateSets);
		
	}

	
	/**
	 * Returns all Characters accepted by the single-Char transitions in the NEA
	 */
	private Set<Character> getSingleCharTransitions(
			Vector<Pair<CustomTransitionHandler<Character>, TokenizerState>> transitions) {

		Set<Character> ret = new HashSet<Character>();
		
		for (Pair<CustomTransitionHandler<Character>, TokenizerState> pair : transitions) {
			
			CustomTransitionHandler<Character> transition = pair.getLeft();
			
			if(transition.isSingleCharTransitions() && !(ret.contains(transition))) {
				ret.add(((CharTransition) transition).getAcceptanceChar());
			}
		}
		return ret;
	}

//	private TokenizerState getStateWithHighestPriority(Set<TokenizerState> aState) {
//		TokenizerState ret = (TokenizerState) aState.toArray()[0];
//		for (TokenizerState tokenizerState : aState) {
//			if (tokenizerState.isAcceptor()) {
//				if (!ret.isAcceptor()) {
//					ret = tokenizerState;
//				} else {
//					if (tokenizerState.getPriority() < ret.getPriority()) {
//						ret = tokenizerState;
//					}
//				}
//			}
//		}
//		return ret;
//	}
	

}
