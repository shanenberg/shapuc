package shapuc.scanning.tokenizer;

import java.util.Vector;

import shapuc.automata.AutomataFactoryParameters;
import shapuc.automata.NEA;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.tokenizers.NEAForTokenCreationVisitor;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.scanning.tokenizers.TokenizerStateInactive;
import shapuc.util.Pair;

public class NEACombinator {

	public TokenizerNEA combineNEAs(
			Tokenizer tokenizer,
			Vector<TokenizerNEA> allNEAS) {

		TokenizerNEA nea = Tokenizer.createNEA();

		nea.setSpontaneousTransition(NEAForTokenCreationVisitor.spontaneousTransition);
		TokenizerStateInactive start = new TokenizerStateInactive(-1, tokenizer);
		TokenizerStateInactive end = new TokenizerStateInactive(-1, tokenizer);
		nea.setStartState(start);

		for (NEA<TokenizerState, CustomTransitionHandler<Character>, Character> aNEA : allNEAS) {
			nea.addSpontaneousTransition(start, aNEA.getStartState());
			
			for (TokenizerState anEndState : aNEA.getEndStates()) {
				// System.out.println("added transition ....");
				nea.addSpontaneousTransition(anEndState, end);
			}
			for (TokenizerState aState : aNEA.getAllStates()) {
				for (Pair<CustomTransitionHandler<Character>, TokenizerState> transitions : aNEA
						.getStateTransitionTable().get(aState)) {
					nea.addTransition(aState, transitions.getLeft(), transitions.getRight());
				}
			}
		}
		nea.stateReachedCmd = AutomataFactoryParameters.INACTIVE_STATE;
		nea.start();

		return nea;
	}

}
