package shapuc.scanning.tokenizer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import shapuc.automata.NEA;
import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.automata.custom.AnyWithoutTransition;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CharTransition;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.util.Pair;
import shapuc.util.Triple;
import shapuc.util.datastructures.IntervalChar;

public class TokenizerNEA extends NEA<TokenizerState, CustomTransitionHandler<Character>, Character> {

	public TokenizerNEA(TransitionCmd transitionHandlesAndAcceptsInputCmd, StateActivationCmd stateReachedCmd) {
		super(transitionHandlesAndAcceptsInputCmd, stateReachedCmd);
	}
	
	
	public void dropNonSponateousFollowUpTransitionsIntoCollector(DEAFollowUpTransitionStateCollector followupTransitionStateCollector) {

			for (Triple<TokenizerState, CustomTransitionHandler<Character>, TokenizerState> t : this.allNonSpontaneousTransitionsWithOrigin()) {
			if (t.getLeft() instanceof CharTransition) {
				followupTransitionStateCollector.addRangeCharTransition(
						new CharRangeTransition(
								new IntervalChar(
										((CharTransition) t.getMiddle()).getAcceptanceChar(), 
										((CharTransition) t.getMiddle()).getAcceptanceChar())),
						t.getRight());
			} else if (t.getLeft() instanceof CharRangeTransition) {
				followupTransitionStateCollector.addRangeCharTransition(
						((CharRangeTransition) t.getMiddle()), t.getRight());
			} else if (t.getLeft() instanceof AnyWithoutTransition) {
				followupTransitionStateCollector.addAnyWithoutTransition(
						((AnyWithoutTransition) t.getMiddle()), t.getRight());
			} else {
				throw new RuntimeException(
					"there must not by anything in there except " +
					"CharTransition, CharRangeTransition, and AnyWithoutTransition");
			}
		}
	}
	
	
	public Vector<Pair<CustomTransitionHandler<Character>, TokenizerState>> allCharRangeTransitions() {

		Vector<Pair<CustomTransitionHandler<Character>, TokenizerState>> ret = 
				new Vector<Pair<CustomTransitionHandler<Character>, TokenizerState>>();
		for (Pair<CustomTransitionHandler<Character>, TokenizerState> t : this.allNonSpontaneousTransitions()) {
			if (t.getLeft() instanceof CharTransition) {
				ret.add(t);
			}
		}
		
		return ret;
	}	

	/**
	 * Gehe nun von aktueller Zustandsmenge alle Transitionen ab.
	 *   - ermittle Folgezustände (inkl. Spontanübergänge) 
	 *     - wenn Folgezustände noch nicht in stateSet enthalten sind, füge sie in 
	 *       newStateSets ein (werden im nächsten Durchgang in stateSet gesetzt)
	 * @param hashSet 
	 * @param hashSet 
	 */	
	public void traverseCurrentNonSpontaneousTransitionsAndAddToDEA(
			TokenizerState currentlyExploredStateInDEA, 
			HashSet<TokenizerState> currentlyExploredStatesInNEA, 
			TokenizerDEA resultDEA,
			HashMap<TokenizerState, HashSet<TokenizerState>> stateSets, 
			HashMap<TokenizerState,HashSet<TokenizerState>> nonexploredStates) {

		DEAFollowUpTransitionStateCollector followupTransitionStateCollector = 
				new DEAFollowUpTransitionStateCollector();

		dropNonSponateousFollowUpTransitionsIntoCollector(followupTransitionStateCollector);
		
		Vector<Pair<IntervalChar, HashSet<TokenizerState>>> result = followupTransitionStateCollector.
				prepareForDEA(this, resultDEA, stateSets, nonexploredStates);
		
		for (Pair<IntervalChar, HashSet<TokenizerState>> pair : result) {
			/* Wenn Zustandsmenge noch unbekannt, dann 
			 * füge Zustand mit höchsterPriorität DEA hinzu.
			 * Setze neuen Zustand in unexplored States.
			 */
			TokenizerState nextDEAState;
			if (!stateSets.values().contains(pair.getRight())) {
				nextDEAState = this.getStateWithHighestPriority(pair.getRight()).clone();
				stateSets.put(nextDEAState, pair.getRight());
				nonexploredStates.put(nextDEAState, pair.getRight());
				resultDEA.addState(nextDEAState);

			} else {
				nextDEAState = this.getDEAStateFromStates(stateSets, pair.getRight());
			}
			
			// Transition muss noch hizugefügt werden!
			// resultDEA.addTransition(startState, transition, endState);
			resultDEA.addTransition(
					currentlyExploredStateInDEA, 
					new CharRangeTransition(pair.getLeft()), 
					nextDEAState);
		}
		
		this.setCurrentStates(currentlyExploredStatesInNEA);
	}
	
	private TokenizerState getDEAStateFromStates(
			HashMap<TokenizerState, HashSet<TokenizerState>> stateSets, 
			HashSet<TokenizerState> targetStates) {
		for (TokenizerState tokenizerState : stateSets.keySet()) {
			if (stateSets.get(tokenizerState).equals(targetStates))
				return tokenizerState;
		}
		throw new RuntimeException("State is unknown");
	}


	public TokenizerState getStateWithHighestPriority(Set<TokenizerState> aState) {
		TokenizerState ret = (TokenizerState) aState.toArray()[0];
		for (TokenizerState tokenizerState : aState) {
			if (tokenizerState.isAcceptor()) {
				if (!ret.isAcceptor()) {
					ret = tokenizerState;
				} else {
					if (tokenizerState.getPriority() < ret.getPriority()) {
						ret = tokenizerState;
					}
				}
			}
		}
		return ret;
	}	
	
	

}
