package shapuc.scanning.tokenizer;

import shapuc.automata.DEA;
import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.tokenizers.TokenizerState;

public class TokenizerDEA extends DEA<TokenizerState, CustomTransitionHandler<Character>, Character> {

	public TokenizerDEA(TransitionCmd transitionHandlesAndAcceptsInputCmd, StateActivationCmd stateReachedCmd) {
		super(transitionHandlesAndAcceptsInputCmd, stateReachedCmd);
	}

}
