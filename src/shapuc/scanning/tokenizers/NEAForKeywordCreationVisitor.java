package shapuc.scanning.tokenizers;

import shapuc.automata.NEA;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.regex.RegExLib;
import shapuc.regex.operations.RegExToNEACreationVisitor;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.Tokens;

public class NEAForKeywordCreationVisitor extends NEAForRegExCreationVisitor {
	public final String keyword;
	
	public NEAForKeywordCreationVisitor(int priority, Tokenizer tokenizer, String keyword) {
		super(Tokens.KEYWORD(), priority, tokenizer, RegExLib.createRegExForKeyword(keyword));
		this.keyword = keyword;
	}	 
	
	@Override
	protected TokenizerState createEndState() {
		return new TokenizerStateKeywordAcceptor(priority, tokenizer, keyword);
	}

}
