package shapuc.scanning.tokenizers;

import shapuc.scanning.Token;
import shapuc.scanning.TokenType;
import shapuc.scanning.Tokenizer;

public class TokenizerStateAcceptor extends TokenizerStateInactive implements TokenizerState {

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TokenizerStateAcceptor other = (TokenizerStateAcceptor) obj;
		if (lastPositionInStream != other.lastPositionInStream)
			return false;
		if (tokenType == null) {
			if (other.tokenType != null)
				return false;
		} else if (!tokenType.equals(other.tokenType))
			return false;
		return true;
	}

	public int lastPositionInStream = -1;
	
	TokenType tokenType;
	
	
	public TokenizerStateAcceptor(int priority, Tokenizer tokenizer, TokenType tokenType) {
		super(priority, tokenizer);
		this.tokenType = tokenType;
	}

	public boolean isAcceptor() {
		return true;
	}	
	
	public void doCommand(Integer startPositionInStream) {
		tokenizer.resultTokenStream.add(null /*new Token(tokenType, null, null)*/);
	} 

	/**
	 * If this state has been reached, ....
	 */
	public void reachedState() {
		tokenizer.lastActiveAcceptorState = this;
		lastPositionInStream = tokenizer.charStream.currentPosition;
	}

	public TokenizerStateAcceptor clone() {
		TokenizerStateAcceptor accept = 
				new TokenizerStateAcceptor(this.lastPositionInStream, tokenizer, 
						this.tokenType);
		return accept;
	}

}
