package shapuc.scanning.tokenizers;

import java.util.List;

import shapuc.automata.AutomataFactoryParameters;
import shapuc.automata.NEA;
import shapuc.automata.NEAFactory;
import shapuc.automata.custom.AnyWithoutTransition;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CharTransition;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.regex.operations.RegExToNEACreationVisitor;
import shapuc.scanning.TokenType;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.tokenizer.TokenizerNEA;

public abstract class NEAForTokenCreationVisitor extends 
	RegExToNEACreationVisitor {

	public static final CustomTransitionHandler<Character> spontaneousTransition = new CharTransition('?'); 
	
	final int priority; 
	final Tokenizer tokenizer;
	final TokenType tokenType;

	public NEAForTokenCreationVisitor(int priority,
			Tokenizer tokenizer, TokenType tokenType, boolean withActive) {
		super();
		this.priority = priority;
		this.tokenizer = tokenizer;
		this.tokenType = tokenType; 
	}	

	@Override
	protected TokenizerState createStartState() {
		return new TokenizerStateInactive(priority, tokenizer);
	}

	@Override
	protected TokenizerState createEndState() {
		return new TokenizerStateAcceptor(priority, tokenizer, tokenType);
	} 

	@Override
	protected TokenizerState createInnerState() {
		return new TokenizerStateInactive(priority, tokenizer);
	}

	@Override
	protected CustomTransitionHandler<Character> createSpontaneousTransition() {
		return spontaneousTransition;
	}

	@Override
	protected CustomTransitionHandler<Character> createTransition(Character val) {
		return new CharTransition(val);
	}
	
	@Override
	protected CustomTransitionHandler<Character> createAnyWithoutTransition(List<Character> val) {
		return new AnyWithoutTransition(val);
	}	
	
	@Override
	protected CustomTransitionHandler<Character> createLiteralGroupTransition(Character start, Character end) {
		return new CharRangeTransition(start, end);
	}		
	
	protected void initializeNEA() { 
		nea = Tokenizer.createNEA();
		nea.setSpontaneousTransition(createSpontaneousTransition());
		nea.setStartState(createStartState());
		nea.addEndState(createEndState());
	}
	
	public abstract void runVisitorForNEACreation();	
	
	public TokenizerNEA createNEA() {
		this.initializeNEA();
		this.runVisitorForNEACreation();
		nea.stateReachedCmd = AutomataFactoryParameters.INACTIVE_STATE;
		this.nea.start();
		return this.nea;
	}


}
