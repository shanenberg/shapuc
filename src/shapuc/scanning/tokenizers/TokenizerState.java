package shapuc.scanning.tokenizers;

public interface TokenizerState {
	public void reachedState();
	public boolean isAcceptor();
	public int getPriority();
	public TokenizerState clone();
}
