package shapuc.scanning.tokenizers;

import shapuc.automata.NEA;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.regex.RegEx;
import shapuc.regex.RegExLib;
import shapuc.regex.operations.RegExToNEACreationVisitor;
import shapuc.scanning.TokenType;
import shapuc.scanning.Tokenizer;

public class NEAForRegExCreationVisitor extends NEAForTokenCreationVisitor {
	public final RegEx regex;
	
	public NEAForRegExCreationVisitor(TokenType tokenType, int priority, Tokenizer tokenizer, RegEx regEx) {
		super(priority, tokenizer, tokenType, true);
		regex =  regEx;
	}	 

	@Override
	protected TokenizerState createEndState() {
		return new TokenizerStateRegExAcceptor(priority, tokenizer, tokenType, regex.regExString()); 
	}
	
	public void runVisitorForNEACreation() {
		regex.accept(this, this.params(nea.getStartState(), (TokenizerState) (nea.getEndStates().toArray()[0])));
	}

}
