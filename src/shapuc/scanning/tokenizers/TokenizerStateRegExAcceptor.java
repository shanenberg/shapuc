package shapuc.scanning.tokenizers;

import shapuc.automata.ActiveState;
import shapuc.scanning.TokenTypeRegEx;
import shapuc.scanning.Token;
import shapuc.scanning.TokenType;
import shapuc.scanning.Tokenizer;

public class TokenizerStateRegExAcceptor extends TokenizerStateAcceptor {

	public String regexString;
	
	public TokenizerStateRegExAcceptor(int priority, Tokenizer tokenizer, TokenType tokenType, String regexString) {
		super(priority, tokenizer, tokenType);
		
		this.tokenType = tokenType;
		this.regexString = regexString;
	}
	
	public TokenizerStateRegExAcceptor clone() {
		TokenizerStateRegExAcceptor accept = 
				new TokenizerStateRegExAcceptor(priority, tokenizer, tokenType, regexString);
		return accept;
	}		

	public void reachedState() {
		super.reachedState();
	}

	public boolean isAcceptor() {
		return true;
	}

	public int getPriority() {
		return priority;
	}
	
	public String toString() {
		return "TokenizerStateRegExAcceptor[" + "priority " + priority + " " + super.toString() + "]";
	}
	
	public void doCommand(Integer startPositionInStream) {
		String string = tokenizer.charStream.elementsToString(startPositionInStream, lastPositionInStream);
		tokenizer.resultTokenStream.add(new Token(tokenType, string, startPositionInStream, lastPositionInStream));
		tokenizer.charStream.currentPosition = lastPositionInStream;
		// System.out.println("ADDED TOKEN");
	}

}
