package shapuc.scanning.tokenizers;

import shapuc.automata.ActiveState;
import shapuc.scanning.Token;
import shapuc.scanning.TokenType;
import shapuc.scanning.Tokenizer;

public class TokenizerStateSeparatorAcceptor extends TokenizerStateAcceptor {

	public TokenizerStateSeparatorAcceptor(int priority, Tokenizer tokenizer, TokenType tokenType) {
		super(priority, tokenizer, tokenType);
	}
	
	public TokenizerStateSeparatorAcceptor clone() {
		TokenizerStateSeparatorAcceptor accept = 
				new TokenizerStateSeparatorAcceptor(priority, tokenizer, tokenType);
		return accept;
	}		

	public void reachedState() {
		super.reachedState();
	}

	public boolean isAcceptor() {
		return true;
	}

	public int getPriority() {
		return priority;
	}
	
	public String toString() {
		return "TokenizerStateSeparatorAcceptor[" + "priority " + priority + " " + super.toString() + "]";
	}
	
	public void doCommand(Integer startPositionInStream) {
		String string = tokenizer.charStream.elementsToString(startPositionInStream, lastPositionInStream);
		tokenizer.charStream.currentPosition = lastPositionInStream;
	}

}
