package shapuc.scanning.tokenizers;

import shapuc.scanning.Token;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.Tokens;

/**
 * I am the Acceptor for a keyword. Since I know the word upfront, I can directly create the 
 * Token without doing any more operations in the inputStream.
 * 
 * @author stefan
 *
 */
public class TokenizerStateKeywordAcceptor extends TokenizerStateAcceptor {

	public final String keyword;

	public TokenizerStateKeywordAcceptor(int priority, Tokenizer tokenizer, String keyword) {
		super(priority, tokenizer, Tokens.KEYWORD());
		this.keyword = keyword;
	}

	@Override
	public void doCommand(Integer startPositionInStream) {
//System.out.println("RUN COMMAND");		
		tokenizer.resultTokenStream.add(
				new Token(tokenType, keyword, startPositionInStream, lastPositionInStream));
		tokenizer.charStream.currentPosition = lastPositionInStream + 1; // and now set the stream position
//System.out.println("ADDED TOKEN");		
	}
	
	@Override
	public void reachedState() {
//System.out.println("REACHED TokenizerStateKeywordAcceptor: " + keyword);
		tokenizer.lastActiveAcceptorState = this;
		// I am invoked after the char has been read....hence -1
		lastPositionInStream = tokenizer.charStream.currentPosition - 1; 
	}	
	
	public TokenizerStateKeywordAcceptor clone() {
		TokenizerStateKeywordAcceptor accept = 
				new TokenizerStateKeywordAcceptor(priority, tokenizer, keyword);
		return accept;
	}	

	
}
