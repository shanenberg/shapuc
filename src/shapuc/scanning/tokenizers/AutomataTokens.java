package shapuc.scanning.tokenizers;

import shapuc.scanning.TokenType;

public enum AutomataTokens implements TokenType {
	FINALIZER;

	public boolean isKeyword() {
		return false;
	}

	public String getTokenTypeName() {
		return "FINALIZER";
	}
}
