package shapuc.scanning.tokenizers;

import shapuc.automata.ActiveState;
import shapuc.scanning.Tokenizer;
import shapuc.util.cmd.CmdVoidP2;

public class TokenizerStateInactive implements  CmdVoidP2<Integer, Integer>, ActiveState, TokenizerState{

	final int priority;
	public int getPriority() {
		return priority;
	}

	final Tokenizer tokenizer;

	public TokenizerStateInactive(int priority, Tokenizer tokenizer) {
		super();
		this.priority = priority;
		this.tokenizer = tokenizer; 
	}	
	
	public TokenizerStateInactive clone() {
		TokenizerStateInactive accept = 
				new TokenizerStateInactive(priority, tokenizer);
		return accept;
	}		
	
	
	
	public boolean isAcceptor() {
		return false;
	} 

	public void doCommand(Integer startPositionInStream, Integer endPositionInStream) {
		// I am not an acceptanceState....I do nothing.
		throw new TokenizingException("A non acceptor tries to be evaluted...");
	}

	/** Nothing to be done here....I do not accept any state */
	public void reachedState() {}
	
	@Override
	public String toString() {
		return "TokenizerStateInactive [priority=" + priority + ", tokenizer=" + tokenizer + "]";
	}

	
}
