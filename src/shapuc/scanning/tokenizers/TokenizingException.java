package shapuc.scanning.tokenizers;

public class TokenizingException extends RuntimeException {

	private static final long serialVersionUID = -1514783454216665893L;

	public TokenizingException(String message) {
		super(message);
	}
	

}
