package shapuc.scanning.tokenizers;

import shapuc.automata.ActiveState;
import shapuc.scanning.Token;
import shapuc.scanning.Tokenizer;
import shapuc.streams.PositionStream;
import shapuc.util.cmd.CmdP2;
import shapuc.util.cmd.CmdVoidP2;

public class TokenizerStateFinalizer extends TokenizerStateAcceptor {

	public TokenizerStateFinalizer(Tokenizer tokenizer) {
		super(-1, tokenizer, AutomataTokens.FINALIZER);
	}

	public void doCommand(Integer startPositionInStream, Integer endPositionInStream) {
		throw new TokenizingException("Noone should ever execute the finalizer!!!!");
	}

	/** Nothing to be done here....I do not accept any state */
	public void reachedState() {
		
		tokenizer.wantsToExit = true;

		if (tokenizer.lastActiveAcceptorState==null) {
			tokenizer.charStream.currentPosition = tokenizer.startPositionInStream;
//			throw new TokenizingException(
//					"Not a single token accepts the input: ");
		} else {
			tokenizer.lastActiveAcceptorState.doCommand(tokenizer.startPositionInStream);
			tokenizer.lastActiveAcceptorState = null;
			tokenizer.startPositionInStream = tokenizer.lastPositionInStream;
		}
	}
	
	public TokenizerStateFinalizer clone() {
		TokenizerStateFinalizer accept = 
				new TokenizerStateFinalizer(tokenizer);
		return accept;
	}	
	
}
