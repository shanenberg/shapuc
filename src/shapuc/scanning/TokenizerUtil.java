package shapuc.scanning;

import shapuc.automata.ActiveState;
import shapuc.automata.AutomataFactoryParameters;
import shapuc.automata.NEA;
import shapuc.automata.NEAFactory;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.tokenizers.TokenizerStateInactive;
import shapuc.util.Pair;

public class TokenizerUtil {
	
//	public void createAutomataFromNEAs(NEA<ActiveState, CustomTransitionHandler<Character>, Character>...inputNEAs) {
//		
//		AutomataFactoryParameters p = new AutomataFactoryParameters();
//		
//		NEA<ActiveState, CustomTransitionHandler<Character>, Character> nea = 
//				new NEAFactory<ActiveState, CustomTransitionHandler<Character>, Character>(p.CUSTOM_CHECK, p.ACTIVE_STATE)
//				.createUninitializedAutomata();
//		
//		TokenizerStateInactive start = new TokenizerStateInactive(-1, this);
//		nea.setStartState(start);
//		
//		TokenizerStateInactive end = new TokenizerStateInactive(-1, this);
//		nea.addEndState(end);
//		
//		for (int i = 0; i < inputNEAs.length; i++) {
//			addNea(nea, inputNEAs[i]);
//		}
//		
//	}	
	
//	public static void addNea(NEA<ActiveState, CustomTransitionHandler<Character>, Character> resultNea,
//			NEA<ActiveState, CustomTransitionHandler<Character>, Character> inputNEA) {
//		
//		// Füge alle Transitionen (und damit States hinzu)
//		for (ActiveState state: inputNEA.getStateTransitionTable().keySet()) {
//			for (Pair<CustomTransitionHandler<Character>, ActiveState> transition: inputNEA.getStateTransitionTable().get(state)) {
//				resultNea.addTransition(state, transition.getLeft(), transition.getRight());
//			}
//		}
//
//		// Spontaneous Transition to startState
//		resultNea.addSpontaneousTransition(resultNea.getStartState(), inputNEA.getStartState());
//		
//		for (ActiveState inputEndState : inputNEA.getEndStates()) {
//			for (ActiveState resultEndState : resultNea.getEndStates()) {
//				resultNea.addSpontaneousTransition(inputEndState, resultEndState);
//			}
//		}
//	}
}
