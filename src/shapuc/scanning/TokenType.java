package shapuc.scanning;

public interface TokenType {
	public boolean isKeyword();
	public String getTokenTypeName();
}
