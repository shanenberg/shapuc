/*******************************************************************************
 * /**
 * * The MIT License (MIT)
 * *
 * * Copyright (c) 2015, 2016 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * * 
 * * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * * software and associated documentation files (the "Software"), to deal in the Software 
 * * without restriction, including without limitation the rights to use, copy, modify, 
 * * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * * The above copyright notice and this permission notice shall be included in all copies or 
 * * substantial portions of the Software.
 *
 * * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * **/

package shapuc.scanning;

import shapuc.regex.RegEx;

public class TokenTypeRegEx implements TokenType {
	
	public final String tokenName;
	public RegEx regex;
	
	public TokenTypeRegEx(String tokenName, RegEx regexString) {
		this.tokenName = tokenName;
		this.regex = regexString;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof TokenTypeRegEx))
			return false;
		TokenTypeRegEx thatType = (TokenTypeRegEx) obj;

		return thatType.regex.equals(this.regex);
	
	}

	@Override
	public String toString() {
		return "RegExTokenType [tokenTypeName = " + this.tokenName + ", " + " regEx=" + regex + "]";
	}

	public boolean isKeyword() {
		return false;
	}

	public String getTokenTypeName() {
		return tokenName;
	}


}
