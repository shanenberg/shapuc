package shapuc.scanning;

import java.util.HashSet;

public class DisjointInputsResults {

	public HashSet<Character> singleAcceptedCharsForTransitions = new HashSet<Character>();
	public HashSet<Character> exceptions = new HashSet<Character>(); 
	public HashSet<Character> exceptionDisjoints = new HashSet<Character>(); 
	public boolean hasAllQuantifiedTransition = false;

	@Override
	public String toString() {
		return "DisjointInputsResults [singleAcceptedCharsForTransitions=" + singleAcceptedCharsForTransitions
				+ ", exceptions=" + exceptions + ", exceptionDisjoints=" + exceptionDisjoints
				+ ", hasAllQuantifiedTransition=" + hasAllQuantifiedTransition + "]";
	}


}
