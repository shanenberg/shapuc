package shapuc.scanning;

import shapuc.automata.Automata;
import shapuc.automata.NEA;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.regex.RegEx;
import shapuc.regex.RegExLib;
import shapuc.regex.operations.RegExToNEACreationVisitor;
import shapuc.scanning.tokenizers.NEAForKeywordCreationVisitor;
import shapuc.scanning.tokenizers.NEAForRegExCreationVisitor;
import shapuc.scanning.tokenizers.TokenizerState;

public class NEAFromKeywordTokenConstructor {
	
	Automata<Object, CustomTransitionHandler<Character>, Character>  
		createAutomata(Tokenizer tokenizer, RegEx...regEx) {
		return null;
	}
	
	public Automata<Object, CustomTransitionHandler<Character>, Character>  
	createSilentAutomataForSingleRegEx(Tokenizer tokenizer, RegEx regEx) {

		return null;

	}
	
	public Automata<TokenizerState, CustomTransitionHandler<Character>, Character>  
	createInitializedKeywordNEA(Tokenizer tokenizer, String keywordString) {

		NEAForRegExCreationVisitor neaForKeyWordVisitor = new NEAForKeywordCreationVisitor(1, tokenizer, keywordString);
		RegEx keyWord = RegExLib.createRegExForKeyword(keywordString);
		
		System.out.println(neaForKeyWordVisitor.nea.getEndStates().toArray()[0]);
		
		keyWord.accept(
			neaForKeyWordVisitor,
			neaForKeyWordVisitor.params(
					neaForKeyWordVisitor.nea.getStartState(), 
					(TokenizerState) neaForKeyWordVisitor.nea.getEndStates().toArray()[0])
		);
		
		neaForKeyWordVisitor.nea.start();
		
		return neaForKeyWordVisitor.nea;

	}	
	
}

