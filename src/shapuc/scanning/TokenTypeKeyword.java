package shapuc.scanning;

public class TokenTypeKeyword implements TokenType {
	public static TokenTypeKeyword keyword = new TokenTypeKeyword();
	private TokenTypeKeyword(){}
	public boolean isKeyword() {
		return true;
	}
	public String getTokenTypeName() {
		return "KEYWORD";
	}
}
