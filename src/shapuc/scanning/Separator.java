package shapuc.scanning;

import shapuc.regex.RegEx;
import shapuc.regex.operations.RegExFactory;

public class Separator {
	public final RegEx regularExpression;

	public Separator(RegEx regularExpression) {
		super();
		this.regularExpression = regularExpression;
	}
	
	public static Separator Char(final Character aChar) {
		return new Separator(new RegExFactory(){public RegEx regEx() {
			return TOKEN(aChar);}}.regEx());	
	}
	
}
