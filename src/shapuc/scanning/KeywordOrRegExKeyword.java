package shapuc.scanning;

public class KeywordOrRegExKeyword extends KeywordOrRegEx {
	
	public String keyword;

	public KeywordOrRegExKeyword(String keyword) {
		super();
		this.keyword = keyword;
	}

	public boolean matches(Token<?> aToken) {
		if (aToken.getTokenType().isKeyword()) {
			
//			System.out.println("COMPARE " + aToken.getValueString());			
//			System.out.println("COMPARE " + keyword);			
//			
			if (aToken.getValueString().equals(keyword))
				return true;
		}
		return false;
	}
	
	
}
