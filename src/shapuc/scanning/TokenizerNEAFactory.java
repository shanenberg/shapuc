package shapuc.scanning;

import shapuc.automata.NEAFactory;
import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.tokenizer.TokenizerNEA;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.util.Pair;
import shapuc.util.cmd.CmdP2;
import shapuc.util.cmd.CmdVoidP1;
import shapuc.util.returnObject.ReturnObject;

public class TokenizerNEAFactory
		extends NEAFactory<TokenizerState, CustomTransitionHandler<Character>, Character>  {

	public TokenizerNEAFactory(TransitionCmd transitionCmd, StateActivationCmd stateCmd) {
		super(transitionCmd, stateCmd);
	}

	@Override
	public TokenizerNEA createUninitializedAutomata() {
		return new TokenizerNEA(transitionCmd, stateCmd);
	}

	public void initializeAutomata(TokenizerNEA automata, TokenizerState start,
			CustomTransitionHandler<Character> spont, Object[][] transitions, TokenizerState[] ends) {

		automata.setSpontaneousTransition(spont);
		automata.setStartState(start);

		for (int i = 0; i < ends.length; i++) {
			automata.addEndState(ends[i]);
		}

		for (int i = 0; i < transitions.length; i++) {
			if (transitions[i].length == 2) {
				automata.addTransition((TokenizerState) transitions[i][0],
						(CustomTransitionHandler<Character>) automata.getSpontaneousTransition(), (TokenizerState) transitions[i][1]);
			} else {
				automata.addTransition((TokenizerState) transitions[i][0], (CustomTransitionHandler<Character>) transitions[i][1],
						(TokenizerState) transitions[i][2]);
			}
		}
	}
	
	
}