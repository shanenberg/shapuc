package shapuc.scanning;

import java.util.List;
import java.util.Vector;

import org.omg.Messaging.SyncScopeHelper;

import shapuc.automata.ActiveState;
import shapuc.automata.Automata;
import shapuc.automata.AutomataFactoryParameters;
import shapuc.automata.DEA;
import shapuc.automata.InvalidInputException;
import shapuc.automata.NEA;
import shapuc.automata.NEAFactory;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.tokenizer.TokenizerDEA;
import shapuc.scanning.tokenizer.TokenizerNEA;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.scanning.tokenizers.TokenizerStateAcceptor;
import shapuc.scanning.tokenizers.TokenizerStateFinalizer;
import shapuc.scanning.tokenizers.TokenizerStateInactive;
import shapuc.scanning.tokenizers.TokenizingException;
import shapuc.streams.PositionStream;
import shapuc.streams.StringBufferStream;
import shapuc.streams.TokenStream;
import shapuc.util.Pair;

public class Tokenizer {

	// This contains the automata for all: Keywords, Separators, etc.
	// Since Wildcards will be permitted, a CustomTransitionHandler is necessary.
	public TokenizerDEA
		automata; 

	public PositionStream<Character> charStream;
	
	public TokenStream<PositionStream<Character>> resultTokenStream;
	public TokenizerStateAcceptor lastActiveAcceptorState;
	
	public int lastPositionInStream = -1;
	public int startPositionInStream=-1;
	public boolean wantsToExit = false;
	
	public Tokenizer() {
		super();
	}	
	
	public Tokenizer(PositionStream<Character> charStream, TokenStream<PositionStream<Character>> resultTokenStream) {
		super();
		this.charStream = charStream; 
		this.resultTokenStream = resultTokenStream;
	}

	public void setStringStream(String aString) {
		StringBufferStream charInputStream = new StringBufferStream(aString);
		TokenStream tokenOutputStream = new TokenStream(charInputStream);
		this.charStream = charInputStream;
		this.resultTokenStream = tokenOutputStream;
		wantsToExit = false;
		lastPositionInStream = -1;
		startPositionInStream=-1;		
		
	}
	
	public void createTokenFromStream()	{
		startPositionInStream = charStream.currentPosition;
		automata.start();
		
		while (!wantsToExit) {
			
				if (charStream.isEOF()) {
					invokeFinalizerState();
					break;
				}
			
				Character next = charStream.next();
//System.out.println("NEXT: " + next);				
				automata.gotoNext(next);
		}
		
		wantsToExit = false;
	}

	private void invokeFinalizerState() {
		for (TokenizerState aState: automata.getAllStates()) {
			if (aState instanceof TokenizerStateFinalizer) {
				automata.reachedState(aState);
				return;
			}
		}
		throw new RuntimeException("Missing finalizer State!");
	}

	public static TokenizerNEA createNEA() {
		AutomataFactoryParameters p = new AutomataFactoryParameters();
		TokenizerNEA nea = new TokenizerNEAFactory(p.CUSTOM_CHECK, p.ACTIVE_STATE)
					.createUninitializedAutomata();
		return nea;
	}

	public void clean() {
		int startPositionInStreamTMP = startPositionInStream;
		resultTokenStream.clean();
		charStream.currentPosition = startPositionInStreamTMP;
		resultTokenStream.currentPosition = startPositionInStreamTMP;
		lastActiveAcceptorState = null;
		lastPositionInStream = -1;
		startPositionInStream=-1;
		wantsToExit = false;		
	}

	public void resetAutomata() {
		automata.start();
	}
	
}
