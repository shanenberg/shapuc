/**
* The MIT License (MIT)
*
* Copyright (c) 2015, 2016 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package shapuc.scanning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import shapuc.scanning.tokenizer.TokenizerFactory;
import shapuc.streams.TokenStream;

/* 
 * TOKEN_TYPES is the interface that contains the declaration of tokens.
*/
public class Scanner {
	
	public Tokenizer tokenizer;

	public Scanner(Tokenizer tokenizer) {
		super();
		this.tokenizer = tokenizer;
	}
	
	public Scanner(Object...keywordOrRegEx) {
		List l = Arrays.asList(keywordOrRegEx);
		tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l);
	}
	
	public Scanner(List keywordOrRegEx) {
		tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(keywordOrRegEx);
	}
	
	
	public void scan() {
		boolean progress = true;
		while(!tokenizer.charStream.isEOF()) {
			int pos = tokenizer.charStream.currentPosition;
			
			tokenizer.resetAutomata();
			tokenizer.createTokenFromStream();
			
			if (pos==tokenizer.charStream.currentPosition) {
				progress = false;
				throw new ScanException("No progress while scanning: " + pos, 
						tokenizer.charStream.elementsToString(0, tokenizer.charStream.currentPosition) + "\n>>>\n" + 
						tokenizer.charStream.elementsToString(tokenizer.charStream.currentPosition+1, tokenizer.charStream.length()-1)
				);
			}
		}
	}

	public TokenStream<?> tokenStream() {
		return tokenizer.resultTokenStream;
	}
	
	public void setInputString(String aString) {
		tokenizer.setStringStream(aString);
	}
	
	
}
