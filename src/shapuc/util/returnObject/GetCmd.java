package shapuc.util.returnObject;

public interface GetCmd<TYPE> {
	public TYPE get();
}

