package shapuc.util.returnObject;

public class ReturnObject<TYPE> {

	private TYPE result;

	public ReturnObject(TYPE result) {
		super();
		this.result = result;
	}

	public ReturnObject() {
		super();
		this.result = null;
	}	

	public final SetCmd<TYPE> setter = new SetCmd<TYPE>() {
		public void set(TYPE value) {
			result = value;
		}
	};

	public final GetCmd<TYPE> getter = new GetCmd<TYPE>() {
		public TYPE get() {
			return result;
		}
	};

	public TYPE getReturn() {
		return result;
	}
}
