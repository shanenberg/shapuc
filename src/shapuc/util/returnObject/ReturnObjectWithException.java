package shapuc.util.returnObject;

public class ReturnObjectWithException<TYPE, EXCEPTIONTYPE> extends ReturnObject<TYPE> {
	
	private EXCEPTIONTYPE exception = null;
	
	public final SetCmd<EXCEPTIONTYPE> setException = new SetCmd<EXCEPTIONTYPE>() {public void set(EXCEPTIONTYPE ex) {
		exception = ex;	
	}};		
	
	public final GetCmd<EXCEPTIONTYPE> getException = new GetCmd<EXCEPTIONTYPE>() {public EXCEPTIONTYPE get() {
		return exception;	
	}};		
	
	public boolean hasException() {
		return exception!=null;
	}
	
}
