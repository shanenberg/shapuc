package shapuc.util.returnObject;

public interface SetCmd<TYPE> {
	public void set(TYPE value);
}
