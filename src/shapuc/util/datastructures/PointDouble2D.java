/*******************************************************************************
 * /**
 * * The MIT License (MIT)
 * *
 * * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * * 
 * * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * * software and associated documentation files (the "Software"), to deal in the Software 
 * * without restriction, including without limitation the rights to use, copy, modify, 
 * * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * * The above copyright notice and this permission notice shall be included in all copies or 
 * * substantial portions of the Software.
 *
 * * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * **/
package shapuc.util.datastructures;

/*
 * Class for 2-dimensional double-Points
 */
public class PointDouble2D {
	public double x;
	public double y;

	public static PointDouble2D xy(double x, double y) {
		return new PointDouble2D(x, y);
	}

	public PointDouble2D(double _x, double _y) {
		x = _x;
		y = _y;
	}

	public PointDouble2D clone() {
		return new PointDouble2D(x, y);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PointDouble2D other = (PointDouble2D) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	public PointDouble2D scale(double s) {
		x = s * x;
		y = s * y;
		return this;
	}

	public PointDouble2D scaleNCopy(double s) {
		return new PointDouble2D(x * s, y * s);
	}

	public double getScaleXToFitCompletelyInto(double xFrame) {
		return xFrame / x;
	}

	public double getScaleYToFitCompletelyInto(double yFrame) {
		return yFrame / y;
	}

	public double getScaleToFitCompletelyInto(PointDouble2D p) {
		double xScale = getScaleXToFitCompletelyInto(p.x);
		double yScale = getScaleYToFitCompletelyInto(p.y);
		return Math.min(xScale, yScale);
	}

	public double getScaleToFitBorderedInto(PointDouble2D p) {
		double xScale = getScaleXToFitCompletelyInto(p.x);
		double yScale = getScaleYToFitCompletelyInto(p.y);
		return Math.max(xScale, yScale);
	}

	public PointDouble2D centralizationOffset(PointDouble2D p) {
		return new PointDouble2D((p.x - x) / 2f, (p.y - y) / 2f);
	}

	public PointDouble2D plus(PointDouble2D toAdd) {
		x = x + toAdd.x;
		y = y + toAdd.y;
		return this;
	}

	public PointDouble2D plusCopy(PointDouble2D toAdd) {
		return new PointDouble2D(x + toAdd.x, y + toAdd.y);
	}

	public PointDouble2D minus(PointDouble2D minus) {
		x = x - minus.x;
		y = y - minus.y;
		return this;
	}

	public PointDouble2D minusCopy(PointDouble2D minus) {
		return new PointDouble2D(x - minus.x, y - minus.y);
	}

	@Override
	public String toString() {
		return super.toString() + "...x:" + x + "...y:" + y;
	}

	public boolean widerThan(PointDouble2D frame) {
		return x >= frame.x;
	}

	public boolean higherThan(PointDouble2D frame) {
		return y >= frame.y;
	}

	public double distanceTo(PointDouble2D p) {
		double x = Math.abs(this.x - p.x);
		double y = Math.abs(this.y - p.y);

		return Math.sqrt((x * x) + (y * y));
	}
	
	public PointDouble2D[] connectionStartEnd(PointDouble2D p) {
		PointDouble2D[] ret = new PointDouble2D[2];
		ret[0] = this.clone();
		ret[1] = p.clone();
		return ret;
	}	
	
	public PointDouble2D[] connectionStartEnd(PointDouble2D p, double radius) {
		
		double diffX = p.x - this.x;
		double diffY = p.y - this.y;
		double hypothenuse = Math.sqrt((diffX*diffX)+(diffY*diffY));
		
		double offsetX = diffX*radius/hypothenuse;
		double offsetY = diffY*radius/hypothenuse;
		
		PointDouble2D[] ret = new PointDouble2D[2];
		ret[0] = new PointDouble2D(this.x + offsetX, this.y + offsetY);
		ret[1] = new PointDouble2D(p.x - offsetX, p.y - offsetY);
		
		return ret;
		
	}		

}
