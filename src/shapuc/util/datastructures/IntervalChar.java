package shapuc.util.datastructures;

import shapuc.automata.custom.CharRangeTransition;
import shapuc.util.Pair;

import static shapuc.util.datastructures.Interval.ContainmentType.*;

public class IntervalChar extends Interval<Character>{
	
	public IntervalChar(char start, char end) {
		if (start>end)
			throw new RuntimeException("Left side of interval larger than right side");
		this.start = start;
		this.end = end;
	}
	
	@Override
	public String toString() {
		return "IntervalChar [start=" + start + ", end=" + end + ", toString()=" + super.toString() + "]";
	}

	public boolean contains(Character c) {
		return start <= c && end >= c;
	}

	public ContainmentType overlapping(IntervalChar other) {
		// other beginnt rechts von this.begin
		if (start.charValue() < other.start.charValue()) {
			// other beginnt rechts von this.ende
			if (end.charValue() < other.start.charValue()) {
				return NONE;
			} else {
				// other beginnt links von this.ende (und rechts von this.begin)
				if (other.end.charValue() < end.charValue()) {
					return FULL_CONTAINMENT;
				} else if (other.end.charValue() > end.charValue()) {
						return PARTIAL_OVERLAP_RIGHT;
				} else {
					// other.end ist gleich this.end
					return FULL_CONTAINMENT_RIGHTBORDERED;
				}
			}
		// other beginnt links von this.begin
		} else if (other.start.charValue() < start.charValue()) {
			// other endet links von this.begin
			if (other.end.charValue() < start.charValue()) {
				return NONE;
			} else {
				// other endet links von this.end
				if (other.end.charValue() < end.charValue()) {
					return PARTIAL_OVERLAP_LEFT;
					// other beginnt links von this.begin und endet rechts von this.end
				} else if (other.end.charValue() > end.charValue()) {
						return FULL_INCLUDED_BY;
				} else {
					// other.end ist gleich this.end
					return FULL_INCLUDED_BY_RIGHTBORDERED;
				}
			}
		} else {
		// other.start und end.start sind gleich, also ist es included_By_leftBordered oder Full_Containment_LEFT
		// oder EQUALS
			if (other.end.charValue() < end.charValue()) {
				return FULL_CONTAINMENT_LEFTBORDERED;
			} else if (other.end.charValue() > end.charValue()) {
				return ContainmentType.FULL_INCLUDED_BY_LEFTBORDERED;
			} else {
				return EQUALS;
			}
		}
	}	
	
	public Pair<ContainmentType, IntervalChar[]> diffIntervals(IntervalChar comparedTo) {
		ContainmentType resultType = overlapping(comparedTo);
		switch (resultType) {
			case NONE: 
				return new Pair<ContainmentType, IntervalChar[]>(resultType, 
					new IntervalChar[] {
							comparedTo.clone(), this.clone()}); 
			case EQUALS: 
				return new Pair<ContainmentType, IntervalChar[]>(resultType, 
						new IntervalChar[] {
								this.clone()}); 
			// bei FullContainment werden drei Intervalle zurückgegeben: 1. 
			case FULL_CONTAINMENT: 
				return new Pair<ContainmentType, IntervalChar[]>(resultType, 
						new IntervalChar[] {
								new IntervalChar(comparedTo.start, comparedTo.end),
								new IntervalChar(this.start, (char) (comparedTo.start-1)),
								new IntervalChar((char) (comparedTo.end+1), this.end)});
			case FULL_CONTAINMENT_LEFTBORDERED:
				return new Pair<ContainmentType, IntervalChar[]>(resultType, 
						new IntervalChar[] {
								new IntervalChar(comparedTo.start, comparedTo.end),
								new IntervalChar((char) (comparedTo.end+1), this.end)});
			case FULL_CONTAINMENT_RIGHTBORDERED:
				return new Pair<ContainmentType, IntervalChar[]>(resultType, 
						new IntervalChar[] {
								new IntervalChar(this.start, (Character) ((char) (comparedTo.start-1))),
								new IntervalChar(comparedTo.start, comparedTo.end)});
			case FULL_INCLUDED_BY:
				return new Pair<ContainmentType, IntervalChar[]>(resultType, 
						new IntervalChar[] {
								new IntervalChar((Character) ((char) (comparedTo.start)), (Character) ((char) (this.start-1))),
								new IntervalChar((Character) ((char) (this.end+1)), (Character) ((char) (comparedTo.end))),
								new IntervalChar((Character) ((char) (this.start)), this.end)});
			case FULL_INCLUDED_BY_LEFTBORDERED:
				return new Pair<ContainmentType, IntervalChar[]>(resultType, 
						new IntervalChar[] {
								new IntervalChar((Character) ((char) (this.end+1)), comparedTo.end),
								new IntervalChar(this.start, this.end)});
			case FULL_INCLUDED_BY_RIGHTBORDERED:
				return new Pair<ContainmentType, IntervalChar[]>(resultType, 
						new IntervalChar[] {
								new IntervalChar(comparedTo.start, (Character) ((char) (this.start-1))),
								new IntervalChar(this.start, this.end)});
			case PARTIAL_OVERLAP_LEFT:
				return new Pair<ContainmentType, IntervalChar[]>(resultType, 
						new IntervalChar[] {
								new IntervalChar(comparedTo.start, (Character) ((char) (this.start-1))),
								new IntervalChar(this.start, comparedTo.end),
								new IntervalChar((Character) ((char) (comparedTo.end+1)), this.end)});
			case PARTIAL_OVERLAP_RIGHT:
				return new Pair<ContainmentType, IntervalChar[]>(resultType, 
						new IntervalChar[] {
								new IntervalChar(comparedTo.start, this.end),
								new IntervalChar((Character) ((char) (this.end+1)), comparedTo.end),
								new IntervalChar(this.start, (Character) ((char) (comparedTo.start-1)))});
				
		}
		throw new RuntimeException("Something is wrong.....I do not know whether the Intervals overlap");
	}
	
	public IntervalChar clone() {
		return new IntervalChar(start.charValue(), end.charValue());
	}

	public IntervalChar[] diffWithRemovedChar(char removedChar) {
		if (!this.contains(removedChar))
			throw new RuntimeException("Interval does not contain char: " + removedChar);
		
		if (this.start.equals(removedChar) && this.end.equals(removedChar)) {
			return new IntervalChar[] {null};
		} else if (this.start.equals(removedChar)) {
			return new IntervalChar[] {new IntervalChar((char) (this.start+1), this.end)};
		} else if (this.end.equals(removedChar)) {
			return new IntervalChar[] {new IntervalChar(this.start, (char) (this.end-1))};
		} else {
			return new IntervalChar[] {
				new IntervalChar(this.start, (char) (removedChar-1)),
				new IntervalChar((char) (removedChar+1), this.end)};
		}
	}
	
	
}
