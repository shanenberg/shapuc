package shapuc.util.datastructures;

import shapuc.automata.custom.CharRangeTransition;

public class Interval<NumberType> {
	
	public static enum ContainmentType {
		NONE, 
		EQUALS, 

		FULL_CONTAINMENT, 
		FULL_CONTAINMENT_RIGHTBORDERED,
		FULL_CONTAINMENT_LEFTBORDERED,
		
		PARTIAL_OVERLAP_LEFT, 
		PARTIAL_OVERLAP_RIGHT, 
		
		FULL_INCLUDED_BY, 
		FULL_INCLUDED_BY_RIGHTBORDERED, 
		FULL_INCLUDED_BY_LEFTBORDERED 
		;
	}
	
	public NumberType start;
	public NumberType end;		
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Interval other = (Interval) obj;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		return true;
	}

	
}
