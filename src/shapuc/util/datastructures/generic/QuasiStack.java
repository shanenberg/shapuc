package shapuc.util.datastructures.generic;
 
public class QuasiStack<Element> {
	StackedElement<Element> first = null;
	StackedElement<Element> last = null;
	
	public StackedElement<Element> push(Element e) {
		if (last==null) {
			last = new StackedElement<Element>(e, null, null);
			first = last;
		} else {
			last.next = new StackedElement<Element>(e, last, null);
			last = last.next;
		}
		return last; 
	}
	
	public StackedElement<Element> pop() {
		StackedElement<Element> ret = last;
		last = last.before;
		if (last==null) {
			first = null;
		} else {
			last.next = null;
		}
		return ret;
	}
	
	public int size() {
		int count = 0;
		StackedElement<Element> current = first;
		while (current != null) {
			current = current.next;
			count++;
		}		
		return count;
	}
	
	public void removeAllAfter(StackedElement<Element> element) {
		element.next = null;
		last = element;
	}
	
	public boolean isEmpty() {
		return first==null;
	}

	public StackedElement<Element> peek() {
		return last;
	}
	
	public void print() {
		StackedElement<Element> current = first;
		while (current != null) {
			System.out.println(current);
			current = current.next;
		}
	}

	public void cleanAfter(StackedElement<Element> meOnStack) {
		last = meOnStack;
		meOnStack.next = null;
	}

	public void clean() {
		last = null;
		first = null;
	}
	
}