package shapuc.util.datastructures.generic;

public interface SelfRef<NODE_TYPE> {
	NODE_TYPE getChild();
	void setChild(NODE_TYPE p);
}
