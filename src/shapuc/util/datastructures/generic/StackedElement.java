package shapuc.util.datastructures.generic;


public class StackedElement<Element> {
	
	public StackedElement(Element content, StackedElement<Element> before, StackedElement<Element> nect) {
		super();
		this.content = content;
		this.before = before;
		this.next = next;
	}
	public Element content;
	public StackedElement<Element> next = null;
	public StackedElement<Element> before = null;
	
	public String toString() {
//		new RuntimeException("blblb").printStackTrace();
		return "StackedElement: " + content;
	}
}
