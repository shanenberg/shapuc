package shapuc.util.datastructures.generic;

import java.util.Collection;

public interface NAry<NODE_TYPE, COLLECTION_TYPE extends Collection<NODE_TYPE>> {
	COLLECTION_TYPE getChildren();
}
