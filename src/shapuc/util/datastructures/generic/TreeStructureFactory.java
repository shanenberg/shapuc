package shapuc.util.datastructures.generic;

import java.util.Collection;

public interface TreeStructureFactory<
		TYPE, 
		CHILDRENTYPE extends Collection<TYPE>,
		SELFREFTYPE extends TYPE> {
	
	public TYPE createElement();
	public NAry<TYPE, CHILDRENTYPE> createNElement();
	public SELFREFTYPE createSelfRefElement();	
	
}
