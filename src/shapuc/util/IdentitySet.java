/*******************************************************************************
 * /**
 * * The MIT License (MIT)
 * *
 * * Copyright (c) 2015, 2016 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * * 
 * * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * * software and associated documentation files (the "Software"), to deal in the Software 
 * * without restriction, including without limitation the rights to use, copy, modify, 
 * * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * * The above copyright notice and this permission notice shall be included in all copies or 
 * * substantial portions of the Software.
 *
 * * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * **/
package shapuc.util;

import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Set;

public class IdentitySet<ElementType> implements Set<ElementType>{

	IdentityHashMap<ElementType, Object> elements = new IdentityHashMap<ElementType, Object>();
	
	public int size() {
		return elements.size();
	}

	public boolean isEmpty() {
		return elements.isEmpty();
	}

	public boolean contains(Object o) {
		return elements.keySet().contains(o);
	}

	public Iterator<ElementType> iterator() {
		return elements.keySet().iterator();
	}

	public Object[] toArray() {
		return elements.keySet().toArray();
	}

	public <T> T[] toArray(T[] a) {
		return elements.keySet().toArray(a);
	}

	public boolean add(ElementType e) {
		boolean changed = false;
		if (!this.contains(e))
			changed = true;
		elements.put(e, null);
		return changed;
	}

	public boolean remove(Object o) {
		elements.remove(o);
		return false;
	}

	public boolean containsAll(Collection<?> c) {
		
//		System.out.println("elements" + this);
//		System.err.println("c " + c);
		
		return elements.keySet().containsAll(c);
	}

	public boolean addAll(Collection<? extends ElementType> c) {
		boolean changed = false;
		for (ElementType elementType : c) {
			boolean added = this.add(elementType);
			if (!changed && added) {
				changed = true;
			}
		}
		return changed;
	}

	public boolean retainAll(Collection<?> c) {
		boolean changed = false;
		for (Object object : c) {
			if (!this.contains(object)) {
				elements.remove(object);
				changed = true;
			}
		}
		return changed;
	}

	public boolean removeAll(Collection<?> c) {
		for (Object elementType : c) {
			this.remove(elementType);
		}
		return true;
	}

	public void clear() {
		elements = new IdentityHashMap<ElementType, Object>();
	}
	
	public IdentitySet<ElementType> clone() {
		IdentitySet<ElementType>  ret = new IdentitySet<ElementType>();
		ret.addAll(this);
		return ret;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		for (ElementType element : this) {
			sb.append(" " + element.toString() + " ");
		}
		sb.append("}");
		
		return sb.toString();
	}

}
