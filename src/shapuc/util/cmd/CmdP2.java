package shapuc.util.cmd;

public interface CmdP2<RETURNTYPE, PARAMTYPE1, PARAMTYPE2> {
	public RETURNTYPE doCommand(PARAMTYPE1 p1, PARAMTYPE2 p2);
}
