package shapuc.util.cmd;

public interface CmdP1<RETURNTYPE, PARAMTYPE> {
	public RETURNTYPE doCommand(PARAMTYPE p);
}
