package shapuc.util.cmd;

public interface Cmd<RETURNTYPE> {
	public RETURNTYPE doCommand();
}
