package shapuc.util.cmd;

public interface CmdVoidP2<PARAM_TYPE1, PARAM_TYPE2> {
	public void doCommand(PARAM_TYPE1 p1, PARAM_TYPE2 p2);
}
