package shapuc.util.cmd;

public interface CmdVoidP1<PARAMTYPE> {
	public void doCommand(PARAMTYPE p);
}
