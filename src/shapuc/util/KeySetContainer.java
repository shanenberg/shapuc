package shapuc.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CharTransition;
import shapuc.scanning.tokenizers.TokenizerState;

public class KeySetContainer<KeyType, ValueType> {
	
	public Vector<Pair<KeyType, HashSet<ValueType>>> elements = new Vector<Pair<KeyType, HashSet<ValueType>>>();
	
	public void add(KeyType aKey, ValueType aValue) {
		if (!this.containsKey(aKey))
			elements.add(new Pair(aKey, new HashSet<ValueType>()));
		this.getKey(aKey).add(aValue);
	}	
	
	public boolean containsKey(KeyType key) {
		for (Pair<KeyType, HashSet<ValueType>> pair : elements) {
			if (pair != null && pair.left != null && pair.left.equals(key))
				return true;
		}
		return false;
	}
	
	public HashSet<ValueType> getKey(KeyType aKey) {
		for (Pair<KeyType, HashSet<ValueType>> pair : elements) {
			if (pair != null && pair.left != null && pair.left.equals(aKey))
				return pair.right;
		}
		throw new RuntimeException("Key not contained");
	}

	public void addAll(KeyType key, HashSet<ValueType> hashSet) {
		if (!this.containsKey(key))
			elements.add(new Pair<KeyType, HashSet<ValueType>>(key, new HashSet<ValueType>()));
		
		this.getKey(key).addAll(hashSet);
	}

	public void remove(KeyType aKey) {
		for(int i=0; i < elements.size(); i++) {
			if (elements.get(i).left.equals(aKey)) {
				elements.remove(i);
				return;
			}
		}
		throw new RuntimeException("Key not contained");
	}
	
	public void printout() {
		System.out.println("KeySetContainer");
		for (Pair<KeyType, HashSet<ValueType>> pair : elements) {
			if (pair!=null && pair.left != null) {
				System.out.println("key: " + pair.left.toString());
				for (ValueType element : pair.right) {
					System.out.println("  " + element);
				}
			} else {
				System.out.println("key: null");
			}
			
		}
	}
	
}
