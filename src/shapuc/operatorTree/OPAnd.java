package shapuc.operatorTree;

import shapuc.automata.NEAFactory;
import shapuc.operatorTree.treeoperations.OPVisitor;
import shapuc.operatorTree.treeoperations.OPVisitorVoid;
import shapuc.regex.RegEx;

public class OPAnd<ROOT_TYPE> extends OPNAry<ROOT_TYPE> implements OPRoot<ROOT_TYPE>{

	public OPAnd(ROOT_TYPE...list) {
		super(list);
	}

	public <PARAMETERYPE> void accept(OPVisitorVoid<PARAMETERYPE, ROOT_TYPE> visitor, PARAMETERYPE p) {
		visitor.visit(this, p);
	}

	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(OPVisitor<RETURNTYPE, PARAMETERYPE, ROOT_TYPE> visitor,
			PARAMETERYPE p) {
		return	visitor.visit(this, p);
	}

}
