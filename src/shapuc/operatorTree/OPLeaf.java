package shapuc.operatorTree;

import shapuc.automata.NEAFactory;
import shapuc.automata.commands.TransitionHandler;
import shapuc.automata.custom.CharTransition;
import shapuc.operatorTree.treeoperations.OPVisitor;
import shapuc.operatorTree.treeoperations.OPVisitorVoid;
import shapuc.regex.RegEx;

public class OPLeaf<TOKENVALUE_TYPE> implements OPRoot<TOKENVALUE_TYPE> {
	
	TOKENVALUE_TYPE value;

	public TOKENVALUE_TYPE getValue() {
		return value;
	}

	public void setValue(TOKENVALUE_TYPE value) {
		this.value = value;
	}

	public OPLeaf(TOKENVALUE_TYPE value) {
		super();
		this.value = value;
	}

	public String regExString() {
		return value.toString();
	}

	public <PARAMETERYPE> void accept(OPVisitorVoid<PARAMETERYPE, TOKENVALUE_TYPE> visitor, PARAMETERYPE p) {
		visitor.visit(this, p);
		
	}

	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(OPVisitor<RETURNTYPE, PARAMETERYPE, TOKENVALUE_TYPE> visitor,
			PARAMETERYPE p) {
		return visitor.visit(this, p);
	}

//	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(RegExVisitor<RETURNTYPE, PARAMETERYPE, VALUE> visitor,
//			PARAMETERYPE p) {
//		return visitor.visit(this, p);
//	}
//
//	public <PARAMETERYPE> void accept(RegExVisitorVoid<PARAMETERYPE, VALUE> visitor, PARAMETERYPE p) {
//		visitor.visit(this, p);
//	}
	
}
