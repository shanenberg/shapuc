package shapuc.operatorTree;

import shapuc.operatorTree.treeoperations.OPVisitor;
import shapuc.operatorTree.treeoperations.OPVisitorVoid;
import shapuc.regex.RegEx;

public class OPN1Fold<ROOT_TYPE> extends OPUnary<ROOT_TYPE> implements OPRoot<ROOT_TYPE>{

//	public <RETURNTYPE, PARAMETERYPE> RETURNTYPE accept(RegExVisitor<RETURNTYPE, PARAMETERYPE, TOKENVALUE_TYPE> visitor, PARAMETERYPE p) {
//		return visitor.visit(this, p);
//	} 
//	
//	public <PARAMETERYPE> void accept(RegExVisitorVoid<PARAMETERYPE, TOKENVALUE_TYPE> visitor, PARAMETERYPE p) {
//		visitor.visit(this, p);
//	} 	
	
	
	public OPN1Fold(ROOT_TYPE regex) {
		super(regex);
	}
//
//	public String regExString() {
//		return child.regExString() + "+";
//	}
	
}
