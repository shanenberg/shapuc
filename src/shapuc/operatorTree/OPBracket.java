package shapuc.operatorTree;

import shapuc.operatorTree.treeoperations.OPVisitor;
import shapuc.operatorTree.treeoperations.OPVisitorVoid;
import shapuc.regex.RegEx;

public class OPBracket<ROOT_TYPE> extends OPUnary<ROOT_TYPE> implements OPRoot<ROOT_TYPE> {

	public OPBracket(ROOT_TYPE regex) {
		super(regex);
	}
	
}
