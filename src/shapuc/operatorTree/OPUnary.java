package shapuc.operatorTree;

import shapuc.regex.RegEx;
import shapuc.util.datastructures.generic.SelfRef;

public class OPUnary<ROOT_TYPE> implements SelfRef<ROOT_TYPE>{

	public OPUnary(ROOT_TYPE regex) {
		super();
		this.child = regex;
	}

	public ROOT_TYPE child;
	
	public ROOT_TYPE getChild() {
		return child;
	}

	public void setChild(ROOT_TYPE p) {
		child = p;
	}

}
