package shapuc.operatorTree;

import java.util.ArrayList;
import java.util.List;

import shapuc.operatorTree.treeoperations.OPVisitor;
import shapuc.regex.RegEx;

public abstract class OPNAry<ROOT_TYPE> 
	implements shapuc.util.datastructures.generic.NAry<ROOT_TYPE, List<ROOT_TYPE>> {

	public final ArrayList<ROOT_TYPE> children = new ArrayList<ROOT_TYPE>();
	
	public List<ROOT_TYPE> getChildren() {
		return children;
	}
	
	public OPNAry(ROOT_TYPE...list) {
		super();
		for (int i = 0; i < list.length; i++) {
			children.add(list[i]);
		}
	}
}
