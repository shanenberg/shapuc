package shapuc.operatorTree.treeoperations;

import shapuc.operatorTree.OPAnd;
import shapuc.operatorTree.OPBracket;
import shapuc.operatorTree.OPN0Fold;
import shapuc.operatorTree.OPOpt;
import shapuc.operatorTree.OPOr;
import shapuc.operatorTree.OPN1Fold;
import shapuc.operatorTree.OPLeaf;
import shapuc.regex.RegEx;
import shapuc.util.cmd.Cmd;
import shapuc.util.cmd.CmdP1;

public interface OPVisitor<RETURNTYPE, PARAMETERYPE, REGEXTYPE> {
	
	public RETURNTYPE visit(OPAnd<REGEXTYPE> node, PARAMETERYPE p);
	public RETURNTYPE visit(OPBracket<REGEXTYPE> node, PARAMETERYPE p);
	public RETURNTYPE visit(OPN0Fold<REGEXTYPE> node, PARAMETERYPE p);
	public RETURNTYPE visit(OPOpt<REGEXTYPE> node, PARAMETERYPE p);
	public RETURNTYPE visit(OPOr<REGEXTYPE> node, PARAMETERYPE p);
	public RETURNTYPE visit(OPN1Fold<REGEXTYPE> node, PARAMETERYPE p);
	public RETURNTYPE visit(OPLeaf<REGEXTYPE> node, PARAMETERYPE p);
}
