package shapuc.operatorTree.treeoperations;

import shapuc.operatorTree.OPAnd;
import shapuc.operatorTree.OPBracket;
import shapuc.operatorTree.OPN0Fold;
import shapuc.operatorTree.OPOpt;
import shapuc.operatorTree.OPOr;
import shapuc.operatorTree.OPN1Fold;
import shapuc.operatorTree.OPLeaf;
import shapuc.regex.RegEx;
import shapuc.util.cmd.Cmd;
import shapuc.util.cmd.CmdP1;

public interface OPVisitorVoid<PARAMETERYPE, REGEXVALUE> {
	
	public void visit(OPAnd<REGEXVALUE> node, PARAMETERYPE p);
	public void visit(OPBracket<REGEXVALUE> node, PARAMETERYPE p);
	public void visit(OPN0Fold<REGEXVALUE> node, PARAMETERYPE p);
	public void visit(OPOpt<REGEXVALUE> node, PARAMETERYPE p);
	public void visit(OPOr<REGEXVALUE> node, PARAMETERYPE p);
	public void visit(OPN1Fold<REGEXVALUE> node, PARAMETERYPE p);
	public void visit(OPLeaf<REGEXVALUE> node, PARAMETERYPE p);
	
}
