package shapuc.operatorTree;

import shapuc.operatorTree.treeoperations.OPVisitor;
import shapuc.operatorTree.treeoperations.OPVisitorVoid;
import shapuc.regex.RegEx;

public class OPOr<ROOT_TYPE> extends OPNAry<ROOT_TYPE> implements OPRoot<ROOT_TYPE>{

	public OPOr(ROOT_TYPE...list) { 
		super(list);
	}

//	@Override
//	public String separator() {	return "|";	}
	
}
