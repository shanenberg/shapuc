/*******************************************************************************
 * /**
 * * The MIT License (MIT)
 * *
 * * Copyright (c) 2015, 2016 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * * 
 * * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * * software and associated documentation files (the "Software"), to deal in the Software 
 * * without restriction, including without limitation the rights to use, copy, modify, 
 * * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * * The above copyright notice and this permission notice shall be included in all copies or 
 * * substantial portions of the Software.
 *
 * * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * **/
package shapuc.graphs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import shapuc.graphs.factories.GraphFactory;

public class Graph<NodeType extends Node<NodeType>, EdgeType extends Edge<NodeType>> {
	
	public Collection<NodeType> getNodes() {
		return nodes.values();
	}

	private GraphFactory<NodeType, EdgeType> factory;

	public Graph(GraphFactory<NodeType, EdgeType> factory) {
		super();
		this.factory = factory;
	}

	HashMap<String, NodeType> nodes = new HashMap<String, NodeType>();
	ArrayList<EdgeType> edges = new ArrayList<EdgeType>();
	
	public NodeType addNode(String identifier) {
		if (nodes.containsKey(identifier))
			throw new RuntimeException("Identifier already contained");
		NodeType n = factory.createNode(identifier);
		nodes.put(identifier, n);
		return n;
	}	
	
	public void addEdge(EdgeType n) {
		edges.add(n);
	}	
	
	public void addEdge(String ni1, String ni2) {
		NodeType n1 = this.getNode(ni1);
		NodeType n2 = this.getNode(ni2);
		edges.add(factory.createEdge(n1, n2));
	}
	
	public NodeType getNode(String nodeIdentifier) {
		if (nodes.containsKey(nodeIdentifier))
			return nodes.get(nodeIdentifier);

		throw new RuntimeException("Node not contained in Graph");
	}
	
	public ArrayList<EdgeType> getEdges() {
		return edges;
	}	
	
	public ArrayList<EdgeType> getEdgesFrom(NodeType n) {
		
		ArrayList<EdgeType> ret = new ArrayList<EdgeType>();
		
		for (EdgeType edge : edges) {
			if (edge.getSource()==n)
				ret.add(edge);
		}
		
		return ret;
	}
	
	public String toString() {
		StringBuffer ret = new StringBuffer();

		for (NodeType n: nodes.values()) {
			for (EdgeType edge : getEdgesFrom(n)) {
				ret.append(edge.toString());
				ret.append("\n");
			}
		}

		return ret.toString();
	}
	
	public boolean hasNode(String n1) {
		try {
			this.getNode(n1);
			return true;
		} catch (Exception e) {
			return false;
		} 		
	}
	
	public boolean hasEdge(String n1, String n2) {
		if (!hasNode(n1)) return false;
		if (!hasNode(n2)) return false;
		
		ArrayList<EdgeType> edgesFrom = this.getEdgesFrom(this.getNode(n1));
		for (EdgeType edge : edgesFrom) {
			if (edge.getTarget()==this.getNode(n2))
				return true;
		}
		return false;
	}
}
