package shapuc.graphs.graphical;

import java.util.ArrayList;

import shapuc.graphs.Edge;
import shapuc.util.datastructures.PointDouble2D;

public class Edge2DMultiPoints extends Edge<Graphical2DNode>{

	ArrayList<Graphical2DNode> pointsBetween = new ArrayList<Graphical2DNode>();
	
	public Edge2DMultiPoints(Graphical2DNode source, Graphical2DNode target) {
		super(source, target);
	} 
	
	public Graphical2DNode getNextNode(Graphical2DNode node) {
		if (this.getSource()==node) {
			if (pointsBetween.size()>0)
				return pointsBetween.get(0);
			else
				return this.getTarget();
		} else {
			for (int i = 0; i < pointsBetween.size(); i++) {
				if (pointsBetween.get(i)==node) {
					if (pointsBetween.size()>i+1) {
						return pointsBetween.get(i+1);
					} else {
						return this.getTarget();
					}
				}
			}
			throw new RuntimeException("Node has no next node");
		}
	}
	
	public Graphical2DNode getPreviousNode(Graphical2DNode node) {
		if (this.getTarget()==node) {
			return this.getNode(1);
		} else {
			for (int i = pointsBetween.size()-1; i >= 0; i--) {
				if (pointsBetween.get(i)==node) {
					return this.getNode(i+1);
				}
			}
			throw new RuntimeException("Node has no next node");
		}
	}	
	
	public int getNumberOfNodes() {
		return 2+this.pointsBetween.size();
	}
	
	public Graphical2DNode getNode(int i) {
		if (i>getNumberOfNodes()) throw new RuntimeException("Graph has less nodes");
		if (i==0) return this.getSource();
		if (i==(getNumberOfNodes()-1)) return this.getTarget();
		
		return pointsBetween.get(i-1);
		
	}

	public PointDouble2D[] getPath() {
		PointDouble2D[] ret = new PointDouble2D[this.getNumberOfNodes()];
		for (int i = 0; i < this.getNumberOfNodes(); i++) {
			ret[i] = getNode(i).getPosition();
		}
		return ret;
	}

	public PointDouble2D getLabelPosition() {
		
		PointDouble2D ret;
		
		if (evenNumberOfNodes()) {
			double newX = getNode(getNumberOfNodes()/2-1).getPosition().x + 
				0.5 * (getNode(getNumberOfNodes()/2).getPosition().x - getNode(getNumberOfNodes()/2-1).getPosition().x);
			
			double newY = getNode(getNumberOfNodes()/2-1).getPosition().y + 
					0.5 * (getNode(getNumberOfNodes()/2).getPosition().y - getNode(getNumberOfNodes()/2-1).getPosition().y);
			ret = new PointDouble2D(newX, newY);
		} else {
			Graphical2DNode middleNode = getNode(getNumberOfNodes()/2);
			ret = new PointDouble2D(middleNode.getX(), middleNode.getY());
		}
		return ret;
	}
	
	public boolean evenNumberOfNodes() {
		return ((getNumberOfNodes() & 1) == 0);
	}

	// TODO: So far just one middlePoint at pos 1 inserted
	public Graphical2DNode insertMiddlePoint() {
		
		Graphical2DNode p = new Graphical2DNode("");
		PointDouble2D labelPos = getLabelPosition();
		p.setX(labelPos.x);
		p.setY(labelPos.y);
		
		pointsBetween.add(p);
		
		return p;
	}

	public Graphical2DNode getMiddlePoint() {
		return getNode(getNumberOfNodes()/2);
	}

}
