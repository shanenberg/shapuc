package shapuc.graphs.graphical;

import shapuc.graphs.Node;
import shapuc.util.datastructures.PointDouble2D;

public class Graphical2DNode extends Node<Graphical2DNode> {
	PointDouble2D position;

	public Graphical2DNode(String identifier, PointDouble2D position) {
		super(identifier);
		this.position = position;
	}
	
	public Graphical2DNode(String identifier, double x, double y) {
		super(identifier);
		this.position = new PointDouble2D(x, y);
	}
	
	public Graphical2DNode(String identifier) {
		super(identifier);
		this.position = new PointDouble2D(0, 0);
	}	
	
	public void setX(double x) {
		this.position.x = x;
	}
	
	public void setY(double y) {
		this.position.y = y;
	}
	
	public double getX() {
		return this.position.x;
	}
	
	public double getY() {
		return this.position.y;
	}	
	
	public PointDouble2D getPosition() {
		return position;
	}
	
}
