package shapuc.graphs.graphical;

import shapuc.graphs.Graph;
import shapuc.graphs.factories.GraphFactory;

public class GraphicalGraph extends Graph<Graphical2DNode, Edge2DMultiPoints>{

	public GraphicalGraph(GraphFactory<Graphical2DNode, Edge2DMultiPoints> factory) {
		super(factory);
	}

}
