/*******************************************************************************
 * /**
 * * The MIT License (MIT)
 * *
 * * Copyright (c) 2015, 2016 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * * 
 * * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * * software and associated documentation files (the "Software"), to deal in the Software 
 * * without restriction, including without limitation the rights to use, copy, modify, 
 * * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * * The above copyright notice and this permission notice shall be included in all copies or 
 * * substantial portions of the Software.
 *
 * * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * **/
package shapuc.graphs.factories;

import shapuc.graphs.Edge;
import shapuc.graphs.Graph;
import shapuc.graphs.Node;

public abstract class GraphFactory<NodeType extends Node<NodeType>, EdgeType extends Edge<NodeType>> {
	public abstract Graph<NodeType, EdgeType> createGraph();
	
	public abstract EdgeType createEdge(NodeType n1, NodeType n2);

	public EdgeType createEdge(NodeType n1, NodeType n2, String name) {
		EdgeType e = createEdge(n1, n2);
		e.setName(name);

		return e;
	}
	
	public EdgeType createEdge(Graph<NodeType, EdgeType> g, String ni1, String ni2) {
		NodeType n1 = g.getNode(ni1);
		NodeType n2 = g.getNode(ni2);
		return this.createEdge(n1, n2, ni2);
	}
	
	public abstract NodeType createNode(String identifier);
}
