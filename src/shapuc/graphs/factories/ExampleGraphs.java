/*******************************************************************************
 * /**
 * * The MIT License (MIT)
 * *
 * * Copyright (c) 2015, 2016 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * * 
 * * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * * software and associated documentation files (the "Software"), to deal in the Software 
 * * without restriction, including without limitation the rights to use, copy, modify, 
 * * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * * The above copyright notice and this permission notice shall be included in all copies or 
 * * substantial portions of the Software.
 *
 * * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * **/
package shapuc.graphs.factories;

import shapuc.graphs.Edge;
import shapuc.graphs.Graph;
import shapuc.graphs.Node;
import shapuc.graphs.graphical.GraphicalNode;

public class ExampleGraphs {

	public <NodeType extends Node<NodeType>, EdgeType extends Edge<NodeType>> Graph<NodeType, EdgeType> createExampleGraph01(
			GraphFactory<NodeType, EdgeType> graphFactory) {

		Graph<NodeType, EdgeType> graph = graphFactory.createGraph();

		graph.addNode("A");
		graph.addNode("B");
		graph.addNode("E");
		graph.addNode("D");
		graph.addNode("F");

		graph.addEdge("A", "B");
		graph.addEdge("B", "E");
		graph.addEdge("B", "A");
		graph.addEdge("E", "D");
		graph.addEdge("D", "E");
		graph.addEdge("D", "A");
		graph.addEdge("A", "F");

		return graph;
	}

	public Graph<GraphicalNode, Edge<GraphicalNode>> createGraphicalGraph() {
		GraphFactoryGraphical graphFactory = new GraphFactoryGraphical();
		Graph<GraphicalNode, Edge<GraphicalNode>> graph = createExampleGraph01(graphFactory);
		
		int positionSetter = 20;
		for (GraphicalNode n : graph.getNodes()) {
			n.setX(positionSetter);
			n.setY(positionSetter);
			positionSetter = positionSetter + 20;
		}
		
		graph.getNode("A").setX(180);
		
		return graph;
	
	}		
}
