package shapuc.automata;

import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.util.Pair;
import shapuc.util.cmd.CmdP2;
import shapuc.util.cmd.CmdVoidP1;
import shapuc.util.returnObject.ReturnObject;

public class NEAFactory<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> 
		extends AutomataFactory<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE>  {

	public NEAFactory(TransitionCmd transitionCmd, StateActivationCmd stateCmd) {
		super(transitionCmd, stateCmd);
	}

	@Override
	public NEA<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> createUninitializedAutomata() {
		return new NEA<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE>(transitionCmd, stateCmd);
	}

	public void initializeAutomata(NEA<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> automata, STATE_TYPE start,
			TRANSITION_TYPE spont, Object[][] transitions, STATE_TYPE[] ends) {

		automata.setSpontaneousTransition(spont);
		automata.setStartState(start);

		for (int i = 0; i < ends.length; i++) {
			automata.addEndState(ends[i]);
		}

		for (int i = 0; i < transitions.length; i++) {
			if (transitions[i].length == 2) {
				automata.addTransition((STATE_TYPE) transitions[i][0],
						(TRANSITION_TYPE) automata.getSpontaneousTransition(), (STATE_TYPE) transitions[i][1]);
			} else {
				automata.addTransition((STATE_TYPE) transitions[i][0], (TRANSITION_TYPE) transitions[i][1],
						(STATE_TYPE) transitions[i][2]);
			}
		}
	}
	
	
}