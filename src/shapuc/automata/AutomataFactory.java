package shapuc.automata;

import shapuc.automata.commands.StateDoNothingCmd;
import shapuc.automata.commands.TransitionEqualityCheckCmd;
import shapuc.automata.commands.TransitionIdentityCheckCmd;
import shapuc.automata.commands.StateInvokeActiveCmd;
import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.util.cmd.CmdP2;
import shapuc.util.cmd.CmdVoidP1;

public abstract class AutomataFactory<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> {


	public final TransitionCmd transitionCmd;
	public final StateActivationCmd stateCmd;
	
	public AutomataFactory(TransitionCmd transitionCmd, StateActivationCmd stateCmd) {
		super();
		this.transitionCmd = transitionCmd;
		this.stateCmd = stateCmd;
	}
	
	public abstract Automata<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> createUninitializedAutomata();	
	
}
