/*******************************************************************************
 * /**
 * * The MIT License (MIT)
 * *
 * * Copyright (c) 2015, 2016 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * * 
 * * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * * software and associated documentation files (the "Software"), to deal in the Software 
 * * without restriction, including without limitation the rights to use, copy, modify, 
 * * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * * The above copyright notice and this permission notice shall be included in all copies or 
 * * substantial portions of the Software.
 *
 * * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * **/
package shapuc.automata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.util.IdentitySet;
import shapuc.util.Pair;
import shapuc.util.Triple;
import shapuc.util.cmd.CmdP2;
import shapuc.util.cmd.CmdVoidP1;

/*
 * Input is handled via equals, states are handled via Identity 
 */

public class  NEA<
				STATE_TYPE, 
				TRANSITION_TYPE, 
				INPUT_TYPE> 
			 extends Automata<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE>{



	public NEA(TransitionCmd transitionHandlesAndAcceptsInputCmd,
			StateActivationCmd stateReachedCmd) {
		super(transitionHandlesAndAcceptsInputCmd, stateReachedCmd);
	}

	TRANSITION_TYPE spontaneousTransition;

	public TRANSITION_TYPE getSpontaneousTransition() {
		return spontaneousTransition;
	}

	public void setSpontaneousTransition(TRANSITION_TYPE spontaneousTransition) {
		this.spontaneousTransition = spontaneousTransition;
	}

	HashSet<STATE_TYPE> currentStates = new HashSet<STATE_TYPE>();
	
	
	public HashSet<STATE_TYPE> getCurrentStates() {
		return currentStates;
	}	
	
	public void setCurrentStates(Set<STATE_TYPE> newCurrentStates) {
		currentStates.clear();
		currentStates.addAll(newCurrentStates);
	}		
	
	public void setCurrentStatesAndExploreSponaneousTransitions(Set<STATE_TYPE> newCurrentStates) {
		currentStates.clear();
		currentStates.addAll(newCurrentStates);
		doSpontaneousTransitionsWhileNecessary();
	}		
	
	public void start() {
		if (startState==null) throw new RuntimeException("StartState is null -> set start state first");
		if (spontaneousTransition==null) throw new RuntimeException("No SpontaneousTransition set");
		currentStates = new HashSet<STATE_TYPE>();
		currentStates.add(startState);
		doSpontaneousTransitionsWhileNecessary();
	}		
	
	
	/**
	 * Slightly naive implementation: for spontaneous transitions all states are repeated 
	 * @param input
	 * @return
	 */
	public void gotoNext(INPUT_TYPE input) {
		if (invalidInput(input)) throw new InvalidInputException("Invalid NEA input: " + input);
		
		HashSet<STATE_TYPE> _nextCurrentStates = new HashSet<STATE_TYPE>();
		
		for (Pair<TRANSITION_TYPE, STATE_TYPE> aTransition : allTransitionsFromCurrent()) {
			if ((input!=spontaneousTransition) && 
					(aTransition.getLeft()!=spontaneousTransition) &&
					transitionHandlesAndAcceptsInput(aTransition.getLeft(), input)) {
				_nextCurrentStates.add(aTransition.getRight());
				this.reachedState(aTransition.getRight());
			}
		}
		currentStates = _nextCurrentStates;
		
		doSpontaneousTransitionsWhileNecessary();
	}
	
	private void printCurrentStates() {
		StringBuffer buf = new StringBuffer(super.toString());
		buf.append("\nCurrentStates:\n");
		for (STATE_TYPE aState : currentStates) {
			buf.append(aState + "--");
		}
		System.out.println(buf.toString());
	}

	private void printStates(IdentitySet<STATE_TYPE> _currentStates) {
		for (STATE_TYPE stateType : _currentStates) {
			System.out.println(stateType);
		}
		
	}

	private boolean invalidInput(INPUT_TYPE input) {
		for (STATE_TYPE stateType : currentStates) {
			if (nextDirectStates(stateType, input).size()>0)
				return false;
		}

		return true;
	}

	public boolean hasSponaneousTransition(STATE_TYPE aState) {
		for (Pair<TRANSITION_TYPE, STATE_TYPE> transition : stateTransitionTable.get(aState)) {
			if (transition.getLeft() == spontaneousTransition) return true;
		}
		return false;
	}
	
	public boolean hasStateWithSpontaneousTransition() {
		for (STATE_TYPE aState : currentStates) {
			if (hasSponaneousTransition(aState)) return true;
		}
		return false;
	}
	
	public IdentitySet<STATE_TYPE> statesWithSpontaneousTransition() {
		IdentitySet<STATE_TYPE> ret = new IdentitySet<STATE_TYPE>();
		
		for (STATE_TYPE aState : currentStates) {
			if (hasSponaneousTransition(aState)) ret.add(startState);
		}
		
		return ret;
	}	
	
	/*
	 * This method lets the NEA run all spontaneous transitions until something is not
	 * 
	 */
	protected void doSpontaneousTransitionsWhileNecessary() {
//		System.out.println("+++++++++++DO SPONTANEOUS TRANSITIONS");		

		HashSet<STATE_TYPE> _currentStates = (HashSet<STATE_TYPE>) currentStates.clone();

		HashSet<STATE_TYPE> newStates = (HashSet<STATE_TYPE>) _currentStates.clone();
		
		while (!newStates.isEmpty()) {
//
//
//			System.out.println("    ###########");
//			System.out.println("    Current States: ");
//			for (STATE_TYPE s : _currentStates) {
//				System.out.println("    " + s);
//			}
//			System.out.println("    ###########");
			
			Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> transitions = allTransitionsFromStateSet(newStates);
//System.out.println("newStates: " + newStates.size());						
//System.out.println("trans: " + transitions.size());						
			if (transitions.isEmpty()) break;
			
			newStates = new HashSet<STATE_TYPE>();

			for (Pair<TRANSITION_TYPE, STATE_TYPE> aTransition : transitions) {

				
				if (aTransition.getLeft()==spontaneousTransition) {
//System.out.println("added: " + aTransition.getRight());						
//System.out.println("before newStates size: " + newStates.size());						
					boolean newStateFound =_currentStates.add(aTransition.getRight());
					if (newStateFound) {
						newStates.add(aTransition.getRight());
						this.reachedState(aTransition.getRight());
//System.out.println("newState: " + aTransition.getRight());						
//System.out.println("newStates size: " + newStates.size());						
					}
				}
			} 
		}
		
		currentStates = _currentStates;
	}

	private IdentitySet<STATE_TYPE> nextDirectStates(STATE_TYPE aState, INPUT_TYPE input) {
		IdentitySet<STATE_TYPE> result = new IdentitySet<STATE_TYPE>();
		for (Pair<TRANSITION_TYPE, STATE_TYPE> transition : stateTransitionTable.get(aState)) {
			if (transitionHandlesAndAcceptsInput(transition.getLeft(), input))
				result.add(transition.getRight());
		}
		
		return result;
	}
	
	private IdentitySet<STATE_TYPE> nextDirectSpontaneousStates(STATE_TYPE aState) {
		IdentitySet<STATE_TYPE> result = new IdentitySet<STATE_TYPE>();
		
		for (Pair<TRANSITION_TYPE, STATE_TYPE> transition : stateTransitionTable.get(aState)) {
			if (transition.getLeft()==spontaneousTransition) {
				result.add(transition.getRight());
			}
		}
		
		return result;
	}	
	
	

	@Override
	public Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> allTransitionsFromCurrent() {
		return allTransitionsFromStateSet(currentStates);
	}
	
	public Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> allTransitionsFromStateSet(Set<STATE_TYPE> states) {

		Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> result = 
				new Vector<Pair<TRANSITION_TYPE, STATE_TYPE>>();
		
		for (STATE_TYPE aCurrentState : states) {
			result.addAll(this.allTransitionsFrom(aCurrentState));
		}
		
		return result;
	}	
	
	public Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> allNonSpontaneousTransitionsFromStateSet(Set<STATE_TYPE> states) {

		Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> result = 
				new Vector<Pair<TRANSITION_TYPE, STATE_TYPE>>();
		
		for (STATE_TYPE aCurrentState : states) {
			
			result.addAll(this.allNonSpontaneousTransitionsFrom(aCurrentState));
		}
		
		return result;
	}	
	
	public Vector<Triple<STATE_TYPE, TRANSITION_TYPE, STATE_TYPE>> allNonSpontaneousTransitionsWithOrigin() {
		return allNonSpontaneousTransitionsFromStateSetWithOrigin(this.getCurrentStates());
	}	
	
	public Vector<Triple<STATE_TYPE, TRANSITION_TYPE, STATE_TYPE>> allNonSpontaneousTransitionsFromStateSetWithOrigin(Set<STATE_TYPE> states) {

		Vector<Triple<STATE_TYPE, TRANSITION_TYPE, STATE_TYPE>> result = 
				new Vector<Triple<STATE_TYPE, TRANSITION_TYPE, STATE_TYPE>>();
		
		for (STATE_TYPE aCurrentState : states) {
			for(Pair<TRANSITION_TYPE, STATE_TYPE> t: this.allNonSpontaneousTransitionsFrom(aCurrentState)) {
				result.add(new Triple(
					aCurrentState, t.getLeft(), t.getRight()
				));
			}
		}
		
		return result;
	}	
	
	public Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> allNonSpontaneousTransitions() {
		return allNonSpontaneousTransitionsFromStateSet(this.currentStates);
	}	

	@Override
	public boolean hasFinished() {
		for (STATE_TYPE aCurrentState : currentStates) {
			if (endStates.contains(aCurrentState)) return true;
		}
		return false;
	}
	
	public void addSpontaneousTransition(STATE_TYPE startState, STATE_TYPE endState) {
		addState(startState);
		addState(endState);
		stateTransitionTable.get(startState).add(
				new Pair<TRANSITION_TYPE, STATE_TYPE>(this.getSpontaneousTransition(), endState));
	}	


	public String toString() { 
		
		StringBuffer buf = new StringBuffer(super.toString());
		buf.append("\nCurrentStates:\n");
		for (STATE_TYPE aState : currentStates) {
			buf.append(aState + "\n");
		}
		buf.append("\nEndStates:\n");
		for (STATE_TYPE aState : endStates) {
			buf.append(aState + "\n");
		}
		
		buf.append("has spontaneousStates: " + hasStateWithSpontaneousTransition());
		return buf.toString();
	}

	@Override
	public void gotoNextWithExceptionSet(Set<INPUT_TYPE> exceptionSet) {
		
		HashSet<STATE_TYPE> _nextCurrentStates = new HashSet<STATE_TYPE>();
//System.out.println(currentStates);		
		for (Pair<TRANSITION_TYPE, STATE_TYPE> aTransition : allTransitionsFromCurrent()) {
//System.out.println("go to next: " + aTransition.getLeft());		

			boolean transitionOfInterest = false;
			if (aTransition.getLeft() != spontaneousTransition
					&& transitionHandlesAndAcceptsOneInputContainedInExceptionSet(aTransition.getLeft(),
							exceptionSet)) {
				transitionOfInterest = true;
//				break;
			}
				
			if (transitionOfInterest) {
				_nextCurrentStates.add(aTransition.getRight());
//System.out.println("go to next: added" + aTransition.getRight());		
				this.reachedState(aTransition.getRight());				
			}
			
		}
		currentStates = _nextCurrentStates;
		
		doSpontaneousTransitionsWhileNecessary();
	}

	
	public Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> allNonSpontaneousTransitionsFrom(STATE_TYPE aState) {
		if (!stateTransitionTable.containsKey(aState))
			throw new RuntimeException("Invalid Statement for DEA: current state not contained in stateTransitionTable");
		Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> ret = new Vector<Pair<TRANSITION_TYPE, STATE_TYPE>>();
		for (Pair<TRANSITION_TYPE, STATE_TYPE> pair : stateTransitionTable.get(aState)) {
			if (spontaneousTransition != pair.getLeft())
				ret.addElement(pair);
		}
		return ret;
	}
	
	public Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> allNonSpontaneousTransitionsFromCurrent() {

		Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> ret = new Vector<Pair<TRANSITION_TYPE, STATE_TYPE>>();
		for (Pair<TRANSITION_TYPE, STATE_TYPE> pair : stateTransitionTable.get(currentStates)) {
			if (spontaneousTransition != pair.getLeft())
				ret.addElement(pair);
		}
		return ret;
	}	
		
}
