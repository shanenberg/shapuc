package shapuc.automata.factories;

import java.util.List;

import shapuc.automata.AutomataFactoryParameters;
import shapuc.automata.NEA;
import shapuc.automata.NEAFactory;
import shapuc.automata.custom.AnyWithoutTransition;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CharTransition;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.regex.RegEx;
import shapuc.regex.operations.RegExToNEACreationVisitor;
import shapuc.scanning.TokenizerNEAFactory;
import shapuc.scanning.tokenizer.TokenizerNEA;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.scanning.tokenizers.TokenizerStateInactive;

public class NEAFromRegExFactory {
	
	public static TokenizerNEA createInactiveNEAFor(RegEx regex) {

		/** First create Visitor for NEACreation */
		RegExToNEACreationVisitor creationVisitor = new RegExToNEACreationVisitor() {
			protected CustomTransitionHandler<Character> createSpontaneousTransition() {
				CustomTransitionHandler<Character> th = new CharTransition('?');
				return th;
			}

			protected TokenizerState createStartState() {
				return new TokenizerStateInactive(-1, null);
			}

			protected TokenizerState createEndState() {
				return new TokenizerStateInactive(-1, null);
			}

			protected CustomTransitionHandler<Character> createTransition(Character value) {
				CustomTransitionHandler<Character> th = new CharTransition(value);
				return th;
			}

			@Override
			protected CustomTransitionHandler<Character> createAnyWithoutTransition(List<Character> value) {
				CustomTransitionHandler<Character> th = new AnyWithoutTransition<Character>(value);
				return null;
			}

			@Override
			protected CustomTransitionHandler<Character> createLiteralGroupTransition(Character start, Character end) {
				CustomTransitionHandler<Character> th = new CharRangeTransition(start, end);
				return th;
			}
		};

		AutomataFactoryParameters p = new AutomataFactoryParameters();
		TokenizerNEA automata = 
				new TokenizerNEAFactory(p.CUSTOM_CHECK, p.INACTIVE_STATE)
				.createUninitializedAutomata();
		creationVisitor.initializeNEA(automata);
				;

		// creationVisitor.nea.setStartState();
		TokenizerState endState = (TokenizerState) creationVisitor.nea.getEndStates().toArray()[0];
		regex.accept(creationVisitor, creationVisitor.params(creationVisitor.nea.getStartState(), endState));
		return creationVisitor.nea;
	}
}
