package shapuc.automata;

import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.util.cmd.CmdP2;
import shapuc.util.cmd.CmdVoidP1;

public class DEAFactory<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> extends AutomataFactory<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> {
		
	public DEAFactory(TransitionCmd transitionCmd, StateActivationCmd stateCmd) {
		super(transitionCmd, stateCmd);
	}

	public DEA<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> 
		   createUninitializedAutomata() {

		return new DEA<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE>(
				transitionCmd,
				stateCmd
		);
	}
		   
	public void initializeAutomata(
			DEA<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> automata, 
			STATE_TYPE start,
			Object[][] transitions, 
			STATE_TYPE[] ends) 
	{

		automata.setStartState(start);

		for (int i = 0; i < ends.length; i++) {
			automata.addEndState(ends[i]);
		}

		for (int i = 0; i < transitions.length; i++) {
			automata.addTransition((STATE_TYPE) transitions[i][0], (TRANSITION_TYPE) transitions[i][1],
						(STATE_TYPE) transitions[i][2]);
		}
	}  
}