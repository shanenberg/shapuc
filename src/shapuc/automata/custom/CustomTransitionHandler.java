package shapuc.automata.custom;

import shapuc.automata.commands.TransitionHandler;

public interface CustomTransitionHandler<TYPE> extends TransitionHandler<TYPE>{
	
	char getAcceptanceChar();
	
	boolean isSingleCharTransitions();
	
 	
}
