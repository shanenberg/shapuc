package shapuc.automata.custom;

import java.awt.datatransfer.Transferable;
import java.util.HashSet;

import shapuc.automata.commands.TransitionHandler;

public class CharTransition implements CustomTransitionHandler<Character> {


	final Character content;
	
	public CharTransition(Character myChar) {
		super();
		this.content = myChar;
	}

	public boolean handles(Character s) {
		return content.equals(s);
	}

	@Override
	public String toString() {
		return "CharTransition [content=" + content + "]";
	}
	
	public char getAcceptanceChar() {
		return content;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CharTransition other = (CharTransition) obj;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		return true;
	}

	public final boolean isSingleCharTransitions() {
		return true;
	}
	
}
