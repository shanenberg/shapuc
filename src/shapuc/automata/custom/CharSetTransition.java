package shapuc.automata.custom;

import java.awt.datatransfer.Transferable;
import java.util.HashSet;

import shapuc.automata.commands.TransitionHandler;

public class CharSetTransition implements CustomTransitionHandler<Character> {
	final HashSet<Character> content = new HashSet<Character>();
	
	public CharSetTransition(HashSet<Character> content) {
		super();
		this.content.addAll(content);
	}

	public boolean handles(Character s) {
		return content.contains(s);
	}

	public String toString() {
		return "CharTransition: " + content;
	}
	
	public char getAcceptanceChar() {
		throw new RuntimeException("dont ask me for this...");
	}

	public final boolean isSingleCharTransitions() {
		return false;
	}
}
