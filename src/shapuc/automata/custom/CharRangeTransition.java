package shapuc.automata.custom;

import java.util.HashSet;

import shapuc.util.Pair;
import shapuc.util.datastructures.Interval.ContainmentType;
import shapuc.util.datastructures.IntervalChar;

public class CharRangeTransition implements CustomTransitionHandler<Character> {

	public IntervalChar interval;

	@Override
	public String toString() {
		return "CharRangeTransition [startChar=" + interval.start + ", endChar=" + interval.end + "]";
	}

	public CharRangeTransition(Character startChar, Character endChar) {
		super();
		interval = new IntervalChar(startChar, endChar);
	}
	
	public CharRangeTransition(IntervalChar interval) {
		super();
		this.interval = interval;
	}	

	public boolean handles(Character s) {
		return interval.contains(s);
	}
	
	public ContainmentType overlapping(CharRangeTransition other) {
		return interval.overlapping(other.interval);
	}	
	
	public char getAcceptanceChar() {
		throw new RuntimeException("Don't ask a CharRangeTransition for acceptance char");
	}

	public final boolean isSingleCharTransitions() {
		return false;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((interval == null) ? 0 : interval.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CharRangeTransition other = (CharRangeTransition) obj;
		if (interval == null) {
			if (other.interval != null)
				return false;
		} else if (!interval.equals(other.interval))
			return false;
		return true;
	}

	public HashSet<CharRangeTransition> rangesWithRemovedChar(char removedChar) {
		if (!handles(removedChar)) {
			throw new RuntimeException("when you want to remove the range, it should be contained!");
		}
		
		IntervalChar[] intervals = interval.diffWithRemovedChar(removedChar);
		HashSet<CharRangeTransition> ret = new HashSet<CharRangeTransition>();
		
		for (int i = 0; i < intervals.length; i++) {
			ret.add(new CharRangeTransition(intervals[i]));
		}
		
		return ret;
	}
	
	public  Pair<ContainmentType, IntervalChar[]> rangesWithRemovedRange(
			CharRangeTransition aRange) {
		
		return interval.diffIntervals(aRange.interval);
		
	}


}
