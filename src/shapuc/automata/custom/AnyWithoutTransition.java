package shapuc.automata.custom;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class AnyWithoutTransition<TYPE> implements CustomTransitionHandler<TYPE>{
	
	@Override
	public String toString() {
		return "AnyWithoutTransition [without=" + without + "]";
	}

	public final HashSet<TYPE> without = new HashSet<TYPE>();
	
	public AnyWithoutTransition(TYPE...withoutList) {
		for (int i=0; i<withoutList.length; i++) {
			without.add(withoutList[i]);
		}
	}	
	
	public AnyWithoutTransition(Collection<TYPE> withoutList) {
		for (TYPE customTransitionHandler : withoutList) {
//			System.out.println("without: " + customTransitionHandler);
			without.add(customTransitionHandler);
		}
	}		

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((without == null) ? 0 : without.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnyWithoutTransition other = (AnyWithoutTransition) obj;
		if (without == null) {
			if (other.without != null)
				return false;
		} else if (!without.equals(other.without))
			return false;
		return true;
	}

	public boolean handles(TYPE s) {
		for (TYPE element : without) {
			if (element.equals(s))
				return false;
		}
		return true;
	}
	
	public boolean accepts(TYPE s) {
		return !without.contains(s);
	}	
	
	public boolean addException(TYPE anException) {
		return without.add(anException);
	}

	public char getAcceptanceChar() {
		throw new RuntimeException("Not xet implementated");
	}

	public final boolean isSingleCharTransitions() {
		return false;
	}

}
