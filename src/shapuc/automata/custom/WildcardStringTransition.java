package shapuc.automata.custom;

import java.awt.datatransfer.Transferable;

import shapuc.automata.commands.TransitionHandler;

public class WildcardStringTransition<TYPE> implements CustomTransitionHandler<TYPE> {

	final String content;
	
	public WildcardStringTransition(String myString) {
		super();
		this.content = myString;
	}

	public boolean handles(TYPE s) {
		if (content.equals("*"))
			return true;
		return content.equals(s);
	}

	public char getAcceptanceChar() {
		throw new RuntimeException("Not xet implementated");
	}

	public boolean isSingleCharTransitions() {
		throw new RuntimeException("blbalbalb");
	}

}
