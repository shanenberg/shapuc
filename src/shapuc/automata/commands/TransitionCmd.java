package shapuc.automata.commands;

import java.util.Set;

import shapuc.util.cmd.CmdP2;

public interface TransitionCmd<TRANSITIONTYPE, INPUT_TYPE> extends CmdP2<Boolean, TRANSITIONTYPE, INPUT_TYPE> {

	/** Returns true, if Transition handles the delivered input */
	Boolean doCommand(TRANSITIONTYPE p1, INPUT_TYPE p2);

	/** Returns true, if Transition handles at least one input from the exceptionSet */
	Boolean doCommand(TRANSITIONTYPE p1, Set<INPUT_TYPE> exceptionSet);
	
	
}
