package shapuc.automata.commands;

import shapuc.util.cmd.CmdVoidP1;

public class StateDoNothingCmd<STATE_TYPE> implements StateActivationCmd<STATE_TYPE> {
	public void doCommand(STATE_TYPE p) {}
}