package shapuc.automata.commands;

import shapuc.util.cmd.CmdVoidP1;

public interface StateActivationCmd<AN_ACTIVE_STATE> extends CmdVoidP1<AN_ACTIVE_STATE> {

	void doCommand(AN_ACTIVE_STATE p);

}
