package shapuc.automata.commands;

import shapuc.automata.custom.WildcardStringTransition;

/**
 * If you want a transition to do some command when it is checked, you define your
 * own TransitionHandler.
 * 
 * Example:
 * DEAFactory<Object, TransitionHandler<String>, String> f = 
 *   new DEAFactories<Object, StringWildcardTransition, String>().createAutomataOnCustomCheckFactory();
 * 
 * It defines a new DEAFactory where a StringWildCardTransition is being used for checking
 * whether a transition can handle a given input.  
 *
 * @param <INPUT_TYPE>
 */
public interface TransitionHandler<INPUT_TYPE> {
	public boolean handles(INPUT_TYPE s);
}
