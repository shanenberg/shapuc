package shapuc.automata.commands;

import java.util.Set;

import shapuc.automata.custom.AnyWithoutTransition;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CharTransition;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.util.cmd.CmdP2;

public class TransitionForwardCmd<TRANSITIONTYPE extends CustomTransitionHandler<INPUT_TYPE>, INPUT_TYPE> implements TransitionCmd<TRANSITIONTYPE, INPUT_TYPE> {
	
	public Boolean doCommand(TRANSITIONTYPE transition, INPUT_TYPE input) {
		return transition.handles(input);
	}

	public Boolean doCommand(TRANSITIONTYPE transition, Set<INPUT_TYPE> exceptionSet) {
		if (transition instanceof CharTransition) {
			return exceptionSet.contains(((CharTransition) transition).getAcceptanceChar());
		} else if (transition instanceof AnyWithoutTransition) {
			return true;
		} else if (transition instanceof CharRangeTransition) {
			return true;
		}
		
		throw new RuntimeException("Something is wrong....this should not happen");
	}

}