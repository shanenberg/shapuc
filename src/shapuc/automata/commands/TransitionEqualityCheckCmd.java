package shapuc.automata.commands;

import java.util.Set;

import shapuc.util.cmd.CmdP2;

public class TransitionEqualityCheckCmd<TRANSITIONTYPE, INPUT_TYPE> implements TransitionCmd<TRANSITIONTYPE, INPUT_TYPE> {
	public Boolean doCommand(TRANSITIONTYPE transition, INPUT_TYPE input) {
		return transition.equals(input);
	}

	public Boolean doCommand(TRANSITIONTYPE transition, Set<INPUT_TYPE> exceptionSet) {
		return exceptionSet.contains(transition);
	}
}