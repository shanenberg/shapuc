package shapuc.automata.commands;

import shapuc.automata.ActiveState;
import shapuc.util.cmd.CmdVoidP1;

public class StateInvokeActiveCmd<AN_ACTIVE_STATE extends ActiveState> implements StateActivationCmd<AN_ACTIVE_STATE> {
	public final void doCommand(AN_ACTIVE_STATE activeState) {
		activeState.reachedState();
	}

}