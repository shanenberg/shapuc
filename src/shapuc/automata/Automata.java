/*******************************************************************************
 * /**
 * * The MIT License (MIT)
 * *
 * * Copyright (c) 2015, 2016 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * * 
 * * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * * software and associated documentation files (the "Software"), to deal in the Software 
 * * without restriction, including without limitation the rights to use, copy, modify, 
 * * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * * The above copyright notice and this permission notice shall be included in all copies or 
 * * substantial portions of the Software.
 *
 * * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * **/
package shapuc.automata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import shapuc.util.Pair;
import shapuc.util.Triple;
import shapuc.util.cmd.CmdP2;
import shapuc.util.cmd.CmdVoidP1;
import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.graphs.Edge;
import shapuc.graphs.Graph;
import shapuc.graphs.Node;
import shapuc.graphs.factories.GraphFactory;
import shapuc.graphs.factories.GraphFactoryGraphical;
import shapuc.graphs.factories.GraphFactoryGraphical2D;
import shapuc.graphs.graphical.Edge2DMultiPoints;
import shapuc.graphs.graphical.Graphical2DNode;
import shapuc.graphs.graphical.GraphicalGraph;
import shapuc.graphs.graphical.GraphicalNode;
import shapuc.util.IdentitySet;

public abstract class Automata<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> {

	STATE_TYPE startState;

	protected HashMap<STATE_TYPE, Vector<Pair<TRANSITION_TYPE, STATE_TYPE>>> 
		stateTransitionTable = 
			new HashMap<STATE_TYPE, Vector<Pair<TRANSITION_TYPE, STATE_TYPE>>>();
	

	/* The command to check whether a transation accepts a given input*/
	public TransitionCmd transitionHandlesAndAcceptsInputCmd;	
	
	/* The command to be executed when a state is reached */
	public CmdVoidP1<STATE_TYPE> stateReachedCmd;			
	
	public abstract Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> allTransitionsFromCurrent();
	public abstract boolean hasFinished();
	public abstract void gotoNext(INPUT_TYPE input);
	public abstract void start();
	/* This method excepts and exceptionSet as input and goes to all Transitions that do not 
	 * accept an element contained in the exception set */
	public abstract void gotoNextWithExceptionSet(Set<INPUT_TYPE> exceptionSet);
	
	public Automata(
			TransitionCmd transitionHandlesAndAcceptsInputCmd,
			StateActivationCmd stateReachedCmd) {
		super();
		this.transitionHandlesAndAcceptsInputCmd = transitionHandlesAndAcceptsInputCmd;
		this.stateReachedCmd = stateReachedCmd;
	}
	
	/*
	 * Checks whether the transition matches the input.
	 * PrimitiveOperation for TemplateMethod gotoNext
	*/	
	protected boolean transitionHandlesAndAcceptsInput(TRANSITION_TYPE transition, INPUT_TYPE input) {
//		System.out.println("transitionHandlesAndAcceptsInput " + input);
		return transitionHandlesAndAcceptsInputCmd.doCommand(transition, input);
	}
	

	protected boolean transitionHandlesAndAcceptsOneInputContainedInExceptionSet(TRANSITION_TYPE transition,
			Set<INPUT_TYPE> exceptionSet) {
		return transitionHandlesAndAcceptsInputCmd.doCommand(transition, exceptionSet);
	}	

	protected void reachedState(STATE_TYPE aState) {
		stateReachedCmd.doCommand(aState);
	}	

	protected IdentitySet<STATE_TYPE> endStates = new IdentitySet<STATE_TYPE>();


	public Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> allTransitionsFrom(STATE_TYPE aState) {
		if (!stateTransitionTable.containsKey(aState))
			throw new RuntimeException("Invalid Statement for DEA: current state not contained in stateTransitionTable");
		return stateTransitionTable.get(aState);
	}

	
	public boolean addState(STATE_TYPE aState) {
		if (aState==null) throw new RuntimeException("IdentitySets do not like null values");
		if (!stateTransitionTable.keySet().contains(aState)) {
			stateTransitionTable.put(aState, new Vector<Pair<TRANSITION_TYPE, STATE_TYPE>>());
			return true;
		}
		return false;
	}
	
	public void addTransition(STATE_TYPE startState, TRANSITION_TYPE transition, STATE_TYPE endState) {
		if (startState == null) throw new RuntimeException("Null is not a valid state for automatas");
		addState(startState);
//System.out.println("startState: " + startState);		
		addState(endState);
		
//		System.out.println("addedStae");
//		System.out.println(stateTransitionTable.get(startState));
//		System.out.println("keys + " + stateTransitionTable.keySet().size());
		
		// TODO: If it would be a set, there is no need to have this code here!!!! damn....
		if(stateTransitionTable.get(startState).contains(new Pair<TRANSITION_TYPE, STATE_TYPE>(transition, endState))) {
			return;
		}
		
		stateTransitionTable.get(startState).add(new Pair<TRANSITION_TYPE, STATE_TYPE>(transition, endState));
	}	
	
	public Set<STATE_TYPE> getEndStates() {
		return endStates;
	}

	public void setEndStates(IdentitySet<STATE_TYPE> endStates) {
		this.endStates = endStates;
	}

	public STATE_TYPE getStartState() {
		return startState;
	}

	public void setStartState(STATE_TYPE startState) {
		addState(startState);
		this.startState = startState;
	}
	
	public void cleanEndStates() {
		endStates = new IdentitySet<STATE_TYPE>();
	}
	
	public void addEndState(STATE_TYPE aState) {
		addState(aState);
		endStates.add(aState);
	}
	
	public HashMap<STATE_TYPE,Vector<Pair<TRANSITION_TYPE,STATE_TYPE>>> getStateTransitionTable() {
		return stateTransitionTable;
	}
	
	public String toString() {
		StringBuffer ret = new StringBuffer();
		ret.append("Start: \n");
		ret.append("  "+ this.getStartState() + "\n"); 

		ret.append("States: \n");
		for (STATE_TYPE state : stateTransitionTable.keySet()) {
			ret.append("  "+ state); 
		}
		ret.append("\n");

		ret.append("EndStates: \n");
		for (STATE_TYPE state : this.getEndStates()) {
			ret.append("  "+ state); 
		}
		ret.append("\n");

		ret.append("Transitions: \n");
		for (STATE_TYPE state : stateTransitionTable.keySet()) {
			for(Pair<TRANSITION_TYPE, STATE_TYPE> transition: stateTransitionTable.get(state)) {
				ret.append( "  " + state + " " + transition.getLeft() + " " + transition.getRight() + "\n");
			}
		}

		return ret.toString();
	}	
	
	public Set<STATE_TYPE> getAllStates() {
		return stateTransitionTable.keySet();
	}
	
	public  GraphicalGraph toGraph(GraphFactoryGraphical2D  factory) {
		   GraphicalGraph g = factory.createGraph();
		
		for (STATE_TYPE aState : this.getAllStates()) {
			g.addNode(aState.toString());
		}

		for (STATE_TYPE aState : stateTransitionTable.keySet()) {
			for (Pair<TRANSITION_TYPE, STATE_TYPE> aTransition : stateTransitionTable.get(aState)) {
				Edge2DMultiPoints edge = factory.createEdge(
						g.getNode(aState.toString()), 
						g.getNode(aTransition.getRight().toString()), 
						aTransition.getLeft().toString()
				);
				edge.setName(aTransition.getLeft().toString());
				g.addEdge(edge);
			}
		}
		
		return g;
	}
	
	public Collection<Triple<STATE_TYPE, TRANSITION_TYPE, STATE_TYPE>> allTransitions() {
		ArrayList ret = new ArrayList();
		
		for (STATE_TYPE aState : stateTransitionTable.keySet()) {
			for (Pair<TRANSITION_TYPE, STATE_TYPE> aTransition : stateTransitionTable.get(aState)) {
				ret.add(new Triple(aState, aTransition.getLeft(), aTransition.getRight()));
			}
		}
		
		return ret;
	}	
	
	public void print() {
		System.out.println(this.toString());
	}	

	
}
