/*******************************************************************************
 * /**
 * * The MIT License (MIT)
 * *
 * * Copyright (c) 2015, 2016 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * * 
 * * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * * software and associated documentation files (the "Software"), to deal in the Software 
 * * without restriction, including without limitation the rights to use, copy, modify, 
 * * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * * The above copyright notice and this permission notice shall be included in all copies or 
 * * substantial portions of the Software.
 *
 * * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * **/
package shapuc.automata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.Vector;

import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.util.IdentitySet;
import shapuc.util.Pair;
import shapuc.util.cmd.CmdP2;
import shapuc.util.cmd.CmdVoidP1;
/**
 * A DEA is a deterministic state machine. It is configured with the StateType, i.e. the kind of state
 * being used, the TransitionType and the InputType. In its easiest form, a DEA is of kind
 *   DEA<Object, String, String> 
 * where a String is an input and a String is a transition (i.e. the edge between nodes). 
 * 
 * @author stefan
 */
public class DEA<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> extends Automata<STATE_TYPE, TRANSITION_TYPE, INPUT_TYPE> {
	
	public DEA(
			TransitionCmd transitionHandlesAndAcceptsInputCmd,
			StateActivationCmd stateReachedCmd) {
		super(transitionHandlesAndAcceptsInputCmd, stateReachedCmd);
	}

	public STATE_TYPE currentState;
	
	public STATE_TYPE getCurrentState() {
		return currentState;
	}
	
	public void start() {
		if (startState==null) throw new RuntimeException("StartState is null -> set start state first");
			reachedState(startState);
	}		
	
	public void reset() {
		start();
	}		
	
	@Override
	public Vector<Pair<TRANSITION_TYPE, STATE_TYPE>> allTransitionsFromCurrent() {
		if (!stateTransitionTable.containsKey(currentState))
			throw new RuntimeException("Invalid Statement for DEA: current state not contained in stateTransitionTable");
		return stateTransitionTable.get(currentState);
	}
	
	/*
	 * Goes to the next state (if corresponding transition agree)
	 * 
	 *   Template method (with PrimitiveMethods setCurrentState, transitionMatches) 
	*/	
	@Override
	public void gotoNext(INPUT_TYPE input) {
//System.out.println(input);		
		for (Pair<TRANSITION_TYPE, STATE_TYPE> transition : allTransitionsFromCurrent()) {
			if (transitionHandlesAndAcceptsInput(transition.getLeft(), input)) {
				reachedState(transition.getRight());
				return;
			}
		}

		throw new InvalidInputException("Input not accepted: " + input.toString() + " current state: " + currentState.toString());
	}

	/*
	 * Sets the current State.
	 * PrimitiveOperation for TemplateMethod gotoNext
	*/
	public void reachedState(STATE_TYPE aState) {
		currentState = aState; 
		super.reachedState(aState);
	}

	public boolean hasFinished() {
		return endStates.contains(currentState);
	}
	
	@Override
	public String toString() {
		return super.toString() + "\nCurrentState: " + currentState;
	}

	@Override
	public void gotoNextWithExceptionSet(Set<INPUT_TYPE> exceptionSet) {
		throw new RuntimeException("gotoNextWithExceptionSet not implemented for DEA => DEA just wants one next state");
		
	}


	
}
