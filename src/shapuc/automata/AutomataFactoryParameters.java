package shapuc.automata;

import shapuc.automata.commands.StateDoNothingCmd;
import shapuc.automata.commands.TransitionEqualityCheckCmd;
import shapuc.automata.commands.TransitionForwardCmd;
import shapuc.automata.commands.TransitionIdentityCheckCmd;
import shapuc.automata.commands.StateInvokeActiveCmd;
import shapuc.automata.commands.StateActivationCmd;
import shapuc.automata.commands.TransitionCmd;
import shapuc.util.cmd.CmdP2;
import shapuc.util.cmd.CmdVoidP1;

public class AutomataFactoryParameters {
	public static final TransitionCmd CUSTOM_CHECK = new TransitionForwardCmd();

	public static final TransitionCmd EQUALITY_CHECK = new TransitionEqualityCheckCmd();

	public static final TransitionCmd IDENTITY_CHECK = new TransitionIdentityCheckCmd();

	public static final StateActivationCmd ACTIVE_STATE = new StateInvokeActiveCmd();
	public static final StateActivationCmd INACTIVE_STATE = new StateDoNothingCmd();
}
