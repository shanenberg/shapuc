package shapuc.automata.nea;

import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

public class SetComparisons extends TestCase {
	
	public void test02() {
		Object o1 = new Object();
		Object o2 = new Object();

		HashSet<Set<Object>> setOfSets = new HashSet<Set<Object>>();
		
		
		HashSet s1 = new HashSet();
		s1.add(o1);
		s1.add(o2);
		setOfSets.add(s1);
		
		HashSet s2 = new HashSet();
		s2.add(o1);
		s2.add(o2);

		assertTrue(setOfSets.contains(s2));
		
	}	
	
	
	public void test01() {
		Object o1 = new Object();
		Object o2 = new Object();

		HashSet s1 = new HashSet();
		HashSet s2 = new HashSet();
		
		s1.add(o1);
		s2.add(o2);
		
		assertFalse(s1.equals(s2));
		
		s1.add(o2);
		assertFalse(s1.equals(s2));

		s2.add(o1);
		assertTrue(s1.equals(s2));
	}
}
