package shapuc.automata.nea;

import java.util.ArrayList;

import junit.framework.TestCase;
import shapuc.regex.RegEx;
import shapuc.regex.operations.RegExFactory;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.tokenizer.TokenizerFactory;
import shapuc.streams.PositionStream;
import shapuc.streams.StringBufferStream;
import shapuc.streams.TokenStream;
import shapuc.util.Pair;

public class RegExTokenizerConstruction extends TestCase {

		public void test01_OR_A_B() throws Exception {
			
			RegEx regex = new RegExFactory() {public RegEx regEx() {return 
					OR(TOKEN('a'), TOKEN('b'));}}.regEx();
			
			ArrayList l = new ArrayList();
			l.add(new Pair("Dummy", regex));
			
			Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l, "a");
			TokenStream<PositionStream<Character>> tokenStream = tokenizer.resultTokenStream;
			tokenizer.createTokenFromStream();
			
			assertEquals(1, tokenStream.length());
			assertEquals("a", tokenizer.resultTokenStream.get(0).getValueString());
			assertEquals("Dummy", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());
			
			tokenizer.setStringStream("b");
			tokenizer.createTokenFromStream();
			assertEquals(1, tokenizer.resultTokenStream.length());
			assertEquals("b", tokenizer.resultTokenStream.get(0).getValueString());
			assertEquals("Dummy", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());

			tokenizer.setStringStream("abXXX");
			tokenizer.resetAutomata();
			
			tokenizer.createTokenFromStream();
			assertEquals(1, tokenizer.resultTokenStream.length());
			assertEquals("a", tokenizer.resultTokenStream.get(0).getValueString());
		
	}
	
}
