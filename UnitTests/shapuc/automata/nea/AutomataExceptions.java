package shapuc.automata.nea;

import junit.framework.TestCase;
import shapuc.automata.AutomataFactoryParameters;
import shapuc.automata.InvalidInputException;
import shapuc.automata.NEA;
import shapuc.automata.NEAFactory;

/**
 * Checks whether an automata throws an InvalidInputException when unknown input occurs.
 * @author stefan
 *
 */
public class AutomataExceptions extends TestCase {
	
		public void testNEAException() {
			AutomataFactoryParameters p = new AutomataFactoryParameters();
			NEAFactory<String, String, String> f = 
					new NEAFactory<String, String, String>(p.EQUALITY_CHECK,p.INACTIVE_STATE);

			NEA<String, String, String> automata = f.createUninitializedAutomata();
			f.initializeAutomata(automata, "S1", "ε", 
					new Object[][] {new Object[] {"S1", "X", "S2"}},
				    new String[]{"S2"});
			automata.start();
			
			try {
				automata.gotoNext("Z");
				assertTrue(false);
			} catch (InvalidInputException ex) {
				assertTrue(true);
				return;
			}
			
			assertTrue(false);

		}
}