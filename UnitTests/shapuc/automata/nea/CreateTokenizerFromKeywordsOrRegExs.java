package shapuc.automata.nea;

import java.util.ArrayList;

import junit.framework.TestCase;
import shapuc.automata.NEA;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.regex.RegEx;
import shapuc.regex.operations.RegExFactory;
import shapuc.regex.operations.RegExVisitor;
import shapuc.regex.operations.RegExVisitorVoid;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.tokenizer.TokenizerFactory;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.streams.StringBufferStream;
import shapuc.streams.TokenStream;
import shapuc.util.Pair;

public class CreateTokenizerFromKeywordsOrRegExs extends TestCase {
	
	public void test02_Precedence() throws Exception {
		
		
		ArrayList l = new ArrayList();
		l.add(new Pair("Dummy", new RegExFactory() {public RegEx regEx() {return 
				OR(TOKEN('a'), TOKEN('b'));}}.regEx()));
//		l.add("a");
//		l.add("b");
		
		StringBufferStream charInputStream = new StringBufferStream("a");
		TokenStream tokenOutputStream = new TokenStream(charInputStream);
		Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l);
		tokenizer.charStream = charInputStream;
		tokenizer.resultTokenStream = tokenOutputStream;
		tokenizer.createTokenFromStream();
		
		assertEquals(1, tokenOutputStream.length());
		assertEquals("a", tokenOutputStream.get(0).getValueString());
		assertEquals("Dummy", tokenOutputStream.get(0).getTokenType().getTokenTypeName());
		

	}	
	
	public void test01Chaotic() throws Exception {
		
		
		ArrayList l = new ArrayList();
		l.add("class");
		l.add("{");
		l.add("}");
		l.add("int");
		l.add("boolean");
		l.add("a");
		l.add("b");
		l.add("d");
		l.add("e");
		l.add("f");
		
		StringBufferStream charInputStream = new StringBufferStream("class");
		TokenStream tokenOutputStream = new TokenStream(charInputStream);
		Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l);
		tokenizer.charStream = charInputStream;
		tokenizer.resultTokenStream = tokenOutputStream;
		tokenizer.createTokenFromStream();
		
		assertEquals(1, tokenOutputStream.length());
		assertEquals("KEYWORD", tokenOutputStream.get(0).getTokenType().getTokenTypeName());
		assertEquals("class", tokenOutputStream.get(0).getValueString());

		charInputStream = new StringBufferStream("boolean");
		tokenOutputStream = new TokenStream(charInputStream);
		tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l);
		tokenizer.charStream = charInputStream;
		tokenizer.resultTokenStream = tokenOutputStream;
		tokenizer.createTokenFromStream();		
		assertEquals("KEYWORD", tokenOutputStream.get(0).getTokenType().getTokenTypeName());
		assertEquals("boolean", tokenOutputStream.get(0).getValueString());
		

	}
}
