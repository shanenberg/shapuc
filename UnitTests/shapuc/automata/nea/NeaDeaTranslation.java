package shapuc.automata.nea;

import java.util.ArrayList;

import junit.framework.TestCase;
import shapuc.automata.Automata;
import shapuc.automata.DEA;
import shapuc.automata.NEA;
import shapuc.automata.NEAFactory;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.automata.factories.NEAFromKeyword;
import shapuc.graphs.factories.GraphFactory;
import shapuc.graphs.factories.GraphFactoryGraphical;
import shapuc.j2d.DrawGraph;
import shapuc.regex.RegEx;
import shapuc.regex.operations.RegExFactory;
import shapuc.scanning.DEAWithStateSet;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.tokenizer.TokenizerDEA;
import shapuc.scanning.tokenizer.TokenizerFactory;
import shapuc.scanning.tokenizers.NEAForKeywordCreationVisitor;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.streams.StringBufferStream;
import shapuc.streams.TokenStream;
import shapuc.util.Pair;

public class NeaDeaTranslation extends TestCase {
	
	public void test_regExCharRange_01()  {
		
		ArrayList l = new ArrayList();
		 RegEx regex = new RegExFactory() {
			public RegEx regEx() {return RANGE('A', 'Z');}}.regEx();
			
		l.add(new Pair<String, RegEx>("DUMMY", regex));
		
		
		Tokenizer t = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l);
		TokenizerDEA dea = t.automata;
		
		t.setStringStream(".................");		

		dea.start();
		try {
			dea.gotoNext('a');
			assertFalse(true);
		} catch (Exception ex) {
			
		}

	
	}			
	
	public void test_KeywordRegEx_05()  {
		
		ArrayList l = new ArrayList();
		l.add("class");
		 RegEx regex = new RegExFactory() {
			public RegEx regEx() {
				return KLEENE(AND(AND(TOKEN('A'), TOKEN('B'), TOKEN('C')), TOKEN('A')));
			}
		}.regEx();
		l.add(new Pair<String, RegEx>("DUMMY", regex));
		
		
		Tokenizer t = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l);
		DEA<TokenizerState, CustomTransitionHandler<Character>, Character> dea = 
				(DEA<TokenizerState, CustomTransitionHandler<Character>, Character>) 
				t.automata;
		t.setStringStream(".................");		

		dea.start();
		dea.gotoNext('A');
		dea.gotoNext('B');
		dea.gotoNext('C');
		dea.gotoNext('A');
		dea.gotoNext('A');
		dea.gotoNext('B');
		dea.gotoNext('C');
		dea.gotoNext('A');

	
	}		
	
	
	public void test_Keyword_04()  {
		
		ArrayList l = new ArrayList();
		l.add("cla");
		l.add("a");
		l.add("f");
		
		Tokenizer t = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l);
		DEA<TokenizerState, CustomTransitionHandler<Character>, Character> dea = 
				(DEA<TokenizerState, CustomTransitionHandler<Character>, Character>) 
				t.automata;
		t.setStringStream(".................");		

		dea.start();
		
		dea.gotoNext((Character) 'c');
		dea.gotoNext((Character) 'l');
		dea.gotoNext((Character) 'a');

		
	}		
	
	
	public void test_Keyword_03()  {
		
		ArrayList l = new ArrayList();
		l.add("ABC");
		l.add("ABD");
		
		Tokenizer t = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l);
		DEA<TokenizerState, CustomTransitionHandler<Character>, Character> dea = 
				(DEA<TokenizerState, CustomTransitionHandler<Character>, Character>) 
				t.automata;
		t.setStringStream(".................");		
		
		dea.start();
		dea.gotoNext((Character) 'A');
		dea.gotoNext((Character) 'B');
		dea.gotoNext((Character) 'D');
		
		dea.start();
		dea.gotoNext((Character) 'A');
		dea.gotoNext((Character) 'B');
		dea.gotoNext((Character) 'C');		
	}		
	
	public void test_Keyword_02()  {

		ArrayList l = new ArrayList();
		l.add("ABC");
		
		Tokenizer t = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l);
		DEA<TokenizerState, CustomTransitionHandler<Character>, Character> dea = 
				(DEA<TokenizerState, CustomTransitionHandler<Character>, Character>) 
				t.automata;
		t.setStringStream("ABC");
		
		assertEquals(5, dea.getAllStates().size());
		
		dea.start();
		dea.gotoNext((Character) 'A');
		dea.gotoNext((Character) 'B');
		dea.gotoNext((Character) 'C');

	}	
	
	public void test_Keyword_01()  {
		
		ArrayList l = new ArrayList();
		l.add("HW");
		
		Tokenizer t = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(l);
		DEA<TokenizerState, CustomTransitionHandler<Character>, Character> dea = 
				(DEA<TokenizerState, CustomTransitionHandler<Character>, Character>) 
				t.automata;
		t.setStringStream("HW");
		
		assertEquals(4, dea.getAllStates().size());
		dea.start();
		dea.gotoNext((Character) 'H');
		dea.gotoNext((Character) 'W');

	}
}
