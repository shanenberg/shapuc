package shapuc.regex;

import junit.framework.TestCase;
import shapuc.regex.*;
import shapuc.regex.operations.RegExFactory;

public class RegExExample extends TestCase {
	
	public void test01() throws Exception {
		
		RegEx r;
		
		r = regExConstruction(new RegExFactory(){public RegEx regEx() {return 
			TOKEN('A');
		}}); 
		
		assertTrue(r instanceof RegExToken);
		assertEquals("A", r.regExString());
		
		
		r = regExConstruction(new RegExFactory(){public RegEx regEx() {
			return AND(TOKEN('A'), TOKEN('B'));
		}});
		
		assertTrue(r instanceof RegExAnd);
		assertEquals("AB", r.regExString());

		r = regExConstruction(new RegExFactory(){public RegEx regEx() {
			return AND(OR(TOKEN('A'), TOKEN('B')), OR(TOKEN('C'), TOKEN('D')));
		}});

		assertTrue(r instanceof RegExAnd);
		assertEquals("A|BC|D", r.regExString());
		
		r = regExConstruction(new RegExFactory(){public RegEx regEx() {
			return AND(BRACKET(OR(TOKEN('A'), TOKEN('B'))), BRACKET(OR(TOKEN('C'), TOKEN('D'))));
		}});

		assertTrue(r instanceof RegExAnd);
		assertEquals("(A|B)(C|D)", r.regExString());
	}
	
	public RegEx regExConstruction(RegExFactory f) {
		return f.regEx(); 
	}
}
