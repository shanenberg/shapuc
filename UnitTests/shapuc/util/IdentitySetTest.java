package shapuc.util;

import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

public class IdentitySetTest extends TestCase {
	
	public void testAddAll() throws Exception {
		IdentitySet<Object> any = new IdentitySet<Object>();
		
		Set aSet = new HashSet();
		aSet.add(new Object());
		aSet.add(new Object());
		aSet.add(new Object());

		any.addAll(aSet);
		assertEquals(3, aSet.size());

		any.addAll(aSet);
		assertEquals(3, aSet.size());
		
	}
}
