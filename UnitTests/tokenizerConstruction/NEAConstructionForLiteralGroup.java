package tokenizerConstruction;

import java.util.List;
import java.util.Vector;

import junit.framework.TestCase;
import shapuc.automata.NEA;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.regex.RegEx;
import shapuc.regex.RegExLiteralGroup;
import shapuc.scanning.KeywordOrRegEx;
import shapuc.scanning.KeywordOrRegExRegEx;
import shapuc.scanning.NEAFromKeywordTokenConstructor;
import shapuc.scanning.Tokenizer;
import shapuc.scanning.tokenizer.NEACombinator;
import shapuc.scanning.tokenizer.NEAsCreation;
import shapuc.scanning.tokenizer.TokenizerNEA;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.util.Triple;

public class NEAConstructionForLiteralGroup extends TestCase {
	
	public void test_LiteralGroupConstruction() {
		Tokenizer t = new Tokenizer();
		List<Object> l = new Vector<Object>();
		l.add(new KeywordOrRegExRegEx("group", new RegExLiteralGroup('A', 'F')));
		
		Vector<TokenizerNEA> neas = 
				new NEAsCreation().createNEAsFromKeywordsOrRegExs(t, l);
		
		assertEquals(1, neas.size());
		assertEquals(2, neas.get(0).getAllStates().size());
		assertEquals(1, neas.get(0).allTransitions().size());
		assertTrue(
				((Triple) (neas.get(0).allTransitions().toArray()[0])).getMiddle() 
				instanceof CharRangeTransition);
		
		CharRangeTransition transition = (CharRangeTransition) ((Triple) (neas.get(0).allTransitions().toArray()[0])).getMiddle();
		assertTrue(transition.handles('B'));
		assertTrue(!transition.handles('X'));
		
		NEA<TokenizerState, CustomTransitionHandler<Character>, Character> nea = 
				new NEACombinator().combineNEAs(t, neas);
		assertEquals(4, nea.getAllStates().size());
	}
}
