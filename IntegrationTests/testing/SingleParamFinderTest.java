package testing;

import java.lang.reflect.Method;

import junit.framework.TestCase;
import shapuc.refvis.exceptions.VisitorAmbiguityException;
import shapuc.refvis.reflectiveVisitor.impl.SingleParameterMethodFinder;

public class SingleParamFinderTest extends TestCase {

	/**
	 * Tests for ambiguitiy-Error (superclass and interface in Visitor)
	 * @author stefan
	 *
	 */
	interface I07_1 {}
	interface I07_2 {}
	class TargetClass07_2 implements I07_1, I07_2 {}
	class VisitorClass07 {
		public void visit(I07_1 param) {}
		public void visit(I07_2 param) {}}
	
	public void test07_AmbiguityError_02() throws NoSuchMethodException, SecurityException {

		try {
			Method m = findMethod("visit", VisitorClass07.class, TargetClass07_2.class);
			assertTrue("An exception should be thrown!", false);
		} catch (shapuc.refvis.exceptions.VisitorAmbiguityException ex) {
			assertTrue("Everything fine", true);
		}
		
	}		

	/**
	 * Tests for ambiguitiy-Error (superclass and interface in Visitor)
	 * @author stefan
	 *
	 */
	interface I06 {}
	class TargetClass06_1 implements I06 {}
	class TargetClass06_2 extends TargetClass06_1 implements I06 {}
	class VisitorClass06 {
		public void visit(TargetClass06_1 param) {}
		public void visit(I06 param) {}}
	
	public void test06_AmbiguityError_01() throws NoSuchMethodException, SecurityException {

		try {
			Method m = findMethod("visit", VisitorClass06.class, TargetClass06_2.class);
			assertTrue("An exception should be thrown!", false);
		} catch (VisitorAmbiguityException ex) {
			assertTrue("Everything fine", true);
		}
		
	}		
	
	
	/**
	 * Tests whether superclass can be used in visitor definition.
	 * @author stefan
	 *
	 */
	interface I05 {}
	class TargetClass05_1 implements I05 {}
	class TargetClass05_2 extends TargetClass05_1 {}
	class VisitorClass05 {
		public void visit(TargetClass05_1 param) {}}
	
	public void test05_SuperClass_In_Visitor() throws NoSuchMethodException, SecurityException {
		Method m = findMethod("visit", VisitorClass05.class, TargetClass05_2.class);
		
		assertEquals(VisitorClass05.class.getMethod("visit", TargetClass05_1.class), m);
		
	}		
	
	/**
	 * Tests whether an interface in a class can be used in the visitor definition.
	 * 
	 * @author stefan
	 *
	 */
	interface I04 {}
	class TargetClass04_1 implements I04 {}
	class TargetClass04_2 extends TargetClass04_1 {}
	class VisitorClass04 {
		public void visit(I04 t) {}}
	
	public void test04_Superclass_Interface_In_Visitor_Interface() throws NoSuchMethodException, SecurityException {
		Method m = findMethod("visit", VisitorClass04.class, TargetClass04_2.class);
		
		assertEquals(VisitorClass04.class.getMethod("visit", I04.class), m);
		
	}		
	
	/**
	 * Tests whether an interface in a class can be used in the visitor definition.
	 * 
	 * @author stefan
	 *
	 */
	interface I03 {}
	class TargetClass03_1 implements I03 {}
	class TargetClass03_2 {}
	class VisitorClass03 {
		public void visit(I03 t) {}
		public void visit(TargetClass03_2 t) {}}
	
	public void test03_Interface_In_Visitor_Interface() throws NoSuchMethodException, SecurityException {
		Method m = findMethod("visit", VisitorClass03.class, TargetClass03_1.class);
		
		assertEquals(VisitorClass03.class.getMethod("visit", I03.class), m);
	}	
	
	class TargetClass02_1 {}
	class TargetClass02_2 {}
	class VisitorClass02 {
		public void visit(TargetClass02_1 t) {}
		public void visit(TargetClass02_2 t) {}}
	
	public void test02_Two_Classes_In_Visitor_Interface() throws NoSuchMethodException, SecurityException {
		Method m = findMethod("visit", VisitorClass02.class, TargetClass02_1.class);

		// visit-Methode für ersten Klasse TargetClass02_1
		assertEquals(VisitorClass02.class.getMethod("visit", TargetClass02_1.class), m);
		
		// visit-Methode für zweite Klasse TargetClass02_2
		m = findMethod("visit", VisitorClass02.class, TargetClass02_2.class);
//		assertEquals(VisitorClass02.class.getMethod("visit", TargetClass02_2.class), m);
	}
	
	
	class TargetClass01 {}
	class VisitorClass01 {
		public void visit(TargetClass01 t) {}}
	
	public void test01_Class_Directly_In_Visitor_Interface() throws NoSuchMethodException, SecurityException {
		Method m = findMethod("visit", VisitorClass01.class, TargetClass01.class);
		assertEquals(VisitorClass01.class.getMethod("visit", TargetClass01.class), m);
		
	}
	
	public Method findMethod(String name, Class visitor, Class parameterType) {
		return new SingleParameterMethodFinder(name, visitor, parameterType).findMethod();
	}
}
