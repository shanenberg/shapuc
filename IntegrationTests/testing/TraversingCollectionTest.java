package testing;

import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreeNode;

import junit.framework.TestCase;
import shapuc.refvis.custom.TreeNodeReflectiveImplAll;
import shapuc.refvis.custom.VisitorReflective;

/**
 * Integration test checking whether the whole Reflective Visitor works.
 *  
 * @author stefan
 *
 */
public class TraversingCollectionTest extends TestCase {
	public void test01() {}
//
//	String result02 ="";
//	class Tree02 extends TreeNodeReflectiveImplAll implements TreeIF02 {
//		public String value;
//		public Tree02 next;
//		public List others = new ArrayList();
//		public Tree02(String value, Tree02 next) { this.value = value; this.next = next;}
//	}
//	
//	interface TreeIF02 extends TreeNode {}
//	public class Visitor02 extends VisitorReflective<TreeIF02, Object>{
//		public Object visit(Tree02 n) {
//			result02 = result02 + n.value;
//			this.visitAllChildren(n); return null;
//		}
//	}
//	
//	
//	String result01 ="";
//	class Tree01 extends TreeNodeReflectiveImplAll implements TreeIF01 {
//		public String value;
//		public Tree01 next;
//		public List others = new ArrayList();
//		public Tree01(String value, Tree01 next) { this.value = value; this.next = next;}
//	}
//	
//	interface TreeIF01 extends TreeNode {}
//	public class Visitor01 extends VisitorReflectiveAll<Object>{
//		public Object visit(Tree01 n) {
//			result01 = result01 + n.value;
//			this.visitAllChildren(n); return null;
//		}
//	}
//	
//	public void test01() {
//		Tree01 n = new Tree01("A", new Tree01("B", null)); 
//		n.others.add(new Tree01("C", null));
//		Visitor01 v = new Visitor01();
//		v.invokeVisit(n);
//		assertEquals(result01, "ABC");
//	}
//	
		
}
