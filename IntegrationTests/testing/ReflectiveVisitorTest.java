package testing;

import junit.framework.TestCase;
import shapuc.refvis.custom.VisitorReflectiveAll;

/**
 * Integration test checking whether the whole Reflective Visitor works.
 *  
 * @author stefan
 *
 */
public class ReflectiveVisitorTest extends TestCase {

	int state_01=0;
	interface NodeInterface_01 {}
	public class Node implements NodeInterface_01 {}
	public class Visitor01 extends VisitorReflectiveAll<Object>{
		public Object visit(NodeInterface_01 n) {
			state_01=1; return null;
		}
	}
	
	public void test01() {
		Node n = new Node();
		Visitor01 v = new Visitor01();
		v.invokeVisit(n);
		assertTrue("Right visit invoke should have changed state", state_01==1);
	}
	
		
}
