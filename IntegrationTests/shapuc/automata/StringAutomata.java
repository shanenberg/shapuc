package shapuc.automata;

import junit.framework.TestCase;
import shapuc.automata.custom.WildcardStringTransition;

public class StringAutomata extends TestCase {

	public void test_NEA_Complex05() {
		
		String[][] transitions = {
				new String[] {"S1", "S11"},	
				new String[] {"S11", "S12"},	
				new String[] {"S12", "x", "S13"},	
				new String[] {"S1", "S21"},	
				new String[] {"S21", "S22"},	
				new String[] {"S22", "x", "S23"},	
		};
		String[] ends = new String[] {"S13", "23"};

		AutomataFactoryParameters p = new AutomataFactoryParameters();
		NEAFactory<Object, String, String> f = 
				new NEAFactory<Object, String, String>(p.EQUALITY_CHECK,p.INACTIVE_STATE);

		NEA<Object, String, String> automata = f.createUninitializedAutomata();
		f.initializeAutomata(automata, "S1", "xxx", transitions, ends);
		automata.start();

		assertEquals(5, automata.getCurrentStates().size());
	}		
	
	public void test_NEA_Complex04() {
		
		String[][] transitions = {
				new String[] {"S1", "S11"},	
				new String[] {"S11", "S12"},	
				new String[] {"S1", "S21"},	
				new String[] {"S21", "S22"},	
		};
		String[] ends = new String[] {"S13", "23"};

		AutomataFactoryParameters p = new AutomataFactoryParameters();
		NEAFactory<Object, String, String> f = 
				new NEAFactory<Object, String, String>(p.EQUALITY_CHECK,p.INACTIVE_STATE);

		NEA<Object, String, String> automata = f.createUninitializedAutomata();
		f.initializeAutomata(automata, "S1", "xxx", transitions, ends);
		automata.start();

		assertTrue(automata.getCurrentStates().contains("S1"));
		assertTrue(automata.getCurrentStates().contains("S11"));
		assertTrue(automata.getCurrentStates().contains("S12"));
		assertTrue(automata.getCurrentStates().contains("S21"));
		assertTrue(automata.getCurrentStates().contains("S22"));
		assertEquals(5, automata.getCurrentStates().size());
	}		
	
	public void test_NEA_Complex03() {
		
		String[][] transitions = {
				new String[] {"S1", "S11"},	
				new String[] {"S1", "S21"},	
		};
		String[] ends = new String[] {"S13", "23"};

		AutomataFactoryParameters p = new AutomataFactoryParameters();
		NEAFactory<Object, String, String> f = 
				new NEAFactory<Object, String, String>(p.EQUALITY_CHECK,p.INACTIVE_STATE);

		NEA<Object, String, String> automata = f.createUninitializedAutomata();
		f.initializeAutomata(automata, "S1", "xxx", transitions, ends);
		automata.start();

		assertEquals(3, automata.getCurrentStates().size());
	}		

	public void test_NEA_Complex01() {
		
		String[][] transitions = {
				new String[] {"S1", "S2"},	
				new String[] {"S2", "S3"},	
				new String[] {"S3", "hallo", "S4"},	
				new String[] {"S4", "S5"},	
		};
		String[] ends = new String[] {"S4", "S5"};

		AutomataFactoryParameters p = new AutomataFactoryParameters();
		NEAFactory<Object, String, String> f = 
				new NEAFactory<Object, String, String>(p.EQUALITY_CHECK,p.INACTIVE_STATE);

		NEA<Object, String, String> automata = f.createUninitializedAutomata();
		f.initializeAutomata(automata, "S1", "xxx", transitions, ends);
		automata.start();

		
		automata.start();
		assertTrue(!automata.hasFinished());
		automata.gotoNext("hallo");
		assertTrue(automata.hasFinished());
		assertTrue(automata.currentStates.contains("S4"));
		assertTrue(automata.currentStates.contains("S5"));
		assertEquals(2, automata.currentStates.size());
		
		
	}			
	
	public void testNEAWithSpontaneousTransitions() throws Exception {
			
			String[][] transitions = {
					new String[] {"S1", "S2"},	
					new String[] {"S2", "S3"},	
					new String[] {"S3", "hallo", "S4"},	
					new String[] {"S4", "S5"},	
			};
			String[] ends = new String[] {"S4", "S5"};

			AutomataFactoryParameters p = new AutomataFactoryParameters();
			NEAFactory<String, String, String> factory = new NEAFactory<String, String, String>(p.EQUALITY_CHECK, p.INACTIVE_STATE);
			NEA<String, String, String> automata = factory.createUninitializedAutomata();
			factory.initializeAutomata(automata, "S1", "ε", transitions, ends);
			
			
			automata.start();
			assertTrue(!automata.hasFinished());
			automata.gotoNext("hallo");
			assertTrue(automata.hasFinished());
			assertTrue(automata.currentStates.contains("S4"));
			assertTrue(automata.currentStates.contains("S5"));
			assertEquals(2, automata.currentStates.size());
			
	}
	
	
//	public void test_WildcardOnNEA() throws Exception {
//
//		AutomataFactoryParameters p = new AutomataFactoryParameters();
//		NEAFactory<Object, WildcardStringTransition, String> f = 
//				new NEAFactory<Object, WildcardStringTransition, String>(p.CUSTOM_CHECK,p.INACTIVE_STATE);
//		
//		NEA<Object, WildcardStringTransition, String> nea = f.createUninitializedAutomata();
//		nea.setSpontaneousTransition(new WildcardStringTransition("?"));
//		Object start = new Object(), end= new Object();
//		
//		nea.addTransition(start, new WildcardStringTransition("*"), end);
//		nea.addState(start); nea.addEndState(end);
//		nea.setStartState(start);
//		nea.start();
//		 
//		nea.gotoNext("blblblb");
//		assertTrue(nea.hasFinished());
//	}		
//	
//	public void test_WildcardOnDEA() throws Exception {
//
//		AutomataFactoryParameters p = new AutomataFactoryParameters();
//		DEAFactory<Object, WildcardStringTransition, String> f = 
//				new DEAFactory<Object, WildcardStringTransition, String>(p.CUSTOM_CHECK,p.INACTIVE_STATE);
//		
//		DEA<Object, WildcardStringTransition, String> dea = f.createUninitializedAutomata();
//
//		Object start = new Object(), end= new Object();
//		
//		dea.addTransition(start, new WildcardStringTransition("*"), end);
//		dea.addState(start); dea.addEndState(end);
//		dea.setStartState(start);
//		dea.start();
//		 
//		dea.gotoNext("blblblb");
//		assertTrue(dea.hasFinished());
//	}	
	
	public void test_IdentityNEA01() throws Exception {

		AutomataFactoryParameters p = new AutomataFactoryParameters();
		NEAFactory<Object, String, String> f = 
				new NEAFactory<Object, String, String>(p.IDENTITY_CHECK,p.INACTIVE_STATE);

		NEA<Object, String, String> nea = f.createUninitializedAutomata();
		f.initializeAutomata(nea, "S1", "zzZ", new Object[][] {new Object[] {"S1", "Hallo",  "S2"}}, new Object[]{"S2"});
		nea.start();
		
		try {
			nea.gotoNext(new String("Hallo")); // NEEDS TO FAIL -> Identity "Hallo" different.
			assertTrue(false);
		} catch (Exception ex) {}
		assertFalse(nea.hasFinished());
		nea.gotoNext("Hallo");
		assertTrue(nea.hasFinished());
		
		try {
			nea.gotoNext("Hallo"); // NEEDS TO FAIL -> DEA has already finished.
			assertTrue(false);
		} catch (Exception ex) {}
		
	}		
	
	public void test_IdentityDEA01() throws Exception {

		AutomataFactoryParameters p = new AutomataFactoryParameters();
		DEAFactory<Object, String, String> f = 
				new DEAFactory<Object, String, String>(p.IDENTITY_CHECK,p.INACTIVE_STATE);

		DEA<Object, String, String> dea = f.createUninitializedAutomata();
		f.initializeAutomata(dea, "S1", new Object[][] {new Object[] {"S1", "Hallo",  "S2"}}, new Object[]{"S2"});
		dea.start();

		try {
			dea.gotoNext(new String("Hallo")); // NEEDS TO FAIL -> Identity "Hallo" different.
			assertTrue(false);
		} catch (Exception ex) {}
		assertFalse(dea.hasFinished());
		dea.gotoNext("Hallo");
		assertTrue(dea.hasFinished());
		
		try {
			dea.gotoNext("Hallo"); // NEEDS TO FAIL -> DEA has already finished.
			assertTrue(false);
		} catch (Exception ex) {}
		
	}	
	
	/*
	 * The same as previous test.....but works with transition object with different identity.
	 */
	public void test_EqualityNEA02() throws Exception {
		AutomataFactoryParameters p = new AutomataFactoryParameters();
		NEAFactory<Object, String, String> f = 
				new NEAFactory<Object, String, String>(p.EQUALITY_CHECK,p.INACTIVE_STATE);

		NEA<Object, String, String> dea = f.createUninitializedAutomata();
		f.initializeAutomata(dea, "S1", "xxx", new Object[][] {new Object[] {"S1", "HalloHallo",  "S2"}}, new Object[]{"S2"});
		dea.start();

		assertFalse(dea.hasFinished());
		dea.gotoNext(new String("HalloHallo")); // Here an object with a different identity goes in
	}
			
	
	/*
	 * The same as previous test.....but works with transition object with different identity.
	 */
	public void test_EqualityDEA02() throws Exception {
		AutomataFactoryParameters p = new AutomataFactoryParameters();
		DEAFactory<Object, String, String> f = 
				new DEAFactory<Object, String, String>(p.EQUALITY_CHECK,p.INACTIVE_STATE);

		DEA<Object, String, String> dea = f.createUninitializedAutomata();
		f.initializeAutomata(dea, "S1", new Object[][] {new Object[] {"S1", "HalloHallo",  "S2"}}, new Object[]{"S2"});
		dea.start();

		assertFalse(dea.hasFinished());
		dea.gotoNext(new String("HalloHallo"));
		assertTrue(dea.hasFinished());
	}	
	
	public void test_EqualityNEA01() throws Exception {

		AutomataFactoryParameters p = new AutomataFactoryParameters();
		NEAFactory<Object, String, String> f = 
				new NEAFactory<Object, String, String>(p.EQUALITY_CHECK,p.INACTIVE_STATE);

		NEA<Object, String, String> dea = f.createUninitializedAutomata();
		f.initializeAutomata(dea, "S1", "xxx", new Object[][] {new Object[] {"S1", "Hallo",  "S2"}}, new Object[]{"S2"});
		dea.start();

		assertFalse(dea.hasFinished());
		dea.gotoNext("Hallo");
		assertTrue(dea.hasFinished());
	}	
	
	public void test_EqualityDEA01() throws Exception {

		AutomataFactoryParameters p = new AutomataFactoryParameters();
		DEAFactory<Object, String, String> f = 
				new DEAFactory<Object, String, String>(p.EQUALITY_CHECK,p.INACTIVE_STATE);

		DEA<Object, String, String> dea = f.createUninitializedAutomata();
		f.initializeAutomata(dea, "S1", new Object[][] {new Object[] {"S1", "Hallo",  "S2"}}, new Object[]{"S2"});
		dea.start();

		assertFalse(dea.hasFinished());
		dea.gotoNext("Hallo");
		assertTrue(dea.hasFinished());
	}
	
	
}
