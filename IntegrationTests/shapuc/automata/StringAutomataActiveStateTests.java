package shapuc.automata;

import junit.framework.TestCase;
import shapuc.util.returnObject.ReturnObject;

public class StringAutomataActiveStateTests extends TestCase {
	
	public interface MyState extends ActiveState {}
	class MyStateActive implements MyState {
		public final ReturnObject ro; 
		public MyStateActive(ReturnObject ro) {super();this.ro = ro;}
		public void reachedState() {ro.setter.set("Hallo");}
	}
	
	class MyStateSilent implements MyState {public void reachedState() {}}	
	
	public void tests() throws Exception {
				final ReturnObject<String> ret = new ReturnObject<String>(); ret.setter.set("");
				
				AutomataFactoryParameters p = new AutomataFactoryParameters();
				NEAFactory<Object, String, String> f = 
						new NEAFactory<Object, String, String>(p.EQUALITY_CHECK,p.ACTIVE_STATE);

				NEA<Object, String, String> automata = f.createUninitializedAutomata();
		
				MyState start = new MyStateSilent();
				MyState active = new MyStateActive(ret);
				
				automata.setSpontaneousTransition(new String("blabla"));
				automata.addTransition(start, "t", active);
				automata.setStartState(start);
				automata.addEndState(active);
				automata.start(); 
				
				assertEquals("", ret.getReturn());
				automata.gotoNext("t");
				assertEquals("Hallo", ret.getReturn());
	}
}
