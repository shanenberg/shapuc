package shapuc.automata;

import junit.framework.TestCase;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.scanning.tokenizer.DEAFollowUpTransitionStateCollector;
import shapuc.scanning.tokenizers.TokenizerState;

public class CharRangeTransitionTests extends TestCase {
	
	public void test01() {
		
		DEAFollowUpTransitionStateCollector collector = 
				new DEAFollowUpTransitionStateCollector();
		
		CharRangeTransition r1 = new CharRangeTransition('a', 'd');
		CharRangeTransition r2 = new CharRangeTransition('c', 'f');
		// es muss als Ergebnis: a,b(1),c,d(1,2), und e,f(2) 
		
		collector.addRangeCharTransition(r1, new TestState(1));
		collector.addRangeCharTransition(r2, new TestState(2));
		
		assertEquals(3, r1.rangesWithRemovedRange(r2).getRight().length);
	}
	
	class TestState implements TokenizerState {
		public int id;
		public TestState(int id) {super(); this.id = id; }
		public void reachedState() {}
		public boolean isAcceptor() {return false;}
		public int getPriority() {return id;}

		public TokenizerState clone() {
			return new TestState(id);
		}
		@Override
		public String toString() {
			return "TestState [id=" + id + "]";
		}
	}	
	
}
