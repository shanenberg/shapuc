package shapuc.graphs.testing;

import junit.framework.TestCase;
import shapuc.graphs.Edge;
import shapuc.graphs.Graph;
import shapuc.graphs.Node;
import shapuc.graphs.factories.GraphFactory;
import shapuc.graphs.factories.GraphFactoryGraphical;
import shapuc.graphs.factories.GraphFactorySimple;
import shapuc.graphs.graphical.GraphicalNode;
import shapuc.graphs.simple.SimpleEdge;
import shapuc.graphs.simple.SimpleNode;

public class TestGraph extends TestCase {
	
	public <NodeType extends Node<NodeType>, EdgeType extends Edge<NodeType>> 
		Graph<NodeType, EdgeType> createExampleGraph(GraphFactory<NodeType, EdgeType> graphFactory) {

		Graph<NodeType, EdgeType> graph = graphFactory.createGraph();
		
		graph.addNode("A");
		graph.addNode("B");
		graph.addNode("E");
		graph.addNode("D");
		graph.addNode("F");
		
		graph.addEdge("A", "B");
		graph.addEdge("B", "E");
		graph.addEdge("B", "A");
		graph.addEdge("E", "D");
		graph.addEdge("D", "E");
		graph.addEdge("D", "A");
		graph.addEdge("A", "F");
		
		return graph;
	}
	
	
	public void test01SimpleGraph() {
		GraphFactorySimple graphFactory = new GraphFactorySimple();
		Graph<SimpleNode, SimpleEdge> graph = createExampleGraph(graphFactory);
		assertTrue(graph.hasEdge("A", "B"));
		assertTrue(!graph.hasEdge("C", "X"));
	}
	
	public void test02GraphicalGraph() {
		GraphFactoryGraphical graphFactory = new GraphFactoryGraphical();
		Graph<GraphicalNode, Edge<GraphicalNode>> graph = createExampleGraph(graphFactory);
	}	
}
