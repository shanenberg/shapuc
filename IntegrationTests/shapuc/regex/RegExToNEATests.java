package shapuc.regex;

import junit.framework.TestCase;
import shapuc.automata.NEA;
import shapuc.automata.NEAFactory;
import shapuc.automata.custom.CustomTransitionHandler;
import shapuc.regex.operations.RegExToNEACreationVisitor;
import shapuc.scanning.tokenizer.TokenizerNEA;
import shapuc.regex.operations.RegExFactory;

public class RegExToNEATests extends TestCase {
	
	
	public void testAOrB() throws Exception {
		RegEx r = new RegExFactory() { public RegEx regEx() {return 
				OR(TOKEN('A'), TOKEN('B'));
		}}.regEx();
		
		TokenizerNEA nea;

		nea = new shapuc.automata.factories.NEAFromRegExFactory().createInactiveNEAFor(r);
		nea.start();
		assertFalse(nea.hasFinished());
		nea.gotoNext('A');
		assertTrue(nea.hasFinished());
		
		nea = new shapuc.automata.factories.NEAFromRegExFactory().createInactiveNEAFor(r);
		nea.start();
		assertFalse(nea.hasFinished());
		nea.gotoNext('B');
		assertTrue(nea.hasFinished());
	}	
	
	public void testAB() throws Exception {
		RegEx r = new RegExFactory() { public RegEx regEx() {return 
				AND(TOKEN('A'), TOKEN('B'));
		}}.regEx();
		
		TokenizerNEA nea = 
				new shapuc.automata.factories.NEAFromRegExFactory().createInactiveNEAFor(r);
		nea.start();
		assertFalse(nea.hasFinished());
		nea.gotoNext('A');
		assertFalse(nea.hasFinished());
		nea.gotoNext('B');
		assertTrue(nea.hasFinished());
	}	
	
	/*
	 * Simple Token combines by A
	 * -> (Start) -> () -> (concatStateStart) -A-> (concatEndState) -> (EndState)
	 */
	public void testA() throws Exception {
		RegEx r = new RegExFactory() { public RegEx regEx() {return 
				AND(TOKEN('A'));
		}}.regEx();
		
		TokenizerNEA nea = 
				new shapuc.automata.factories.NEAFromRegExFactory().createInactiveNEAFor(r);

		nea.start();

		assertFalse(nea.hasFinished());
		nea.gotoNext('A');
		assertTrue(nea.hasFinished());
		assertEquals(4, nea.getAllStates().size());
		assertEquals(1, nea.getEndStates().size());
	}
}
