package shapuc.util;

import java.util.HashSet;

import junit.framework.TestCase;

public class IdentitySetTests extends TestCase {
	
	public void testHashSetTest() {
		HashSet<HashSet<Character>> h = new HashSet<HashSet<Character>>();
		
		HashSet<Character> set1 = new HashSet<Character>();
		set1.add('a');
		
		HashSet<Character> set2 = new HashSet<Character>();
		set1.add('a');
		
		h.add(set1);
		h.add(set2);
		
		assertEquals(2, h.size());
	}
	
	
	public void test01() {
		class Element {};
		IdentitySet<Element> set = new IdentitySet<Element>();
		
		assertTrue(set.isEmpty());
		assertTrue(set.size()==0);

		Element e  = new Element();
		set.add(e); 
		set.add(e);

		assertTrue(!set.isEmpty());
		assertTrue(set.size()==1);

		Element e2  = new Element();
		set.add(e2); 
		set.add(e2);
		
		assertTrue(!set.isEmpty());
		assertTrue(set.size()==2);
		
		
	}
}
