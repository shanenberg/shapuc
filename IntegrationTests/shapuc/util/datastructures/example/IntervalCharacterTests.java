package shapuc.util.datastructures.example;

import shapuc.util.datastructures.IntervalChar;
import static shapuc.util.datastructures.Interval.ContainmentType.*;

import junit.framework.TestCase;

public class IntervalCharacterTests extends TestCase {
	
	public void testDiffChar_simple() {
		assertEquals(2, new IntervalChar('c', 'm').diffWithRemovedChar('d').length);
		assertEquals(new IntervalChar('c', 'c'), new IntervalChar('c', 'm').diffWithRemovedChar('d')[0]);
		assertEquals(new IntervalChar('e', 'm'), new IntervalChar('c', 'm').diffWithRemovedChar('d')[1]);
		assertEquals(1, new IntervalChar('c', 'c').diffWithRemovedChar('c').length);
		assertEquals(null, new IntervalChar('c', 'c').diffWithRemovedChar('c')[0]);
	}
		
		
	public void testDiff_simple() {
		IntervalChar origin = new IntervalChar('c', 'm');
		IntervalChar noneOverlap = new IntervalChar('n', 'p');
		IntervalChar fullContainment = new IntervalChar('f', 'h');
		IntervalChar fullRightBorderedContainment = new IntervalChar('f', 'm'); 
		IntervalChar fullLeftBorderedContainment = new IntervalChar('c', 'g'); 
		IntervalChar partialOverlapRight = new IntervalChar('h', 'p');
		IntervalChar partialOverlapLeft = new IntervalChar('a', 'f');
		IntervalChar fullIncludedBy = new IntervalChar('a', 'z');
		IntervalChar fullIncludedByLeft = new IntervalChar('c', 'z');
		IntervalChar fullIncludedByRight = new IntervalChar('a', 'm');
		IntervalChar equal = new IntervalChar('c', 'm');
		
		// Wenn kein Overlap, dann erhalte ich beide
		assertEquals(NONE, origin.diffIntervals(noneOverlap).getLeft());
		assertEquals(new IntervalChar('n', 'p'), origin.diffIntervals(noneOverlap).getRight()[0]);
		assertEquals(new IntervalChar('c', 'm'), origin.diffIntervals(noneOverlap).getRight()[1]);
		
		// Wenn equal, dann nur eins, was beide repräsentiert
		assertEquals(EQUALS, origin.diffIntervals(equal).getLeft());
		assertEquals(1, origin.diffIntervals(equal).getRight().length);
		assertEquals('c', (char) origin.diffIntervals(equal).getRight()[0].start);
		assertEquals('m', (char) origin.diffIntervals(equal).getRight()[0].end);
		assertEquals(new IntervalChar('c', 'm'), origin.diffIntervals(equal).getRight()[0]);
		
		// Wenn fullContainment, dann nur drei, erstes in angepasstes element
		assertEquals(FULL_CONTAINMENT, origin.diffIntervals(fullContainment).getLeft());
		assertEquals(3, origin.diffIntervals(fullContainment).getRight().length);
		assertEquals(new IntervalChar('f', 'h'), origin.diffIntervals(fullContainment).getRight()[0]);
		assertEquals(new IntervalChar('c', 'e'), origin.diffIntervals(fullContainment).getRight()[1]);
		assertEquals(new IntervalChar('i', 'm'), origin.diffIntervals(fullContainment).getRight()[2]);
		
		// Wenn fullContainment, dann nur drei, was beide repräsentiert
		assertEquals(FULL_CONTAINMENT_LEFTBORDERED, origin.diffIntervals(fullLeftBorderedContainment).getLeft());
		assertEquals(2, origin.diffIntervals(fullLeftBorderedContainment).getRight().length);
		assertEquals(new IntervalChar('c', 'g'), origin.diffIntervals(fullLeftBorderedContainment).getRight()[0]);
		assertEquals(new IntervalChar('h', 'm'), origin.diffIntervals(fullLeftBorderedContainment).getRight()[1]);

		// Wenn fullContainment, dann nur drei, was beide repräsentiert
		assertEquals(FULL_CONTAINMENT_RIGHTBORDERED, origin.diffIntervals(fullRightBorderedContainment).getLeft());
		assertEquals(2, origin.diffIntervals(fullRightBorderedContainment).getRight().length);
		assertEquals(new IntervalChar('c', 'e'), origin.diffIntervals(fullRightBorderedContainment).getRight()[0]);
		assertEquals(new IntervalChar('f', 'm'), origin.diffIntervals(fullRightBorderedContainment).getRight()[1]);
	
		// Wenn fullContainment, dann nur drei, was beide repräsentiert
		assertEquals(FULL_INCLUDED_BY, origin.diffIntervals(fullIncludedBy).getLeft());
		assertEquals(3, origin.diffIntervals(fullIncludedBy).getRight().length);
		assertEquals(new IntervalChar('a', 'b'), origin.diffIntervals(fullIncludedBy).getRight()[0]);
		assertEquals(new IntervalChar('n', 'z'), origin.diffIntervals(fullIncludedBy).getRight()[1]);
		assertEquals(new IntervalChar('c', 'm'), origin.diffIntervals(fullIncludedBy).getRight()[2]);
	
		// Wenn fullContainment, dann nur drei, was beide repräsentiert
		assertEquals(FULL_INCLUDED_BY_LEFTBORDERED, origin.diffIntervals(fullIncludedByLeft).getLeft());
		assertEquals(2, origin.diffIntervals(fullIncludedByLeft).getRight().length);
		assertEquals(new IntervalChar('n', 'z'), origin.diffIntervals(fullIncludedByLeft).getRight()[0]);
		assertEquals(new IntervalChar('c', 'm'), origin.diffIntervals(fullIncludedByLeft).getRight()[1]);

		// Wenn fullContainment, dann nur drei, was beide repräsentiert
		assertEquals(FULL_INCLUDED_BY_RIGHTBORDERED, origin.diffIntervals(fullIncludedByRight).getLeft());
		assertEquals(2, origin.diffIntervals(fullIncludedByRight).getRight().length);
		assertEquals(new IntervalChar('a', 'b'), origin.diffIntervals(fullIncludedByRight).getRight()[0]);
		assertEquals(new IntervalChar('c', 'm'), origin.diffIntervals(fullIncludedByRight).getRight()[1]);
	
		// Wenn fullContainment, dann nur drei, was beide repräsentiert
		assertEquals(PARTIAL_OVERLAP_LEFT, origin.diffIntervals(partialOverlapLeft).getLeft());
		assertEquals(3, origin.diffIntervals(partialOverlapLeft).getRight().length);
		assertEquals(new IntervalChar('a', 'b'), origin.diffIntervals(partialOverlapLeft).getRight()[0]);
		assertEquals(new IntervalChar('c', 'f'), origin.diffIntervals(partialOverlapLeft).getRight()[1]);
		assertEquals(new IntervalChar('g', 'm'), origin.diffIntervals(partialOverlapLeft).getRight()[2]);

		// ('c', 'm') vs ('h', 'p');
		assertEquals(PARTIAL_OVERLAP_RIGHT, origin.diffIntervals(partialOverlapRight).getLeft());
		assertEquals(3, origin.diffIntervals(partialOverlapRight).getRight().length);
		assertEquals(new IntervalChar('h', 'm'), origin.diffIntervals(partialOverlapRight).getRight()[0]);
		assertEquals(new IntervalChar('n', 'p'), origin.diffIntervals(partialOverlapRight).getRight()[1]);
		assertEquals(new IntervalChar('c', 'g'), origin.diffIntervals(partialOverlapRight).getRight()[2]);
	
	}
	
	
	public void testComparison() {
		IntervalChar origin = new IntervalChar('c', 'm');
		IntervalChar noneOverlap = new IntervalChar('n', 'p');
		IntervalChar fullContainment = new IntervalChar('f', 'h');
		IntervalChar fullRightBorderedContainment = new IntervalChar('f', 'm'); 
		IntervalChar fullLeftBorderedContainment = new IntervalChar('c', 'g'); 
		IntervalChar partialOverlapRight = new IntervalChar('h', 'p');
		IntervalChar partialOverlapLeft = new IntervalChar('a', 'f');
		IntervalChar fullIncludedBy = new IntervalChar('a', 'z');
		IntervalChar fullIncludedByLeft = new IntervalChar('c', 'z');
		IntervalChar fullIncludedByRight = new IntervalChar('a', 'm');
		IntervalChar equal = new IntervalChar('c', 'm');
		
		assertEquals(NONE, origin.overlapping(noneOverlap));
		assertEquals(NONE, noneOverlap.overlapping(origin));
		
		assertEquals(FULL_CONTAINMENT, origin.overlapping(fullContainment));
		assertEquals(FULL_INCLUDED_BY, fullContainment.overlapping(origin));
		
		assertEquals(FULL_CONTAINMENT_LEFTBORDERED, origin.overlapping(fullLeftBorderedContainment));
		assertEquals(FULL_CONTAINMENT_RIGHTBORDERED, origin.overlapping(fullRightBorderedContainment));

		assertEquals(PARTIAL_OVERLAP_LEFT, origin.overlapping(partialOverlapLeft));
		assertEquals(PARTIAL_OVERLAP_RIGHT, origin.overlapping(partialOverlapRight));

		assertEquals(FULL_INCLUDED_BY, origin.overlapping(fullIncludedBy));
		assertEquals(FULL_INCLUDED_BY_LEFTBORDERED, origin.overlapping(fullIncludedByLeft));
		assertEquals(FULL_INCLUDED_BY_RIGHTBORDERED, origin.overlapping(fullIncludedByRight));
		
		assertEquals(EQUALS, origin.overlapping(equal));
		assertEquals(EQUALS, equal.overlapping(origin));
		
	}
}