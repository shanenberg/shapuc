package shapuc.util.datastructures.example;

import java.util.List;

import shapuc.util.datastructures.generic.NAry;
import shapuc.util.datastructures.generic.TreeStructureFactory;

public class GraphFactory implements TreeStructureFactory<TreeElement, List<TreeElement>, Wrapper>{

	public TreeElement createElement() {
		return null;
	}

	public NAry<TreeElement, List<TreeElement>> createNElement() {
		return null;
	}

	public Wrapper createSelfRefElement() {
		return null;
	}
	
}
