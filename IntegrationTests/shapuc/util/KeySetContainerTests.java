package shapuc.util;

import junit.framework.TestCase;
import shapuc.automata.custom.CharTransition;

public class KeySetContainerTests extends TestCase {

	public void test02() {
		KeySetContainer<CharTransition, Object> c = 
				new KeySetContainer<CharTransition, Object>();
		
		c.add(new CharTransition('a'), new Integer(1));
		c.add(new CharTransition('a'), new Integer(2));
		
		assertEquals(1, c.elements.size());
		
	}
	
	
	
	public void test01() {
		KeySetContainer<Character, Object> c = new KeySetContainer<Character, Object>();
		
		c.add('a', new Integer(1));
		c.add(new Character('a'), new Integer(2));
		
		assertEquals(1, c.elements.size());
		
	}
}
