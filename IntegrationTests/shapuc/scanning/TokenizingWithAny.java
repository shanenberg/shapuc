package shapuc.scanning;

import java.util.ArrayList;

import junit.framework.TestCase;
import shapuc.automata.Automata;
import shapuc.graphs.factories.GraphFactoryGraphical2D;
import shapuc.j2d.DrawGraph;
import shapuc.regex.RegEx;
import shapuc.regex.operations.RegExFactory;
import shapuc.scanning.tokenizer.TokenizerFactory;
import shapuc.util.Pair;

public class TokenizingWithAny extends TestCase {
	
	public static void main(String[] args) {
		new TokenizingWithAny().test_SimplifiedJavaString02();
		dg.setVisible(true);
		dg2.setVisible(true);
	}	
	
	static DrawGraph dg;
	static DrawGraph dg2;
	
	static void drawGraph(Automata automata) {
		GraphFactoryGraphical2D f = new GraphFactoryGraphical2D();
		dg = new DrawGraph(automata.toGraph(f), 50);
		dg.setSize(500, 500);
		dg.automPositionNodes();		

		automata.print();
	}	
	
	static void drawGraph2(Automata automata) {
		GraphFactoryGraphical2D f = new GraphFactoryGraphical2D();
		dg2 = new DrawGraph(automata.toGraph(f), 50);
		dg2.setSize(500, 500);
		dg2.automPositionNodes();		

		automata.print();
	}		
	
	static void drawGraph(Tokenizer tokenizer) {
		GraphFactoryGraphical2D f = new GraphFactoryGraphical2D();
		dg = new DrawGraph(tokenizer.automata.toGraph(f), 50);
		dg.setSize(500, 500);
		dg.automPositionNodes();		

		tokenizer.automata.print();
	}
	
	public void test_SimplifiedJavaString02() {
		ArrayList stringOrRegExList = new ArrayList();
		RegEx regex1 = new RegExFactory() { public RegEx regEx() {	
			return AND(KLEENE(ANYTOKEN_WITHOUT('"')), TOKEN('"'));} }.regEx();
		stringOrRegExList.add(new Pair("IDENTIFIER", regex1));	
		
		Tokenizer tokenizer = new TokenizerFactory().
				createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		
		// Now let tokenizer run
		tokenizer.setStringStream("abc\"def");
		tokenizer.resetAutomata();
//		tokenizer.automata.print();
		
//		drawGraph(tokenizer);
		
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("abc\"", tokenizer.resultTokenStream.get(0).getValueString());
	}	
	
	public void test_SimplifiedJavaString01() {
		ArrayList stringOrRegExList = new ArrayList();
		RegEx regex1 = new RegExFactory() { public RegEx regEx() {	
			return KLEENE(ANYTOKEN_WITHOUT('"'));} }.regEx();
		stringOrRegExList.add(new Pair("IDENTIFIER", regex1));	
		
		Tokenizer tokenizer = new TokenizerFactory().
				createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		
		// Now let tokenizer run
		tokenizer.setStringStream("abc\"def");
		tokenizer.resetAutomata();
//		drawGraph(tokenizer);
		
		GraphFactoryGraphical2D f = new GraphFactoryGraphical2D();

		tokenizer.automata.print();
		
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("abc", tokenizer.resultTokenStream.get(0).getValueString());
	}	
	
	/**
	 * any" seems to work......
	 */
	public void test_06() {
		ArrayList stringOrRegExList = new ArrayList();
		RegEx regex1 = new RegExFactory() { public RegEx regEx() {	
			return AND(ANYTOKEN_WITHOUT('"'), TOKEN('"'));} }.regEx();
		stringOrRegExList.add(new Pair("IDENTIFIER", regex1));	
		
		Tokenizer tokenizer = new TokenizerFactory().
				createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		
		// Now let tokenizer run
		tokenizer.setStringStream("a\"def");
		tokenizer.resetAutomata();
		tokenizer.automata.print();
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("a\"", tokenizer.resultTokenStream.get(0).getValueString());
		
		tokenizer.setStringStream("\"def");
		tokenizer.resetAutomata();
		assertEquals(0, tokenizer.resultTokenStream.length());

		tokenizer.setStringStream("abc");
		tokenizer.resetAutomata();
		assertEquals(0, tokenizer.resultTokenStream.length());
	
	}			
	
	
	public void test05() {
		ArrayList stringOrRegExList = new ArrayList();
		RegEx regex1 = new RegExFactory() { public RegEx regEx() {	
			return ANYTOKEN_WITHOUT('X'); 	} }.regEx();
				
		stringOrRegExList.add("A");
		stringOrRegExList.add(new Pair<String, RegEx>("WithoutX", regex1));
			 
		Tokenizer tokenizer = new TokenizerFactory().
				createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		
		// Now let tokenizer run
		tokenizer.setStringStream("A");
		tokenizer.createTokenFromStream();

		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("A", tokenizer.resultTokenStream.get(0).getValueString());
		assertEquals("KEYWORD", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());

		tokenizer.setStringStream("Y");
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("Y", tokenizer.resultTokenStream.get(0).getValueString());
		assertEquals("WithoutX", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());

		tokenizer.setStringStream("X");
		tokenizer.resetAutomata();
		assertEquals(0, tokenizer.resultTokenStream.length());

		
	}		
	
	public void test04() {
		ArrayList stringOrRegExList = new ArrayList();
		RegEx regex1 = new RegExFactory() { public RegEx regEx() {	
			return ANYTOKEN_WITHOUT('X'); 	} }.regEx();
		RegEx regex2 = new RegExFactory() { public RegEx regEx() {	
				return ANYTOKEN_WITHOUT('Y'); 	} }.regEx();
				
				stringOrRegExList.add("A");
				stringOrRegExList.add(new Pair<String, RegEx>("WithoutX", regex1));
				stringOrRegExList.add(new Pair<String, RegEx>("WithoutY", regex2));
			
		Tokenizer tokenizer = new TokenizerFactory().
				createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		
		// Now let tokenizer run
		tokenizer.setStringStream("A");
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("A", tokenizer.resultTokenStream.get(0).getValueString());
		assertEquals("KEYWORD", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());

		tokenizer.setStringStream("X");
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("X", tokenizer.resultTokenStream.get(0).getValueString());
		assertEquals("WithoutY", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());

		tokenizer.setStringStream("Y");
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("Y", tokenizer.resultTokenStream.get(0).getValueString());
		assertEquals("WithoutX", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());
		
	}	
	
	public void test03() {
		ArrayList stringOrRegExList = new ArrayList();
		RegEx regex1 = new RegExFactory() { public RegEx regEx() {	
			return ANYTOKEN_WITHOUT('X'); 	} }.regEx();
		RegEx regex2 = new RegExFactory() { public RegEx regEx() {	
				return ANYTOKEN_WITHOUT('Y'); 	} }.regEx();
				
				stringOrRegExList.add(new Pair<String, RegEx>("WithoutX", regex1));
				stringOrRegExList.add(new Pair<String, RegEx>("WithoutY", regex2));
			
		Tokenizer tokenizer = new TokenizerFactory().
				createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		
		// Now let tokenizer run
		tokenizer.setStringStream("A");
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("A", tokenizer.resultTokenStream.get(0).getValueString());
		assertEquals("WithoutX", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());

		tokenizer.setStringStream("X");
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("X", tokenizer.resultTokenStream.get(0).getValueString());
		assertEquals("WithoutY", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());

		tokenizer.setStringStream("Y");
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("Y", tokenizer.resultTokenStream.get(0).getValueString());
		assertEquals("WithoutX", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());
		
	}		
	
	public void test02() {
		ArrayList stringOrRegExList = new ArrayList();
		RegEx regex = new RegExFactory() { public RegEx regEx() {	
			return ANYTOKEN_WITHOUT('X'); 	} }.regEx();
		stringOrRegExList.add(new Pair<String, RegEx>("dummy", regex));
			
		Tokenizer tokenizer = new TokenizerFactory().
				createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		// Now let tokenizer run
		tokenizer.setStringStream("X");
		tokenizer.createTokenFromStream();
		assertEquals(0, tokenizer.resultTokenStream.length());
	}	
	
	public void test01() {
		ArrayList stringOrRegExList = new ArrayList();
		RegEx regex = new RegExFactory() { public RegEx regEx() {	
			return ANYTOKEN_WITHOUT('X'); 	} }.regEx();
		stringOrRegExList.add(new Pair<String, RegEx>("dummy", regex));
			
		Tokenizer tokenizer = new TokenizerFactory().
				createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		// Now let tokenizer run
		
		tokenizer.setStringStream("A");
		tokenizer.automata.start();
		tokenizer.createTokenFromStream();
		assertEquals(1, tokenizer.resultTokenStream.length());
		assertEquals("A", tokenizer.resultTokenStream.get(0).getValueString());
		assertEquals("dummy", tokenizer.resultTokenStream.get(0).getTokenType().getTokenTypeName());
	}
}
