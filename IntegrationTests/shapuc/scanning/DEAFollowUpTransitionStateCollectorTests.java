package shapuc.scanning;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.TreeSet;
import java.util.Vector;

import junit.framework.TestCase;
import shapuc.automata.custom.CharRangeTransition;
import shapuc.automata.custom.CharTransition;
import shapuc.scanning.tokenizer.DEAFollowUpTransitionStateCollector;
import shapuc.scanning.tokenizers.TokenizerState;
import shapuc.scanning.tokenizers.TokenizerStateInactive;
import shapuc.util.Pair;
import shapuc.util.datastructures.IntervalChar;

public class DEAFollowUpTransitionStateCollectorTests extends TestCase {
	
	class TestState implements TokenizerState {
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + id;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TestState other = (TestState) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (id != other.id)
				return false;
			return true;
		}
		public int id;
		public TestState(int id) {super(); this.id = id; }
		public void reachedState() {}
		public boolean isAcceptor() {return false;}
		public int getPriority() {return id;}

		public TokenizerState clone() {
			return new TestState(id);
		}
		@Override
		public String toString() {
			return "TestState [id=" + id + "]";
		}
		private DEAFollowUpTransitionStateCollectorTests getOuterType() {
			return DEAFollowUpTransitionStateCollectorTests.this;
		}
	}

	public void test07() {
		DEAFollowUpTransitionStateCollector c = new DEAFollowUpTransitionStateCollector();
		c.addRangeCharTransition(new CharRangeTransition('a', 'd'), new TestState(1));
		c.addRangeCharTransition(new CharRangeTransition('b', 'b'), new TestState(2));

		Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>> result = 
				new Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>>(); 

		Vector<Pair<IntervalChar, HashSet<TokenizerState>>> resultIntervalStates =
				new Vector<Pair<IntervalChar, HashSet<TokenizerState>>>();
		
		c.prepareIntervals(result);
	
		c.createIntervals(result, resultIntervalStates);

		assertEquals(3, resultIntervalStates.size());
	
		assertEquals('a', (char) (resultIntervalStates.get(0).getLeft().start));
		assertEquals('a', (char) (resultIntervalStates.get(0).getLeft().end));
		assertTrue(resultIntervalStates.get(0).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(0).getRight().size() == 1);

		assertEquals('b', (char) (resultIntervalStates.get(1).getLeft().start));
		assertEquals('b', (char) (resultIntervalStates.get(1).getLeft().end));
		assertTrue(resultIntervalStates.get(1).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(1).getRight().contains(new TestState(2)));
		assertTrue(resultIntervalStates.get(1).getRight().size() == 2);
	
		assertEquals('c', (char) (resultIntervalStates.get(2).getLeft().start));
		assertEquals('d', (char) (resultIntervalStates.get(2).getLeft().end));
		assertTrue(resultIntervalStates.get(2).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(2).getRight().size() == 1);
	}	
	
	public void test06() {
		DEAFollowUpTransitionStateCollector c = new DEAFollowUpTransitionStateCollector();
		c.addRangeCharTransition(new CharRangeTransition('a', 'a'), new TestState(1));
		c.addRangeCharTransition(new CharRangeTransition('a', 'c'), new TestState(2));

		Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>> result = 
				new Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>>(); 

		Vector<Pair<IntervalChar, HashSet<TokenizerState>>> resultIntervalStates =
				new Vector<Pair<IntervalChar, HashSet<TokenizerState>>>();
		
		c.prepareIntervals(result);
	
		c.createIntervals(result, resultIntervalStates);

		assertEquals(2, resultIntervalStates.size());
	
		assertEquals('a', (char) (resultIntervalStates.get(0).getLeft().start));
		assertEquals('a', (char) (resultIntervalStates.get(0).getLeft().end));
		assertTrue(resultIntervalStates.get(0).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(0).getRight().contains(new TestState(2)));
		assertTrue(resultIntervalStates.get(0).getRight().size() == 2);

		assertEquals('b', (char) (resultIntervalStates.get(1).getLeft().start));
		assertEquals('c', (char) (resultIntervalStates.get(1).getLeft().end));
		assertTrue(resultIntervalStates.get(1).getRight().contains(new TestState(2)));
		assertTrue(resultIntervalStates.get(1).getRight().size() == 1);

	}
	
	public void test05() {
		DEAFollowUpTransitionStateCollector c = new DEAFollowUpTransitionStateCollector();
		c.addRangeCharTransition(new CharRangeTransition('a', 'c'), new TestState(1));
		c.addRangeCharTransition(new CharRangeTransition('c', 'e'), new TestState(2));
		c.addRangeCharTransition(new CharRangeTransition('c', 'f'), new TestState(3));
		
		Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>> result = 
				new Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>>(); 

		Vector<Pair<IntervalChar, HashSet<TokenizerState>>> resultIntervalStates =
				new Vector<Pair<IntervalChar, HashSet<TokenizerState>>>();
		
		c.prepareIntervals(result);
		TreeSet<Character> ts = new TreeSet<Character>();
		ts.addAll(result.keySet());
		
		assertEquals('a', (char) (Character) (ts.toArray()[0]));
		assertEquals(0, result.get('a').getLeft().size());
		assertEquals(1, result.get('a').getRight().size());
		
		assertEquals('c', (char) (Character) (ts.toArray()[1]));
		assertEquals(1, result.get('c').getLeft().size());
		assertEquals(2, result.get('c').getRight().size());

		assertEquals('e', (char) (Character) (ts.toArray()[2]));
		assertEquals(1, result.get('e').getLeft().size());
		assertEquals(0, result.get('e').getRight().size());

		assertEquals('f', (char) (Character) (ts.toArray()[3]));
		assertEquals(1, result.get('f').getLeft().size());
		assertEquals(0, result.get('f').getRight().size());
		
		c.createIntervals(result, resultIntervalStates);
		
		assertEquals('a', (char) (resultIntervalStates.get(0).getLeft().start));
		assertEquals('b', (char) (resultIntervalStates.get(0).getLeft().end));
		assertTrue(resultIntervalStates.get(0).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(0).getRight().size() == 1);
	
		assertEquals('c', (char) (resultIntervalStates.get(1).getLeft().start));
		assertEquals('c', (char) (resultIntervalStates.get(1).getLeft().end));
		assertTrue(resultIntervalStates.get(1).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(1).getRight().contains(new TestState(2)));
		assertTrue(resultIntervalStates.get(1).getRight().contains(new TestState(3)));
		assertTrue(resultIntervalStates.get(1).getRight().size() == 3);

		assertEquals('d', (char) (resultIntervalStates.get(2).getLeft().start));
		assertEquals('e', (char) (resultIntervalStates.get(2).getLeft().end));
		assertTrue(resultIntervalStates.get(2).getRight().contains(new TestState(2)));
		assertTrue(resultIntervalStates.get(2).getRight().contains(new TestState(3)));
		assertTrue(resultIntervalStates.get(2).getRight().size() == 2);
	
		assertEquals('f', (char) (resultIntervalStates.get(3).getLeft().start));
		assertEquals('f', (char) (resultIntervalStates.get(3).getLeft().end));
		assertTrue(resultIntervalStates.get(3).getRight().contains(new TestState(3)));

		assertTrue(resultIntervalStates.size()==4);
	
	}
	
	public void test04() {
		DEAFollowUpTransitionStateCollector c = new DEAFollowUpTransitionStateCollector();
		c.addRangeCharTransition(new CharRangeTransition('a', 'c'), new TestState(1));
		c.addRangeCharTransition(new CharRangeTransition('c', 'e'), new TestState(2));
		
		Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>> result = 
				new Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>>(); 

		Vector<Pair<IntervalChar, HashSet<TokenizerState>>> resultIntervalStates =
				new Vector<Pair<IntervalChar, HashSet<TokenizerState>>>();
		
		c.prepareIntervals(result);
		TreeSet<Character> ts = new TreeSet<Character>();
		ts.addAll(result.keySet());
		
		assertEquals('a', (char) (Character) (ts.toArray()[0]));
		assertEquals(0, result.get('a').getLeft().size());
		assertEquals(1, result.get('a').getRight().size());
		
		assertEquals('c', (char) (Character) (ts.toArray()[1]));
		assertEquals(1, result.get('c').getLeft().size());
		assertEquals(1, result.get('c').getRight().size());

		assertEquals('e', (char) (Character) (ts.toArray()[2]));
		assertEquals(1, result.get('e').getLeft().size());
		assertEquals(0, result.get('e').getRight().size());
		
		c.createIntervals(result, resultIntervalStates);
		assertTrue(resultIntervalStates.size()>1);
		
		assertEquals('a', (char) (resultIntervalStates.get(0).getLeft().start));
		assertEquals('b', (char) (resultIntervalStates.get(0).getLeft().end));
		assertTrue(resultIntervalStates.get(0).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(0).getRight().size() == 1);
	
		assertEquals('c', (char) (resultIntervalStates.get(1).getLeft().start));
		assertEquals('c', (char) (resultIntervalStates.get(1).getLeft().end));
		assertTrue(resultIntervalStates.get(1).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(1).getRight().contains(new TestState(2)));
		assertTrue(resultIntervalStates.get(1).getRight().size() == 2);

		assertEquals('d', (char) (resultIntervalStates.get(2).getLeft().start));
		assertEquals('e', (char) (resultIntervalStates.get(2).getLeft().end));
		assertTrue(resultIntervalStates.get(2).getRight().contains(new TestState(2)));
		assertTrue(resultIntervalStates.get(2).getRight().size() == 1);
		

	}	
	
	public void test03() {
		DEAFollowUpTransitionStateCollector c = new DEAFollowUpTransitionStateCollector();
		c.addRangeCharTransition(new CharRangeTransition('a', 'f'), new TestState(1));
		c.addRangeCharTransition(new CharRangeTransition('c', 'd'), new TestState(2));
		
		Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>> result = 
				new Hashtable<Character, Pair<HashSet<HashSet<TokenizerState>>, HashSet<HashSet<TokenizerState>>>>(); 
		
		Vector<Pair<IntervalChar, HashSet<TokenizerState>>> resultIntervalStates =
				new Vector<Pair<IntervalChar, HashSet<TokenizerState>>>();
		
		c.prepareIntervals(result);
		TreeSet<Character> ts = new TreeSet<Character>();
		ts.addAll(result.keySet());
		
		assertEquals('a', (char) (Character) (ts.toArray()[0]));
		assertTrue(result.get('a').getLeft().isEmpty());
		assertEquals(1, result.get('a').getRight().size());
		
		assertEquals('c', (char) (Character) (ts.toArray()[1]));
		assertTrue(result.get('c').getLeft().isEmpty());
		assertEquals(1, result.get('c').getRight().size());

		assertEquals('d', (char) (Character) (ts.toArray()[2]));
		assertEquals(1, result.get('d').getLeft().size());
		assertEquals(0, result.get('d').getRight().size());

		assertEquals('f', (char) (Character) (ts.toArray()[3]));
		assertEquals(1, result.get('f').getLeft().size());
		assertEquals(0, result.get('d').getRight().size());

		c.createIntervals(result, resultIntervalStates);
		assertTrue(resultIntervalStates.size()>1);
		
		assertEquals('a', (char) (resultIntervalStates.get(0).getLeft().start));
		assertEquals('b', (char) (resultIntervalStates.get(0).getLeft().end));
		assertTrue(resultIntervalStates.get(0).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(0).getRight().size() == 1);
	
		assertEquals('c', (char) (resultIntervalStates.get(1).getLeft().start));
		assertEquals('d', (char) (resultIntervalStates.get(1).getLeft().end));
		assertTrue(resultIntervalStates.get(1).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(1).getRight().contains(new TestState(2)));
		assertTrue(resultIntervalStates.get(1).getRight().size() == 2);

		assertEquals('e', (char) (resultIntervalStates.get(2).getLeft().start));
		assertEquals('f', (char) (resultIntervalStates.get(2).getLeft().end));
		assertTrue(resultIntervalStates.get(2).getRight().contains(new TestState(1)));
		assertTrue(resultIntervalStates.get(2).getRight().size() == 1);
	
	
	}
	
	
	public void test02() {
		
		DEAFollowUpTransitionStateCollector c = new DEAFollowUpTransitionStateCollector();
		c.addRangeCharTransition(new CharRangeTransition('a', 'f'), new TokenizerStateInactive(6, null));
		c.addRangeCharTransition(new CharRangeTransition('c', 'd'), new TokenizerStateInactive(6, null));

		c.prepareForDEA();
		assertEquals(2, c.rangeTransitions.elements.size());
		assertTrue(c.rangeTransitions.containsKey(new CharRangeTransition('a', 'b')));
		assertTrue(c.rangeTransitions.containsKey(new CharRangeTransition('e', 'f')));
		assertTrue(c.rangeTransitions.containsKey(new CharRangeTransition('c', 'd')));
		
	}
	
	
	public void test00_rangeOverlap() {
		
		
		// Test 0
		DEAFollowUpTransitionStateCollector c;
		c = new DEAFollowUpTransitionStateCollector();
		c.addRangeCharTransition(new CharRangeTransition('a', 'f'), new TestState(1));
		c.addRangeCharTransition(new CharRangeTransition('d', 'f'), new TestState(2));

		
		
		c.prepareForDEA();

		assertEquals(2, c.rangeTransitions.elements.size());
		assertEquals('a', (char) (c.rangeTransitions.elements.get(0).getLeft().interval.start));
		assertEquals('c', (char) (c.rangeTransitions.elements.get(0).getLeft().interval.end));
		assertEquals(1, c.rangeTransitions.elements.get(0).getRight().size());
		assertTrue(c.rangeTransitions.elements.get(0).getRight().contains(new TestState(1)));
		
		assertEquals('d', (char) (c.rangeTransitions.elements.get(1).getLeft().interval.start));
		assertEquals('f', (char) (c.rangeTransitions.elements.get(1).getLeft().interval.end));
		assertTrue(c.rangeTransitions.elements.get(1).getRight().contains(new TestState(1)));
		assertTrue(c.rangeTransitions.elements.get(1).getRight().contains(new TestState(2)));
		
		// Test 1
		c = new DEAFollowUpTransitionStateCollector();
		c.addRangeCharTransition(new CharRangeTransition('a', 'f'), new TestState(1));
		c.addRangeCharTransition(new CharRangeTransition('d', 'i'), new TestState(2));
		c.prepareForDEA();
		
		c.rangeTransitions.printout();
		assertEquals(3, c.rangeTransitions.elements.size());

		assertEquals('a', (char) (c.rangeTransitions.elements.get(0).getLeft().interval.start));
		assertEquals('c', (char) (c.rangeTransitions.elements.get(0).getLeft().interval.end));
		assertEquals(1, c.rangeTransitions.elements.get(0).getRight().size());
		assertTrue(c.rangeTransitions.elements.get(0).getRight().contains(new TestState(1)));

		assertEquals('g', (char) (c.rangeTransitions.elements.get(1).getLeft().interval.start));
		assertEquals('i', (char) (c.rangeTransitions.elements.get(1).getLeft().interval.end));
		assertEquals(1, c.rangeTransitions.elements.get(1).getRight().size());
		assertTrue(c.rangeTransitions.elements.get(0).getRight().contains(new TestState(2)));
		
		
		assertEquals('d', (char) (c.rangeTransitions.elements.get(2).getLeft().interval.start));
		assertEquals('f', (char) (c.rangeTransitions.elements.get(2).getLeft().interval.end));
		assertTrue(c.rangeTransitions.elements.get(1).getRight().contains(new TestState(1)));
		
		
//
//		assertEquals('d', (char) (c.rangeTransitions.elements.get(1).getLeft().interval.start));
//		assertEquals('f', (char) (c.rangeTransitions.elements.get(1).getLeft().interval.end));
	}	
}
