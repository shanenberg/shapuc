package shapuc.scanning;

import java.util.ArrayList;
import java.util.Vector;

import junit.framework.TestCase;
import shapuc.automata.InvalidInputException;
import shapuc.scanning.tokenizer.TokenizerFactory;
import shapuc.scanning.tokenizers.TokenizingException;
import shapuc.streams.StringBufferStream;
import shapuc.streams.TokenStream;

public class TokenizerTest extends TestCase {

	public void test02TwoKeywords02() throws Exception {

		ArrayList stringOrRegExList = new ArrayList();
		stringOrRegExList.add("HelloWorld1");
		stringOrRegExList.add("HelloWorld2");

		Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		// Now let tokenizer run
		tokenizer.setStringStream("HelloWorld1");
		tokenizer.createTokenFromStream();

		assertEquals("HelloWorld1", tokenizer.resultTokenStream.get(0).getValueString());
		
		tokenizer.setStringStream("HelloWorld2");
		tokenizer.createTokenFromStream();

		assertEquals("HelloWorld2", tokenizer.resultTokenStream.get(0).getValueString());
	}
	
	public void test02TwoKeywords01() throws Exception {
		StringBufferStream charInputStream = new StringBufferStream("1");
		TokenStream tokenOutputStream = new TokenStream(charInputStream);

		Vector<Object> keywords = new Vector<Object>();
		keywords.addElement("1");
		keywords.addElement("2");

		Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(keywords);
		tokenizer.charStream = charInputStream;
		tokenizer.resultTokenStream = tokenOutputStream;
		tokenizer.automata.start();
		tokenizer.createTokenFromStream();

		tokenizer.setStringStream("2");
		tokenizer.createTokenFromStream();

		try {
			charInputStream = new StringBufferStream("3");
			tokenOutputStream = new TokenStream(charInputStream);
			tokenizer.charStream = charInputStream;
			tokenizer.resultTokenStream = tokenOutputStream;
			tokenizer.clean();
			tokenizer.automata.start();
			tokenizer.createTokenFromStream();
			
//			System.out.println(tokenizer.automata);
			
			
//			System.out.println(tokenizer.resultTokenStream.length());			
//			System.out.println(tokenizer.resultTokenStream.get(0));			
			
			
			assertTrue(false);
		} catch (Exception ex) {
			assertTrue(true);
		}
	}

	public void testTokenizerWithSingleKeyword() throws Exception {

		StringBufferStream charInputStream = new StringBufferStream("HelloWorld1");
		TokenStream tokenOutputStream = new TokenStream(charInputStream);

		Vector<Object> keywords = new Vector<Object>();
		keywords.addElement("HelloWorld1");

		Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(keywords);
		tokenizer.charStream = charInputStream;
		tokenizer.resultTokenStream = tokenOutputStream;
		tokenizer.automata.start();
		tokenizer.createTokenFromStream();

		assertEquals("HelloWorld1", tokenOutputStream.get(0).getValueString());

	}

}
