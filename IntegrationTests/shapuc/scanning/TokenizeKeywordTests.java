package shapuc.scanning;

import java.util.ArrayList;

import junit.framework.TestCase;
import shapuc.automata.AutomataFactoryParameters;
import shapuc.automata.InvalidInputException;
import shapuc.scanning.tokenizer.TokenizerFactory;
import shapuc.scanning.tokenizers.NEAForKeywordCreationVisitor;
import shapuc.scanning.tokenizers.NEAForRegExCreationVisitor;
import shapuc.scanning.tokenizers.TokenizingException;
import shapuc.streams.PositionStream;
import shapuc.streams.StringBufferStream;
import shapuc.streams.TokenStream;

public class TokenizeKeywordTests extends TestCase {
	
	public void test03_ExceptionFromTokenizer() throws Exception {
		
		ArrayList stringOrRegExList = new ArrayList();
		stringOrRegExList.add("ABC");

		Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		// Now let tokenizer run
		tokenizer.setStringStream("X");
		tokenizer.createTokenFromStream();
		assertEquals(0, tokenizer.resultTokenStream.length());
	
	}		
	
	public void test02() throws Exception {
		
		ArrayList stringOrRegExList = new ArrayList();
		stringOrRegExList.add("ABC");

		Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		// Now let tokenizer run
		tokenizer.setStringStream("ABC");
		tokenizer.createTokenFromStream();

		assertTrue(tokenizer.automata.hasFinished()); // Automata has finished, but the currentPosition is not the end!
		assertEquals(1, tokenizer.resultTokenStream.length());		
		assertTrue(tokenizer.resultTokenStream.get(0).getTokenType().isKeyword());		
		assertEquals(0, tokenizer.resultTokenStream.get(0).startPositionInStream);		
		assertEquals(2, tokenizer.resultTokenStream.get(0).endPositionInStream);		
		assertEquals(3, tokenizer.charStream.currentPosition);	
		
		tokenizer.setStringStream("ABCABC");
		tokenizer.resetAutomata();
		tokenizer.createTokenFromStream();		
		assertEquals(1, tokenizer.resultTokenStream.length()); // NOTE: THIS IS NOT THE SCANNER....ITS THE TOKENIZER!
		
	}	
	
	
	/**
	 * Tokenizer Accepts part of the input word.....but not everything.
	 * @throws Exception
	 */
	public void test01() throws Exception {
		
		ArrayList stringOrRegExList = new ArrayList();
		stringOrRegExList.add("Hello");

		Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(stringOrRegExList);
		// Now let tokenizer run
		tokenizer.setStringStream("Hello");
		tokenizer.createTokenFromStream();

		assertTrue(tokenizer.automata.hasFinished());
		assertEquals(1, tokenizer.resultTokenStream.length());		
		assertTrue(tokenizer.resultTokenStream.get(0).getTokenType().isKeyword());		
		assertTrue(tokenizer.resultTokenStream.get(0).startPositionInStream==0);		
		assertTrue(tokenizer.resultTokenStream.get(0).endPositionInStream==4);		
		
	}
}
