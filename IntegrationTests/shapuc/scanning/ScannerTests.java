package shapuc.scanning;

import java.util.ArrayList;

import junit.framework.TestCase;
import shapuc.regex.RegEx;
import shapuc.regex.operations.RegExFactory;
import shapuc.scanning.tokenizer.TokenizerFactory;
import shapuc.util.Pair;

public class ScannerTests extends TestCase {
	
	public void test_SimplifiedAPI() {
		Scanner scanner = new Scanner(Separator.Char(' '), "T1", "T2", "T3");
		scanner.setInputString("T1 T1 T1 T2 T2 T1 T1");
		scanner.scan();		
	}
	
	
	public void test_HalloWeltWithReadAll() {

		ArrayList keywords = new ArrayList();
		keywords.add(new Separator(new RegExFactory(){public RegEx regEx() {
				return TOKEN(' ');}}.regEx()));
		
		keywords.add("hallo");
		keywords.add("meine");
		keywords.add("sehr");
		keywords.add("kleine");
		keywords.add("welt");
		
		RegEx regex = new RegExFactory(){public RegEx regEx() {
			return AND(TOKEN('"'), KLEENE(ANYTOKEN_WITHOUT('"')), TOKEN('"'));}}.regEx();
		keywords.add(new Pair("IDENTIFIER",regex));

		Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(keywords);
		Scanner scanner = new Scanner(tokenizer);
		tokenizer.setStringStream("hallo \"abc\" welt");
		scanner.scan();
		assertEquals("hallo", tokenizer.resultTokenStream.get(0).getValueString());
		assertEquals("\"abc\"", tokenizer.resultTokenStream.get(1).getValueString());
		assertEquals("welt", tokenizer.resultTokenStream.get(2).getValueString());
		assertEquals(3, tokenizer.resultTokenStream.length());
		
	}	
	
	
	public void test_HalloWelt() {

		ArrayList keywords = new ArrayList();
		keywords.add(new Separator(new RegExFactory(){public RegEx regEx() {
				return TOKEN(' ');}}.regEx()));
		
		keywords.add("hallo");
		keywords.add("meine");
		keywords.add("sehr");
		keywords.add("kleine");
		keywords.add("welt");
		Tokenizer tokenizer = new TokenizerFactory().createTokenizerFromKeywordsOrRegExs(keywords);
		Scanner scanner = new Scanner(tokenizer);
		tokenizer.setStringStream("hallomeinesehrsehrsehrsehrsehrsehr sehr  sehrsehrsehrsehrkleinewelt");
		scanner.scan();
		
	}

}
